package com.locatordevice.utils;

import java.io.File;
import java.io.IOException;

/**
 * Util methods for work with files.
 * @version 1.0
 * @author Morhun N.G.
 */
public final class FilesUtil {

    /**
     * Verify whether specified directory exist in current directory and create it if it not exist
     * @param dirName name of directory
     * @return true if directory was create and false if directory already exist
     */
    public static boolean verifyAndCreateDirectory(String dirName) throws IOException {
        File scansDirectory = new File(dirName);
        if (!scansDirectory.exists()) {
            if (! scansDirectory.mkdir()) {
                throw new IOException("Cannot create scans directory");
            }
            return true;
        }
        return false;
    }

}
