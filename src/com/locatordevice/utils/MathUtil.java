package com.locatordevice.utils;

/**
 * Util mathematics methods
 * @version 1.0
 * @author Morhun N.G.
 */
public final class MathUtil {

    /**
     * sqr methods returns square of argument
     */
    public static double sqr(double arg) {
        return arg * arg;
    }
    public static float sqr(float arg) {
        return arg * arg;
    }
    public static int sqr(int arg) {
        return arg * arg;
    }

    /**
     * Convert radians to degrees
     * @param arg value in radians
     * @return argument in degrees
     */
    public static double radToDeg(double arg) {
        return arg * 180 / Math.PI;
    }

    /**
     * Convert degrees to radians
     * @param arg value in degrees
     * @return argument in radians
     */
    public static double degToRad(double arg) {
        return arg * Math.PI / 180;
    }

}
