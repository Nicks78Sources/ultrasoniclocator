package com.locatordevice.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Util methods for converting dates.
 * @version 1.0
 * @author Morhun N.G.
 */
public final class DateUtil {
    public static final String DATE_PATTERN = "yyyy-MM-dd_HH-mm-ss-SSS";
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_PATTERN);

    /**
     * Shape date and time of scan
     * @param time time of scan
     * @return time of scan
     */
    public static String formatScanDate(long time){
        return SIMPLE_DATE_FORMAT.format(new Date(time));
    }

    /**
     * Convert time of scan from string to long
     * @param dateInString time of scan
     * @return canonical time
     */
    public static long parseScanDate(String dateInString) {
        try {
            return SIMPLE_DATE_FORMAT.parse(dateInString).getTime();
        } catch (ParseException e) {
            return 0;
        }
    }
}
