package com.locatordevice.emulator;

import com.locatordevice.comio.DeviceAnswerData;
import com.locatordevice.comio.enums.DeviceSoundType;
import com.locatordevice.comio.enums.TransferType;
import com.locatordevice.comio.exceptions.DeviceIOException;
import com.locatordevice.comio.interfaces.LocatorDeviceDriver;

import com.locatordevice.beans.VirtualObject;
import com.locatordevice.utils.MathUtil;

import java.util.List;

import static com.locatordevice.comio.LocatorConstants.*;

/**
 * emulator of driver for work with Locator Device
 * @version 1.0
 * @author Morhun N.G.
 */
public class DeviceEmulator implements LocatorDeviceDriver {

    /** current azimuth in steps */
    protected int azimuth; // 127 ~ 0 degree, one step: 1.5 degree
    /** current LED state: on/off */
    protected boolean led;
    /** for compatibility (ms), is never exceeded */
    protected int timeout;

    /** objects for simulating */
    protected List<VirtualObject> objectsInSpace;

    /** names and masks for each receiver channel */
    protected enum ChannelName {
        LDn(8), RDn(4), LUp(2), RUp(1);

        private byte mask;
        ChannelName(int shift) {
            mask = (byte) shift;
        }

        /**
         * Returns mask of selected channel.
         * Example: RDn -  00000100
         */
        public byte getMask() {
            return mask;
        }
    }

    /**
     * Constructor. Gets list of objects, from which signal is generated
     * @param objectsInSpace modeled objects
     */
    public DeviceEmulator(List<VirtualObject> objectsInSpace) {
        this.objectsInSpace = objectsInSpace;
        this.led = false;
        this.azimuth = ZERO_AZIMUTH;
    }

    @Override
    public DeviceAnswerData scanSpace(TransferType transferType, int periods) throws DeviceIOException {
        DeviceAnswerData deviceAnswerData = new DeviceAnswerData(this.azimuth, periods);

        int maxDistance = transferType.getScanDistance();
        int dataLength = 2 * transferType.getRawDataLength(); // *2 because in one byte only one sample, instead two
        byte[] pseudoScanData = new byte[dataLength]; // contains one sample per byte: 0000 LDn RDn LUp RUp
        for (int i = 0; i < dataLength; i++) {
            pseudoScanData[i] = 0;
        }

        addParasiticOscillationEffect(pseudoScanData, periods);

        // distances from object to each receiver
        double rUpDist;
        double rDnDist;
        double lUpDist;
        double lDnDist;
        // distance between object and transmitter
        double centralDist;

        double horizontalDelta;
        double verticalDelta;

        int signalStartSample;
        int signalNumberOfPeriods;

        for (VirtualObject currentObject : objectsInSpace) {
            if (currentObject.getDistance() <= maxDistance) {
                horizontalDelta = (HORIZONTAL_DISTANCE_BETWEEN_RECEIVERS * 1e-3) / 2
                        * Math.sin(MathUtil.degToRad(currentObject.getHorizontalAzimuth() - getCurrentAzimuthDegrees()));
                rUpDist = rDnDist = currentObject.getDistance() - horizontalDelta;
                lUpDist = lDnDist = currentObject.getDistance() + horizontalDelta;

                verticalDelta = (VERTICAL_DISTANCE_BETWEEN_RECEIVERS * 1e-3) / 2
                        * Math.sin(MathUtil.degToRad(currentObject.getVerticalAzimuth()));
                rUpDist -= verticalDelta;
                lUpDist -= verticalDelta;
                rDnDist += verticalDelta;
                lDnDist += verticalDelta;
                centralDist = (rUpDist + lUpDist + rDnDist + lDnDist) / 4;

                signalNumberOfPeriods = (int) (periods * currentObject.getReflectCoefficient());

                signalStartSample = (int) ((centralDist + lDnDist - PAUSE_DISTANCE - periods * LENGTH_OF_WAVE + LENGTH_OF_WAVE * (Math.random() - 0.5)) / (LENGTH_OF_WAVE / SAMPLES_PER_PERIOD));
                fillPeriods(pseudoScanData, ChannelName.LDn, signalStartSample, signalNumberOfPeriods);

                signalStartSample = (int) ((centralDist + rDnDist - PAUSE_DISTANCE - periods * LENGTH_OF_WAVE + LENGTH_OF_WAVE * (Math.random() - 0.5)) / (LENGTH_OF_WAVE / SAMPLES_PER_PERIOD));
                fillPeriods(pseudoScanData, ChannelName.RDn, signalStartSample, signalNumberOfPeriods);

                signalStartSample = (int) ((centralDist + lUpDist - PAUSE_DISTANCE - periods * LENGTH_OF_WAVE + LENGTH_OF_WAVE * (Math.random() - 0.5)) / (LENGTH_OF_WAVE / SAMPLES_PER_PERIOD));
                fillPeriods(pseudoScanData, ChannelName.LUp, signalStartSample, signalNumberOfPeriods);

                signalStartSample = (int) ((centralDist + rUpDist - PAUSE_DISTANCE - periods * LENGTH_OF_WAVE + LENGTH_OF_WAVE * (Math.random() - 0.5)) / (LENGTH_OF_WAVE / SAMPLES_PER_PERIOD));
                fillPeriods(pseudoScanData, ChannelName.RUp, signalStartSample, signalNumberOfPeriods);
              }
        }

        deviceAnswerData.setUnpackedData(pseudoScanData, transferType);

        // simulating data transfer time
        final int transferTimeMs = 700;
        try{
            switch (transferType) {
                case QUARTER:
                    Thread.sleep(transferTimeMs / 4);
                    break;
                case HALF:
                    Thread.sleep(transferTimeMs / 2);
                    break;
                case ALL:
                    Thread.sleep(transferTimeMs);
                    break;
            }
        } catch(InterruptedException ignored) { }

        return deviceAnswerData;
    }

    private void addParasiticOscillationEffect(byte[] signal, int periods) {
        for (ChannelName channel : ChannelName.values()) {
            fillPeriods(signal, channel, 0, periods);
        }
    }

    /**
     * Fills specified number of periods in specified signal's channel
     * @param signal array with signal data
     * @param channel name of channel for editing
     * @param startSample sample which begins echo-signal
     * @param periods number of periods for writing
     */
    private void fillPeriods(byte[] signal, ChannelName channel, int startSample, int periods) {
        final byte periodBitmap[] = {1, 1, 0, 0, 0};
        for (int i = 0;  i < periods && (startSample + (i+1) * SAMPLES_PER_PERIOD) < signal.length; i++) {
            for (int j = 0; j < SAMPLES_PER_PERIOD; j++) {
                setSignalBit(signal, channel, startSample + i * SAMPLES_PER_PERIOD + j, periodBitmap[j]);
            }
        }
    }

    /**
     * Sets specified bit in specified channel
     * @param signal array with signal's data
     * @param channel name of channel for editing
     * @param sampleNumber number of sample for editing
     * @param bit bit which will be written (0,1)
     */
    private void setSignalBit(byte[] signal, ChannelName channel, int sampleNumber, int bit) {
        if (bit == 0) {
            signal[sampleNumber] &= ~channel.getMask();
        } else {
            signal[sampleNumber] |= channel.getMask();
        }
    }

    private double getCurrentAzimuthDegrees() {
        return (azimuth - ZERO_AZIMUTH) * AZIMUTH_STEP;
    }

    @Override
    public void setAzimuth(int newAzimuth) throws DeviceIOException {
        if (newAzimuth <= MINIMAL_AZIMUTH || newAzimuth >= MAXIMAL_AZIMUTH) {
            throw new IllegalArgumentException("Wrong azimuth parameter");
        }

        this.azimuth = newAzimuth;
    }

    @Override
    public void ledOn() throws DeviceIOException {
        this.led = true;
    }

    @Override
    public void ledOff() throws DeviceIOException {
        this.led = false;
    }

    @Override
    public void setLed(boolean ledState) throws DeviceIOException {
        if (ledState) {
            ledOn();
        } else {
            ledOff();
        }
    }

    @Override
    public void makeSound(DeviceSoundType soundType) throws DeviceIOException {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            throw new DeviceIOException();
        }
    }

    @Override
    public void reset() throws DeviceIOException {
        this.azimuth = 0;
        this.led = false;
    }

    @Override
    public boolean getLedState() {
        return this.led;
    }

    @Override
    public int getCurrentAzimuth() {
        return this.azimuth;
    }

    @Override
    public int getTimeout() {
        return this.timeout;
    }

    @Override
    public void setTimeout(int newTimeout) {
        this.timeout = newTimeout;
    }

    @Override
    public void resourcesFree() throws DeviceIOException {

    }

}
