package com.locatordevice.controller;

import com.locatordevice.MainApp;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * About program window controller
 * @version 1.0
 * @author Morhun N.G.
 */

public class AboutWindowController {

    public static void showAboutWindow(MainApp mainApp) throws IOException {
        Stage aboutWindowStage = new Stage();
        aboutWindowStage.setTitle("About");
        aboutWindowStage.getIcons().add(new Image("/images/Radar.png"));
        aboutWindowStage.setResizable(false);
        aboutWindowStage.initOwner(mainApp.getPrimaryStage());

        FXMLLoader loader = new FXMLLoader();
        AboutWindowController controller = new AboutWindowController();
        loader.setController(controller);
        loader.setLocation(MainApp.class.getResource("/fxml/AboutWindow.fxml"));

        AnchorPane windowContent = loader.load();

        Scene scene = new Scene(windowContent);
        aboutWindowStage.setScene(scene);
        aboutWindowStage.show();
    }
}
