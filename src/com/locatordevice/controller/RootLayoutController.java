package com.locatordevice.controller;

import com.locatordevice.MainApp;
import com.locatordevice.view.Dialogs;

import javafx.application.Platform;
import javafx.fxml.FXML;

import java.io.IOException;

/**
 * Controller for root layout of main window
 * Processing main menu events
 */
public class RootLayoutController {
	/** pointer to main app */
	private MainApp mainApp;

	public RootLayoutController(MainApp mainAppPointer) {
		this.mainApp = mainAppPointer;
	}

	@FXML
	private void menuExitClick() {
		Platform.exit();
	}
	
	@FXML
	private void menuAboutClick() {
		try {
			AboutWindowController.showAboutWindow(mainApp);
		} catch (IOException e) {
			Dialogs.errorDialog("Cannot load about window");
		}
	}

}
