package com.locatordevice.controller.tabs;

import com.locatordevice.MainApp;

import com.locatordevice.algorithms.RangefinderAlgorithm;

import com.locatordevice.beans.ScanData;
import com.locatordevice.comio.LocatorConstants;
import com.locatordevice.comio.exceptions.DeviceIOException;

import com.locatordevice.controller.ScannedChannelsWindowController;
import com.locatordevice.view.Dialogs;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import java.io.IOException;

/**
 * Controller for rangefinder tab of main window
 * @version 1.0
 * @author Morhun N.G.
 */
public class MainWindowRangefinderTabController extends MainWindowAlgorithmTabController {

    private static final int MAXIMAL_AZIMUTH_IN_DEGREE = 90;
    private static final int MINIMAL_AZIMUTH_IN_DEGREE = -90;

    /** algorithm instance */
    private RangefinderAlgorithm rangefinder;
    /** flag which show whether autorun is turned on */
    private boolean isAutoran = false;
    /** last number of periods which was inputted by user */
    private int periods;
    /** last coordinates of mouse cursor on radar
     * if mouse left radar, then assignment negative values */
    private int lastMouseXCoordinateOnRadar;
    private int lastMouseYCoordinateOnRadar;

    @FXML
    private TextField azimuthTextField;
    @FXML
    private TextField periodsTextField;
    @FXML
    private CheckBox autorunCheckBox;
    @FXML
    private CheckBox filteringCheckBox;
    @FXML
    private Button setAzimuthButton;
    @FXML
    private Button startStopButton;
    @FXML
    private Button showChannelsButton;

    public MainWindowRangefinderTabController(final MainApp mainAppPointer) {
        super(mainAppPointer);

        rangefinder = new RangefinderAlgorithm(mainAppPointer);
    }

    @Override
    public void onActivateTab() {
        super.onActivateTab();

        rangefinder.redrawDistancesDiagram(
                mainApp.getMainWindowController().getSelectedDistance());
    }

    @Override
    public void onDeactivateTab() {
        if (isAutoran) {
            stopAutorun();
        }
    }

    @Override
    public void onDistanceChanged(int newDistance) {
        rangefinder.redrawDistancesDiagram(newDistance);
    }

    @Override
    public void onMouseMoveOnRadar(int x, int y) {
        lastMouseXCoordinateOnRadar = x;
        lastMouseYCoordinateOnRadar = y;
        rangefinder.drawMouseCoordinates(x, y);
    }

    @Override
    public void onMouseLeftRadar() {
        rangefinder.eraseMouseCoordinates();
        lastMouseXCoordinateOnRadar = -1;
        lastMouseYCoordinateOnRadar = -1;
    }

    public void importScanData(ScanData scanData) {
        rangefinder.setLastScanRawData(scanData);
    }

    @FXML
    private void startStopButtonClick() {
        if (isAutoran) {
            stopAutorun();
        } else {
            periods = getInputtedPeriodsOfRadiation();
            if (periods == -1) {
                Dialogs.warningDialog("Inputted periods of radiation is incorrect.\n" +
                        "Please, input correct value and try again.");
                return;
            }

            if (autorunCheckBox.isSelected()) {
                startAutorun();
            } else {
                // single scan
                try {
                    rangefinder.drawDistancesDiagram(
                            mainApp.getMainWindowController().getSelectedDistance(), periods);
                } catch (DeviceIOException e) {
                    Dialogs.errorDialog("An error occurred while getting data from device.", e.getMessage());
                }
            }
        }
    }

    /**
     * Actions which must doing when autorun is active
     */
    private void autorunAction() {
        if (thisTab.isSelected()) {
            try {
                rangefinder.drawDistancesDiagram(
                        mainApp.getMainWindowController().getSelectedDistance(), periods);
                if (lastMouseXCoordinateOnRadar >= 0) {
                    rangefinder.drawMouseCoordinates(lastMouseXCoordinateOnRadar, lastMouseYCoordinateOnRadar);
                }
            } catch (DeviceIOException e) {
                stopAutorun();
                Dialogs.errorDialog("An error occurred while getting data from device.", e.getMessage());
            }
        }

        if (isAutoran) {
            Platform.runLater(this::autorunAction);
        }
    }

    /**
     * Starts autoran and lock some control which must be locked while it working
     * @throws RuntimeException if autorun is turned on
     */
    private void startAutorun() {
        if (isAutoran) {
            throw new RuntimeException();
        }
        isAutoran = true;
        lockUnlockControlsWhileAutorun();
        startStopButton.setText("Stop");
        Platform.runLater(this::autorunAction);
    }

    /**
     * Stops autorun and unlock controls
     */
    private void stopAutorun() {
        isAutoran = false;
        lockUnlockControlsWhileAutorun();
        startStopButton.setText("Start");
    }

    /**
     * Lock some controls which must be locked, while autorun looping,
     * and unlock them when autorun is off
     */
    private void lockUnlockControlsWhileAutorun() {
        periodsTextField.setDisable(isAutoran);
        autorunCheckBox.setDisable(isAutoran);
        showChannelsButton.setDisable(isAutoran);
        setAzimuthButton.setDisable(isAutoran);
        azimuthTextField.setDisable(isAutoran);
        mainApp.getMainWindowController().setDistanceSliderDisable(isAutoran);
    }

    /**
     * @return periods of radiation set by user or -1 if value is incorrect
     */
    private int getInputtedPeriodsOfRadiation() {
        int periods;
        try {
            periods = Integer.parseInt(periodsTextField.getText());
            if (periods < LocatorConstants.MINIMUM_PERIODS_OF_RADIATION
                    || periods > LocatorConstants.MAXIMUM_PERIODS_OF_RADIATION) {
                periods = -1;
            }
        }
        catch(NumberFormatException e) {
            periods = -1;
        }
        return periods;
    }

    @FXML
    private void filteringCheckBoxClick() {
        rangefinder.setFilter(filteringCheckBox.isSelected());
        rangefinder.redrawDistancesDiagram(
                mainApp.getMainWindowController().getSelectedDistance());
    }

    @FXML
    private void showChannelsButtonClick() {
        if (rangefinder.getLastScanRawData() == null) {
            Dialogs.warningDialog("Please, start algorithm before this action");
        } else {
            try {
                ScannedChannelsWindowController.showScanChannelsInWindow(rangefinder.getLastScanRawData());
            } catch (IOException e) {
                Dialogs.errorDialog(e.getMessage());
            }
        }
    }

    @FXML
    private void setAzimuthButtonClick() {
        int azimuth = getInputtedAzimuth();
        if (azimuth == -1) {
            Dialogs.warningDialog("Inputted azimuth is incorrect.\n" +
                    "Please, input correct value and try again.");
        } else {
            try {
                mainApp.getLocatorDeviceDriver().setAzimuth(azimuth);
            } catch (DeviceIOException e) {
                Dialogs.errorDialog("Azimuth has not changed.", e.getMessage());
            }
        }
    }

    /**
     * @return azimuth in steps set by user or -1 if value is incorrect
     */
    private int getInputtedAzimuth() {
        double azimuth;
        try {
            azimuth = Double.parseDouble(azimuthTextField.getText());
            if (azimuth < MINIMAL_AZIMUTH_IN_DEGREE || azimuth > MAXIMAL_AZIMUTH_IN_DEGREE) {
                azimuth = -1;
            }
        }
        catch(NumberFormatException e) {
            azimuth = -1;
        }
        return (int) Math.round(azimuth / 1.5 + LocatorConstants.ZERO_AZIMUTH);
    }

}
