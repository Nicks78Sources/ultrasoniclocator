package com.locatordevice.controller.tabs;

import com.locatordevice.MainApp;
import com.locatordevice.controller.tabs.MainWindowAlgorithmTab;
import javafx.scene.control.Tab;

/**
 * Abstract class of algorithm tab controller
 */
public abstract class MainWindowAlgorithmTabController implements MainWindowAlgorithmTab {
    /** pointer to main app */
    protected MainApp mainApp;
    /** pointer to a controlled tab */
    protected Tab thisTab;

    public MainWindowAlgorithmTabController(MainApp mainAppPointer) {
        this.mainApp = mainAppPointer;
    }

    /**
     * Set pointer to controlled tab and bind event for select this tab
     * @param tabPointer controlled tab
     */
    public void setTabPointer(Tab tabPointer) {
        this.thisTab = tabPointer;
        thisTab.setOnSelectionChanged(event -> {
            if (thisTab.isSelected()) {
                onActivateTab();
            } else {
                onDeactivateTab();
            }
        });
    }

    /**
     * Invoking when user switch to this tab
     */
    public void onActivateTab() {
           mainApp.setActiveAlgorithmController(this);
    }

    /**
     * Invoking when user leave tab
     */
    public void onDeactivateTab() { }

    /**
     * Invoking when user change distance on slider
     * @param newDistance new distance value
     */
    public abstract void onDistanceChanged(int newDistance);

    /**
     * Invoking when user moves mouse over radar
     * @param x x-coordinate of mouse cursor
     * @param y y-coordinate of mouse cursor
     */
    @Override
    public abstract void onMouseMoveOnRadar(int x, int y);

    /**
     * Invoking when user moves mouse out from radar
     */
    @Override
    public void onMouseLeftRadar() { }

}
