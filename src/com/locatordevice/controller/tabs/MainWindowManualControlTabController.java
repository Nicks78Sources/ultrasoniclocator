package com.locatordevice.controller.tabs;

import com.locatordevice.MainApp;

import com.locatordevice.algorithms.ScanDataProcessor;
import com.locatordevice.comio.DeviceAnswerData;
import com.locatordevice.comio.LocatorConstants;
import com.locatordevice.comio.enums.DeviceSoundType;
import com.locatordevice.comio.enums.TransferType;
import com.locatordevice.comio.exceptions.DeviceIOException;
import com.locatordevice.comio.interfaces.LocatorDeviceDriver;

import static com.locatordevice.comio.LocatorConstants.HORIZONTAL_DISTANCE_BETWEEN_RECEIVERS;
import static com.locatordevice.comio.LocatorConstants.VERTICAL_DISTANCE_BETWEEN_RECEIVERS;

import com.locatordevice.controller.ScannedChannelsWindowController;
import com.locatordevice.view.Dialogs;
import com.locatordevice.view.Drawer2D;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.io.IOException;

/**
 * Controller for manual-control tab in main window.
 */
public class MainWindowManualControlTabController extends MainWindowAlgorithmTabController {
    /** displayed in azimuth-in-degree label when user input incorrect azimuth */
    private static final String INVALID_INPUTTED_AZIMUTH = "----";

    /** pointer to driver of locator device */
    private LocatorDeviceDriver deviceDriver;

    private DeviceInfoDrawer deviceInfoDrawer;

    @FXML
    private TextField azimuthTextField;
    @FXML
    private TextField periodsTextField;
    @FXML
    private ToggleButton ledToggleButton;
    @FXML
    private ComboBox<DeviceSoundType> soundTypeComboBox;
    @FXML
    private CheckBox showChannelsCheckBox;
    @FXML
    private CheckBox saveToFileCheckBox;
    @FXML
    private Button setAzimuthButton;
    @FXML
    private Button scanButton;
    @FXML
    private Button playSoundButton;
    @FXML
    private Button resetButton;
    @FXML
    private Label azimuthDegreeLabel;

    public MainWindowManualControlTabController(MainApp mainAppPointer) {
        super(mainAppPointer);

        deviceDriver = mainApp.getLocatorDeviceDriver();
        deviceInfoDrawer = new DeviceInfoDrawer(mainAppPointer);
    }

    @Override
    public void onActivateTab() {
        super.onActivateTab();

        onDistanceChanged(mainApp.getMainWindowController().getSelectedDistance());
        deviceInfoDrawer.drawInfo();
    }

    @Override
    public void onDistanceChanged(int newDistance) { }

    @Override
    public void onMouseMoveOnRadar(int x, int y) { }

    @FXML
    private void initialize() {
        // set items in sound type combo box
        this.soundTypeComboBox.getItems().addAll(
                DeviceSoundType.SILENT,
                DeviceSoundType.F_4800HZ,
                DeviceSoundType.F_6400HZ,
                DeviceSoundType.SHOOT);
        this.soundTypeComboBox.setValue(DeviceSoundType.SILENT);
    }

    /**
     * Converting locator's antenna azimuth from steps to degree
     * and changed azimuth-in-degree label value
     * Invoking when user change value in azimuth text field
     * If input is incorrect set azimuth-in-degree label text to INVALID_INPUTTED_AZIMUTH
     */
    @FXML
    private void azimuthTextChanged() {
        int inputtedAzimuth = getInputtedAzimuth();
        if (inputtedAzimuth != -1) {
            Double azimuth = LocatorConstants.AZIMUTH_STEP * (inputtedAzimuth - LocatorConstants.ZERO_AZIMUTH);
            azimuthDegreeLabel.setText(azimuth.toString() + "°");
        } else {
            azimuthDegreeLabel.setText(INVALID_INPUTTED_AZIMUTH);
        }
    }

    @FXML
    private void setAzimuthButtonClick() {
        lockControls(true);

        int azimuth = getInputtedAzimuth();
        if (azimuth == -1) {
            Dialogs.warningDialog("Inputted azimuth is incorrect.\n" +
                    "Please, input correct value and try again.");
        } else {
            try {
                deviceDriver.setAzimuth(azimuth);
            } catch (DeviceIOException e) {
                Dialogs.errorDialog("Azimuth has not changed.", e.getMessage());
            }
        }

        lockControls(false);
    }

    @FXML
    private void scanButtonClick() {
        lockControls(true);

        DeviceAnswerData deviceAnswerData;

        int periods = getInputtedPeriodsOfRadiation();
        if (periods == -1) {
            Dialogs.warningDialog("Inputted periods of radiation is incorrect.\n" +
                    "Please, input correct value and try again.");
        } else {
            try {
                deviceAnswerData = deviceDriver.scanSpace(
                        TransferType.getTransferTypeByDistance(mainApp
                                .getMainWindowController().getSelectedDistance()), periods);
            } catch (DeviceIOException e) {
                Dialogs.errorDialog("Failed to scan space.", e.getMessage());
                lockControls(false);
                return;
            }

            processScanOptions(deviceAnswerData);
        }

        lockControls(false);
    }

    @FXML
    private void ledToggleButtonClick() {
        lockControls(true);

        try {
            deviceDriver.setLed(!deviceDriver.getLedState());
        } catch (DeviceIOException e) {
            Dialogs.errorDialog("Cannot change LED state", e.getMessage());
        }
        if (deviceDriver.getLedState()) {
            ledToggleButton.setText("ON");
        } else {
            ledToggleButton.setText("OFF");
        }

        lockControls(false);
    }

    @FXML
    private void soundButtonClick() {
        lockControls(true);

        try {
            deviceDriver.makeSound(getSelectedSoundType());
        } catch (DeviceIOException e) {
            Dialogs.errorDialog("An error occurred when trying to play a sound on the device.", e.getMessage());
        }

        lockControls(false);
    }

    /**
     * Reset locator device and UI values
     */
    @FXML
    private void resetButtonClick() {
        lockControls(true);
        azimuthTextField.setText("127");
        azimuthDegreeLabel.setText("0.0°");
        periodsTextField.setText("1");
        ledToggleButton.setText("OFF");
        ledToggleButton.setSelected(false);

        try {
            deviceDriver.reset();
        } catch (DeviceIOException e) {
            Dialogs.errorDialog("Cannot reset the device.", e.getMessage());
        }

        lockControls(false);
    }

    /**
     * Processing scan data corresponding to user settings
     * @param deviceAnswerData data from locator
     */
    private void processScanOptions(DeviceAnswerData deviceAnswerData) {
        if (showChannelsFlag()) {
            try {
                ScannedChannelsWindowController.showScanChannelsInWindow(
                        ScanDataProcessor.getScanData(deviceAnswerData));
            } catch (IOException e) {
                Dialogs.errorDialog(e.getMessage());
            }
        }
        if (saveToFileFlag()) {
            try {
                deviceAnswerData.saveToFile();
            } catch (IOException e) {
                Dialogs.errorDialog("Cannot save scan data to file.", e.getMessage());
            }
        }
    }

    /**
     * @return azimuth in steps set by user or -1 if value is incorrect
     */
    private int getInputtedAzimuth() {
        int azimuth;
        try {
            azimuth = Integer.parseInt(azimuthTextField.getText());
            if (azimuth < LocatorConstants.MINIMAL_AZIMUTH || azimuth > LocatorConstants.MAXIMAL_AZIMUTH) {
                azimuth = -1;
            }
        }
        catch(NumberFormatException e) {
            azimuth = -1;
        }
        return azimuth;
    }

    /**
     * @return periods of radiation set by user or -1 if value is incorrect
     */
    private int getInputtedPeriodsOfRadiation() {
        int periods;
        try {
            periods = Integer.parseInt(periodsTextField.getText());
            if (periods < LocatorConstants.MINIMUM_PERIODS_OF_RADIATION
                    || periods > LocatorConstants.MAXIMUM_PERIODS_OF_RADIATION) {
                periods = -1;
            }
        }
        catch(NumberFormatException e) {
            periods = -1;
        }
        return periods;
    }

    private DeviceSoundType getSelectedSoundType() {
        return soundTypeComboBox.getValue();
    }

    private boolean showChannelsFlag() {
        return showChannelsCheckBox.isSelected();
    }

    private boolean saveToFileFlag() {
        return saveToFileCheckBox.isSelected();
    }

    /**
     * Lock / unlock controls
     * Using for lock controls while command processing
     * and unlock, when command completed.
     * @param enable set disable to controls
     */
    public void lockControls(boolean enable){
        setAzimuthButton.setDisable(enable);
        scanButton.setDisable(enable);
        playSoundButton.setDisable(enable);
        ledToggleButton.setDisable(enable);
        resetButton.setDisable(enable);
    }

    /**
     * Inner class for drawing information about locator device
     * on radar's canvas in manual control mode
     */
    private class DeviceInfoDrawer {
        private static final int RECEIVER_DIAMETER = 15; // mm

        private static final int ANTENNA_DIAGRAM_X_START = 50;
        private static final int ANTENNA_DIAGRAM_Y_START = 50;
        private static final int ANTENNA_DISTANCE_TO_RECEIVER_FROM_BORDER = 15;
        private static final int ANTENNA_FOOTNOTE_PROTRUSION = 15;
        private static final int ANTENNA_COEFFICIENT = 2;

        private static final int INFO_TEXT_X_START = 25;
        private static final int INFO_TEXT_Y_START = 250;
        private static final int INFO_TEXT_Y_SHIFT = 35;

        private final Color backgroundColor = Color.rgb(37, 81, 67);

        private final String[] info = {
                "Operating  frequency  F = 32.53  kHz    (T = 30.74085  mks)",
                "Wavelength  at  v = 340  m/s :  λ = 10.45189  mm",
                "Sampling  frequency  fs = 5∙F = 162.65  kHz",
                "Step  rotation  of  antenna :  1.5 °"
        };

        /** pointer to radar drawer */
        private Drawer2D drawer2D;
        /** font for drawing information about device */
        private Font infoFont;

        public DeviceInfoDrawer(MainApp mainAppPointer) {
            drawer2D = mainAppPointer.getDrawer2D();
            infoFont = Font.loadFont("file:resources/fonts/XiomaraScript.ttf", 25);
        }

        public void drawInfo() {
            drawer2D.setColor(backgroundColor);
            drawer2D.fillRadar();
            drawer2D.setColor(Color.WHITE);
            drawAntenna();
            drawTextInfo();
        }

        private void drawAntenna() {
            int receiversDistanceToBorder = ANTENNA_COEFFICIENT * ANTENNA_DISTANCE_TO_RECEIVER_FROM_BORDER;
            int receiversStartX = ANTENNA_DIAGRAM_X_START + receiversDistanceToBorder;
            int receiversStartY = ANTENNA_DIAGRAM_Y_START + receiversDistanceToBorder;
            int receiversDistanceX = ANTENNA_COEFFICIENT * HORIZONTAL_DISTANCE_BETWEEN_RECEIVERS;
            int receiversDistanceY = ANTENNA_COEFFICIENT * VERTICAL_DISTANCE_BETWEEN_RECEIVERS;
            int receiversFinishX = receiversStartX + receiversDistanceX;
            int receiversFinishY = receiversStartY + receiversDistanceY;
            int receiverDiameter = ANTENNA_COEFFICIENT * RECEIVER_DIAMETER;
            int footnoteLength = receiversDistanceToBorder + ANTENNA_COEFFICIENT * ANTENNA_FOOTNOTE_PROTRUSION;
            int footnoteTextMargin = ANTENNA_COEFFICIENT * 5;
            final int SYMBOL_WIDTH = 5;
            final int SYMBOL_HEIGHT = 10;

            drawer2D.drawRectangle(ANTENNA_DIAGRAM_X_START, ANTENNA_DIAGRAM_Y_START,
                    2 * receiversDistanceToBorder + receiversDistanceX,
                    2 * receiversDistanceToBorder + receiversDistanceY);

            drawer2D.drawCircle(receiversStartX + receiversDistanceX / 2, receiversStartY + receiversDistanceY / 2, receiverDiameter);
            drawer2D.drawCircle(receiversStartX, receiversStartY, receiverDiameter);
            drawer2D.drawCircle(receiversFinishX, receiversStartY, receiverDiameter);
            drawer2D.drawCircle(receiversStartX, receiversFinishY, receiverDiameter);
            drawer2D.drawCircle(receiversFinishX, receiversFinishY, receiverDiameter);

            drawer2D.drawLine(receiversFinishX, receiversStartY, receiversFinishX + footnoteLength, receiversStartY);
            drawer2D.drawLine(receiversFinishX, receiversFinishY, receiversFinishX + footnoteLength, receiversFinishY);
            drawer2D.drawLine(receiversFinishX + footnoteLength - footnoteTextMargin, receiversStartY, receiversFinishX + footnoteLength - footnoteTextMargin, receiversFinishY);
            drawer2D.drawText(String.format("%d mm", VERTICAL_DISTANCE_BETWEEN_RECEIVERS),
                    receiversFinishX + footnoteLength - footnoteTextMargin + SYMBOL_WIDTH, receiversStartY + receiversDistanceY / 2);

            drawer2D.drawLine(receiversStartX, receiversStartY, receiversStartX, receiversStartY - footnoteLength);
            drawer2D.drawLine(receiversFinishX, receiversStartY, receiversFinishX, receiversStartY - footnoteLength);
            drawer2D.drawLine(receiversStartX, receiversStartY - footnoteLength + footnoteTextMargin, receiversFinishX, receiversStartY - footnoteLength + footnoteTextMargin);
            drawer2D.drawText(String.format("%d mm", HORIZONTAL_DISTANCE_BETWEEN_RECEIVERS),
                    receiversStartX + receiversDistanceX / 2 - SYMBOL_WIDTH*4, receiversStartY - footnoteLength + footnoteTextMargin - SYMBOL_HEIGHT);
        }

        private void drawTextInfo() {
            drawer2D.setFont(infoFont);
            for (int i = 0; i < info.length; i++) {
                drawer2D.drawText(info[i], INFO_TEXT_X_START, INFO_TEXT_Y_START + i*INFO_TEXT_Y_SHIFT);
            }
            drawer2D.resetFont();
        }

    }

}
