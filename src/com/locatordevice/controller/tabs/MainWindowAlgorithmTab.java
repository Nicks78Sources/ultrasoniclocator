package com.locatordevice.controller.tabs;

public interface MainWindowAlgorithmTab {
    void onActivateTab();
    void onDeactivateTab();
    void onDistanceChanged(int newDistance);
    void onMouseMoveOnRadar(int x, int y);
    void onMouseLeftRadar();
}
