package com.locatordevice.controller.tabs;

import com.locatordevice.MainApp;
import com.locatordevice.algorithms.Algorithm2D;
import com.locatordevice.algorithms.MultiScanAlgorithm2D;
import com.locatordevice.comio.LocatorConstants;
import com.locatordevice.comio.exceptions.DeviceIOException;
import com.locatordevice.view.Dialogs;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import javafx.concurrent.Task;

/**
 * Controller for 2D Multi scan tab of main window
 * @version 1.0
 * @author Morhun N.G.
 */
public class MainWindowMultiScan2DTabController extends MainWindowAlgorithmTabController {

    /** pause between scanning cycles */
    protected static final int AUTORUN_DELAY_MS = 1000;

    /** algorithm instance */
    private MultiScanAlgorithm2D multiScan2D;
    /** flag shows whether algorithm running */
    private boolean isRunning = false;
    /** thread in which the algorithm is executed */
    private Thread algorithmThread = null;
    /** task which runs in algorithm's thread */
    private Task algorithmTask;
    /** last coordinates of mouse cursor on radar
     * if mouse left radar, then assignment negative values */
    private int lastMouseXCoordinateOnRadar;
    private int lastMouseYCoordinateOnRadar;

    @FXML
    private TextField studiedAreaTextField;
    @FXML
    private TextField viewingAngleTextField;
    @FXML
    private TextField periodsTextField;
    @FXML
    private CheckBox autorunCheckBox;
    @FXML
    private Button startStopButton;

    public MainWindowMultiScan2DTabController(MainApp mainAppPointer) {
        super(mainAppPointer);

        multiScan2D = new MultiScanAlgorithm2D(mainAppPointer, this);
    }

    @Override
    public void onActivateTab() {
        super.onActivateTab();

        redrawRadar(mainApp.getMainWindowController().getSelectedDistance());
    }

    @Override
    public void onDeactivateTab() {
        multiScan2D.sendInterruptSignal();
    }

    @Override
    public void onDistanceChanged(int newDistance) {
         multiScan2D.redrawRadar(newDistance);
    }

    @Override
    public void onMouseMoveOnRadar(int x, int y) {
        lastMouseXCoordinateOnRadar = x;
        lastMouseYCoordinateOnRadar = y;
        multiScan2D.drawMouseCoordinates(x, y);
    }

    @Override
    public void onMouseLeftRadar() {
        multiScan2D.eraseMouseCoordinates();
        lastMouseXCoordinateOnRadar = -1;
        lastMouseYCoordinateOnRadar = -1;
    }

    /**
     * Redraws radar with specified maximum distance
     * @param distance maximum distance on radar
     */
    public void redrawRadar(int distance) {
        if (thisTab.isSelected()) {
            multiScan2D.redrawRadar(distance);
            if (lastMouseXCoordinateOnRadar != -1) {
                multiScan2D.drawMouseCoordinates(lastMouseXCoordinateOnRadar, lastMouseYCoordinateOnRadar);
            }
        }
    }

    private void stopAlgorithm() {
        multiScan2D.sendInterruptSignal();
        isRunning = false;
        startStopButton.setText("Start");
        lockUnlockControlsWhileAutorun();
    }

    @FXML
    private void startStopButtonClick() {
        if (isRunning) {
            stopAlgorithm();
        } else {
            int periods = getInputtedPeriodsOfRadiation();
            double viewingAngle = getInputtedViewingAngle();
            double studiedArea = getStudiedArea();

            if (periods == -1) {
                Dialogs.warningDialog("Inputted periods of radiation is incorrect.\n" +
                        "Please, input correct value and try again.");
                return;
            }
            if (viewingAngle == -1) {
                Dialogs.warningDialog("Inputted viewing angle is incorrect.\n" +
                        "Please, input correct value and try again.");
                return;
            }
            if (studiedArea == -1 || studiedArea < 3) {
                Dialogs.warningDialog("Inputted studied area is incorrect.\n" +
                        "Please, input correct value ant try again");
                return;
            }

            startStopButton.setText("Interrupt");

            MainWindowMultiScan2DTabController parentThis = this;
            algorithmTask = new Task() {
                @Override
                public Void call() {
                    do {
                        try {
                            multiScan2D.runAlgorithm(mainApp.getMainWindowController().getSelectedDistance(),
                                    periods,
                                    studiedArea,
                                    viewingAngle);
                        } catch (DeviceIOException e) {
                            Platform.runLater(() -> parentThis.algorithmError(e.getMessage()));
                            break;
                        }
                        if (autorunCheckBox.isSelected()) {
                            try {
                                Thread.sleep(AUTORUN_DELAY_MS);
                            } catch (InterruptedException e) {
                                break;
                            }
                        }
                    } while (autorunCheckBox.isSelected() && ! multiScan2D.isInterrupted()); // waiting for interrupt if autorun mode on

                    Platform.runLater(parentThis::stopAlgorithm);

                    return null;
                }
            };

            multiScan2D.prepareToStart();
            isRunning = true;

            algorithmThread = new Thread(algorithmTask);
            algorithmThread.setDaemon(true);
            algorithmThread.start();

            lockUnlockControlsWhileAutorun();
        }
    }

    /**
     * Shows error which occurred when algorithm running
     * @param errorText error message
     */
    public void algorithmError(String errorText) {
        Dialogs.errorDialog(errorText);
    }

    /**
     * Lock some controls which must be locked, while autorun looping,
     * and unlock them when autorun is off
     */
    private void lockUnlockControlsWhileAutorun() {
        periodsTextField.setDisable(isRunning);
        viewingAngleTextField.setDisable(isRunning);
        studiedAreaTextField.setDisable(isRunning);
        autorunCheckBox.setDisable(isRunning);
        mainApp.getMainWindowController().setDistanceSliderDisable(isRunning);
    }


    /**
     * @return studied area in degrees which set by user or -1 if value is incorrect
     */
    private double getStudiedArea() {
        double studiedAreaAngle;
        try {
            studiedAreaAngle = Double.parseDouble(studiedAreaTextField.getText());
            if (studiedAreaAngle < MultiScanAlgorithm2D.MINIMUM_STUDIED_AREA_ANGLE
                    || studiedAreaAngle > MultiScanAlgorithm2D.MAXIMUM_STUDIED_AREA_ANGLE) {
                studiedAreaAngle = -1;
            }
        } catch(NumberFormatException e) {
            studiedAreaAngle = -1;
        }

        return studiedAreaAngle;
    }

    /**
     * @return viewing angle in degrees which set by user or -1 if value is incorrect
     */
    private double getInputtedViewingAngle() {
        double viewingAngle;
        try {
            viewingAngle = Double.parseDouble(viewingAngleTextField.getText());
            if (viewingAngle < Algorithm2D.MINIMUM_VIEWING_ANGLE_DEGREES
                    || viewingAngle > Algorithm2D.MAXIMUM_VIEWING_ANGLE_DEGREES) {
                viewingAngle = -1;
            }
        }
        catch (NumberFormatException e) {
            viewingAngle = -1;
        }
        return viewingAngle;
    }

    /**
     * @return periods of radiation set by user or -1 if value is incorrect
     */
    private int getInputtedPeriodsOfRadiation() {
        int periods;
        try {
            periods = Integer.parseInt(periodsTextField.getText());
            if (periods < LocatorConstants.MINIMUM_PERIODS_OF_RADIATION
                    || periods > LocatorConstants.MAXIMUM_PERIODS_OF_RADIATION) {
                periods = -1;
            }
        }
        catch(NumberFormatException e) {
            periods = -1;
        }
        return periods;
    }

}
