package com.locatordevice.controller.tabs;

import com.locatordevice.MainApp;
import com.locatordevice.algorithms.Algorithm2D;
import com.locatordevice.algorithms.SingleScanAlgorithm2D;
import com.locatordevice.comio.LocatorConstants;
import com.locatordevice.comio.exceptions.DeviceIOException;
import com.locatordevice.controller.ScannedChannelsWindowController;
import com.locatordevice.view.Dialogs;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.IOException;

/**
 * Controller for 2D Single scan tab of main window
 * @version 1.0
 * @author Morhun N.G.
 */
public class MainWindowSingleScan2DTabController extends MainWindowAlgorithmTabController {

    private static final int MAXIMAL_AZIMUTH_IN_DEGREE = 90;
    private static final int MINIMAL_AZIMUTH_IN_DEGREE = -90;

    /** algorithm instance */
    private SingleScanAlgorithm2D singleScan2D;
    /** flag which show whether autorun is turned on */
    private boolean isAutoran = false;
    /** last number of periods which was inputted by user */
    private int periods;
    /** last coordinates of mouse cursor on radar
     * if mouse left radar, then assignment negative values */
    private int lastMouseXCoordinateOnRadar;
    private int lastMouseYCoordinateOnRadar;

    @FXML
    private TextField azimuthTextField;
    @FXML
    private TextField periodsTextField;
    @FXML
    private TextField viewingAngleTextField;
    @FXML
    private CheckBox autorunCheckBox;
    @FXML
    private CheckBox averagingCheckBox;
    @FXML
    private CheckBox filteringCheckBox;
    @FXML
    private Button setAzimuthButton;
    @FXML
    private Button startStopButton;
    @FXML
    private Button showChannelsButton;
    @FXML
    private Button exportInRangefinderButton;

    public MainWindowSingleScan2DTabController(MainApp mainAppPointer) {
        super(mainAppPointer);

        singleScan2D = new SingleScanAlgorithm2D(mainAppPointer);
    }

    @Override
    public void onActivateTab() {
        super.onActivateTab();

        singleScan2D.redrawRadar(
                mainApp.getMainWindowController().getSelectedDistance());
    }

    @Override
    public void onDeactivateTab() {
        if (isAutoran) {
            stopAutorun();
        }
    }

    @Override
    public void onDistanceChanged(int newDistance) {
        singleScan2D.redrawRadar(newDistance);
    }

    @Override
    public void onMouseMoveOnRadar(int x, int y) {
        lastMouseXCoordinateOnRadar = x;
        lastMouseYCoordinateOnRadar = y;
        singleScan2D.drawMouseCoordinates(x, y);
    }

    @Override
    public void onMouseLeftRadar() {
        singleScan2D.eraseMouseCoordinates();
        lastMouseXCoordinateOnRadar = -1;
        lastMouseYCoordinateOnRadar = -1;
    }

    @FXML
    private void startStopButtonClick() {
        if (isAutoran) {
            stopAutorun();
        } else {
            periods = getInputtedPeriodsOfRadiation();
            if (periods == -1) {
                Dialogs.warningDialog("Inputted periods of radiation is incorrect.\n" +
                        "Please, input correct value and try again.");
                return;
            }
            double inputtedViewingAngle = getInputtedViewingAngle();
            if (inputtedViewingAngle == -1) {
                Dialogs.warningDialog("Inputted viewing angle is incorrect.\n" +
                        "Please, input correct value and try again.");
                return;
            }
            singleScan2D.setViewingAngle(inputtedViewingAngle);

            if (autorunCheckBox.isSelected()) {
                startAutorun();
            } else {
                // single scan
                try {
                    singleScan2D.runAlgorithm(
                            mainApp.getMainWindowController().getSelectedDistance(), periods);
                } catch (DeviceIOException e) {
                    Dialogs.errorDialog("An error occurred while getting data from device.", e.getMessage());
                }
            }
        }
    }

    /**
     * Actions which must doing when autorun is active
     */
    private void autorunAction() {
        if (thisTab.isSelected()) {
            try {
                singleScan2D.runAlgorithm(
                        mainApp.getMainWindowController().getSelectedDistance(), periods);
                if (lastMouseXCoordinateOnRadar >= 0) {
                    singleScan2D.drawMouseCoordinates(lastMouseXCoordinateOnRadar, lastMouseYCoordinateOnRadar);
                }
            } catch (DeviceIOException e) {
                stopAutorun();
                Dialogs.errorDialog("An error occurred while getting data from device.", e.getMessage());
            }
        }

        if (isAutoran) {
            Platform.runLater(this::autorunAction);
        }
    }

    /**
     * Starts autoran and lock some control which must be locked while it working
     * @throws RuntimeException if autorun is turned on
     */
    private void startAutorun() {
        if (isAutoran) {
            throw new RuntimeException();
        }
        isAutoran = true;
        lockUnlockControlsWhileAutorun();
        startStopButton.setText("Stop");
        Platform.runLater(this::autorunAction);
    }

    /**
     * Stops autorun and unlock controls
     */
    private void stopAutorun() {
        isAutoran = false;
        lockUnlockControlsWhileAutorun();
        startStopButton.setText("Start");
    }

    /**
     * Lock some controls which must be locked, while autorun looping,
     * and unlock them when autorun is off
     */
    private void lockUnlockControlsWhileAutorun() {
        periodsTextField.setDisable(isAutoran);
        autorunCheckBox.setDisable(isAutoran);
        showChannelsButton.setDisable(isAutoran);
        setAzimuthButton.setDisable(isAutoran);
        azimuthTextField.setDisable(isAutoran);
        viewingAngleTextField.setDisable(isAutoran);
        exportInRangefinderButton.setDisable(isAutoran);
        mainApp.getMainWindowController().setDistanceSliderDisable(isAutoran);
    }

    @FXML
    private void setAzimuthButtonClick() {
        double azimuth = getInputtedAzimuthDegrees();
        if (azimuth == -1) {
            Dialogs.warningDialog("Inputted azimuth is incorrect.\n" +
                    "Please, input correct value and try again.");
        } else {
            try {
                singleScan2D.setAzimuth(azimuth);
                singleScan2D.setViewingAngle(getInputtedViewingAngle());
            } catch (DeviceIOException e) {
                Dialogs.errorDialog("Azimuth has not changed.", e.getMessage());
            }
        }
    }

    /**
     * @return azimuth in degrees which set by user or -1 if value is incorrect
     */
    private double getInputtedAzimuthDegrees() {
        double azimuth;
        try {
            azimuth = Double.parseDouble(azimuthTextField.getText());
            if (azimuth < MINIMAL_AZIMUTH_IN_DEGREE || azimuth > MAXIMAL_AZIMUTH_IN_DEGREE) {
                azimuth = -1;
            }
        }
        catch(NumberFormatException e) {
            azimuth = -1;
        }
        return azimuth;
    }

    /**
     * @return viewing angle in degrees which set by user or -1 if value is incorrect
     */
    private double getInputtedViewingAngle() {
        double viewingAngle;
        try {
            viewingAngle = Double.parseDouble(viewingAngleTextField.getText());
            if (viewingAngle < Algorithm2D.MINIMUM_VIEWING_ANGLE_DEGREES
                    || viewingAngle > Algorithm2D.MAXIMUM_VIEWING_ANGLE_DEGREES) {
                viewingAngle = -1;
            }
        }
        catch (NumberFormatException e) {
            viewingAngle = -1;
        }
        return viewingAngle;
    }

    /**
     * @return periods of radiation set by user or -1 if value is incorrect
     */
    private int getInputtedPeriodsOfRadiation() {
        int periods;
        try {
            periods = Integer.parseInt(periodsTextField.getText());
            if (periods < LocatorConstants.MINIMUM_PERIODS_OF_RADIATION
                    || periods > LocatorConstants.MAXIMUM_PERIODS_OF_RADIATION) {
                periods = -1;
            }
        }
        catch(NumberFormatException e) {
            periods = -1;
        }
        return periods;
    }

    @FXML
    private void onAveragingChanged() {
        singleScan2D.setAveraging(averagingCheckBox.isSelected());
        singleScan2D.redrawRadar(mainApp.getMainWindowController().getSelectedDistance());
    }

    @FXML
    private void showChannelsButtonClick() {
        if (singleScan2D.getLastScanRawData() == null) {
            Dialogs.warningDialog("Please, start algorithm before this action");
        } else {
            try {
                ScannedChannelsWindowController.showScanChannelsInWindow(singleScan2D.getLastScanRawData());
            } catch (IOException e) {
                Dialogs.errorDialog(e.getMessage());
            }
        }
    }

    /**
     * Export data of last scan in rangefinder algorithm
     * and switch tab to rangefinder
     */
    @FXML
    private void exportInRangefinderButtonClick() {
        if (singleScan2D.getLastScanRawData() == null) {
            Dialogs.warningDialog("Nothing to import in rangefinder.\nMake scan and try again.");
        } else {
            mainApp.getRangefinderTabController().importScanData(
                    singleScan2D.getLastScanRawData());
            ((TabPane) mainApp.getPrimaryStage().getScene().lookup("#controlTabPane"))
                    .getSelectionModel().select(mainApp.getRangefinderTabController().thisTab);
        }
    }

}
