package com.locatordevice.controller.tabs;

import com.locatordevice.MainApp;
import com.locatordevice.algorithms.RangefinderAlgorithm;
import com.locatordevice.algorithms.ScanDataProcessor;
import com.locatordevice.beans.ScanData;
import com.locatordevice.comio.DeviceAnswerData;
import com.locatordevice.comio.exceptions.ScanFileFormatException;
import com.locatordevice.controller.ScannedChannelsWindowController;
import com.locatordevice.controller.tabs.MainWindowAlgorithmTabController;
import com.locatordevice.utils.DateUtil;
import com.locatordevice.view.Dialogs;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;

/**
 * Controller for offline mode tab of main window
 * @version 1.0
 * @author Morhun N.G.
 */
public class MainWindowOfflineModeTabController extends MainWindowAlgorithmTabController {
    /** data of scan which has been loaded */
    private ScanData loadedScanData = null;
    /** used as scan data drawer */
    private RangefinderAlgorithm rangefinderAsSimpleScanDataDrawer;

    private static final String PATH_SEPARATOR = System.getProperty("file.separator");
    private static final String PATH_TO_SCANS = "." + PATH_SEPARATOR + "scans" + PATH_SEPARATOR;

    public MainWindowOfflineModeTabController(MainApp mainAppPointer) {
        super(mainAppPointer);

        rangefinderAsSimpleScanDataDrawer = new RangefinderAlgorithm(mainApp);
    }

    @FXML
    private Label currentScanTimeLabel;
    @FXML
    private Button showChannelsButton;
    @FXML
    private Button drawOnRadarButton;

    @Override
    public void onDistanceChanged(int newDistance) {
        rangefinderAsSimpleScanDataDrawer.redrawDistancesDiagram(newDistance);
    }

    @Override
    public void onMouseMoveOnRadar(int x, int y) {
        rangefinderAsSimpleScanDataDrawer.drawMouseCoordinates(x, y);
    }

    @FXML
    private void loadButtonClick() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "Scan files (yyyy-MM-dd_HH-mm-ss-SSS.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setInitialDirectory(new File(PATH_TO_SCANS));
        File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());

        if (file != null) {
            loadFile(file);
        }
    }

    /**
     * Loading data from file
     * @param file which user had selected
     */
    private void loadFile(File file) {
        DeviceAnswerData rawData;
        try {
            rawData = new DeviceAnswerData(file.toString());
        } catch (IOException e) {
            Dialogs.errorDialog("Cannot load file: \"" + file.toString() + "\"", e.getMessage());
            return;
        } catch (ScanFileFormatException e) {
            Dialogs.errorDialog("Selected file has wrong format or was damaged.\nPlease choose correct file.");
            return;
        }

        loadedScanData = ScanDataProcessor.getScanData(rawData);

        currentScanTimeLabel.setText(DateUtil.formatScanDate(rawData.getScanTime()));
    }

    /**
     * Show warning with recommendation to select scan file for reading
     */
    private void showSelectScanFileWarning() {
        Dialogs.warningDialog("Please select file before this action!");
    }

    /**
     * Show scan data in separate window
     */
    @FXML
    private void showChannelsButtonClick() {
        showChannelsButton.setDisable(true);

        if (loadedScanData != null) {
            try {
                ScannedChannelsWindowController.showScanChannelsInWindow(loadedScanData);
            } catch (IOException e) {
                Dialogs.errorDialog(e.getMessage());
            }
        } else {
            showSelectScanFileWarning();
        }

        showChannelsButton.setDisable(false);
    }

    /**
     * Draw scan data on radar's canvas
     */
    @FXML
    private void drawOnRadarButtonClick() {
        drawOnRadarButton.setDisable(true);

        if (loadedScanData != null) {
            rangefinderAsSimpleScanDataDrawer.setLastScanRawData(loadedScanData);
            mainApp.getMainWindowController().setSelectedDistance(loadedScanData.getChannelSize() / 1024); // channelSize / 1024 gives ~ distance in meters
        } else {
            showSelectScanFileWarning();
        }

        drawOnRadarButton.setDisable(false);
    }

}
