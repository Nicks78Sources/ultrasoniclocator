package com.locatordevice.controller;

import com.locatordevice.MainApp;
import com.locatordevice.algorithms.ScanDataProcessor;
import com.locatordevice.beans.ScanData;
import com.locatordevice.comio.DeviceAnswerData;
import com.locatordevice.utils.DateUtil;
import com.locatordevice.view.Dialogs;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Controller for window which shows channels of scan data
 * Many windows of this type can be opened simultaneously
 * @version 1.0
 * @author Morhun N.G.
 */
public class ScannedChannelsWindowController {
    private ScanData data;
    private Stage stage;

    /**
     * @param stage stage of controlled window
     * @param scanData data of scan for visualization
     */
    public ScannedChannelsWindowController(Stage stage, ScanData scanData) {
        this.stage = stage;
        this.data = scanData;
    }

    /**
     * @param stage stage of controlled window
     * @param rawData raw data of scan for visualization
     */
    public ScannedChannelsWindowController(Stage stage, DeviceAnswerData rawData) {
        this.stage = stage;
        this.data = ScanDataProcessor.getScanData(rawData);
    }

    @FXML
    private Button saveButton;
    @FXML
    private TextArea dataTextArea;

    @FXML
    private void initialize() {
        dataTextArea.setEditable(false);
        showData();
    }

    @FXML
    private void onCloseButtonClick() {
        stage.close();
    }

    /**
     * Saves table from window into file
     * File: ./scans/c_YYYY.MM.DD_HH-MM-SS-MSS.txt
     */
    @FXML
    private void onSaveButtonClick() {
        try {
            ScanDataProcessor.saveToFile(data);
        } catch (IOException e) {
            Dialogs.errorDialog("Can not save data.", e.getMessage());
            return;
        }
        saveButton.setDisable(true);
        saveButton.setText("Saved");
    }

    /**
     * Prints data of scan as table in text area
     */
    private void showData() {
        dataTextArea.setText(ScanDataProcessor.getDecodedDataTable(data));
        dataTextArea.setScrollTop(0);
    }

    /**
     * Create controller and load & show window with scan channels
     * @param scanData data of scan
     * @return stage of window or null on error
     * @throws IOException
     */
    public static Stage showScanChannelsInWindow(ScanData scanData) throws IOException {
        Stage showChannelsStage = new Stage();
        showChannelsStage.setTitle(DateUtil.formatScanDate(scanData.getScanTime()));
        showChannelsStage.getIcons().add(new Image("/images/BitChannels.png"));
        //showChannelsStage.initOwner(primaryStage);

        FXMLLoader loader = new FXMLLoader();
        ScannedChannelsWindowController controller = new ScannedChannelsWindowController(showChannelsStage, scanData);
        loader.setController(controller);
        loader.setLocation(MainApp.class.getResource("/fxml/ScannedChannelsWindow.fxml"));

        AnchorPane windowContent = loader.load();

        Scene scene = new Scene(windowContent);
        showChannelsStage.setScene(scene);
        showChannelsStage.show();

        return showChannelsStage;
    }

}
