package com.locatordevice.controller;

import com.locatordevice.MainApp;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Slider;

/**
 * Controller for main window excluding algorithms tab
 * Each algorithm has own controller named MainWindow[AlgorithmName]TabController
 * @version 1.1
 * @author Morhun N.G.
 */
public class MainWindowController {
    /** pointer to main app */
    private MainApp mainApp;

    @FXML
    Canvas radarCanvas;
	@FXML
	private Slider distanceSlider;

    public MainWindowController(MainApp mainAppPointer) {
        this.mainApp = mainAppPointer;
    }

	@FXML
    private void initialize() {
        // add listener for distance change event
        distanceSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            int oldDistance = Math.round(oldValue.floatValue());
            int newDistance = Math.round(newValue.floatValue());
            if (newDistance != oldDistance) {
                distanceChanged(newDistance);
            }
        });

        // add listener for mouse move on canvas
        radarCanvas.setOnMouseMoved((mouseEvent) ->
                mouseMovedUnderRadar((int) mouseEvent.getX(), (int) mouseEvent.getY()));

        // add listener for mouse leave canvas
        radarCanvas.setOnMouseExited((mouseEvent) -> mouseLeftRadar());
	}

    /**
     * Listener for distance change event
     * @param newDistance new distance on slider
     */
    private void distanceChanged(int newDistance) {
        mainApp.getActiveAlgorithmController().onDistanceChanged(newDistance);
    }

    /**
     * Listener for mouse move event on radar's canvas
     * @param x x-coordinate of mouse
     * @param y y-coordinate of mouse
     */
    private void mouseMovedUnderRadar(int x, int y) {
        mainApp.getActiveAlgorithmController().onMouseMoveOnRadar(x, y);
    }

    /**
     * Listener for mouse leave event on radar's canvas
     */
    public void mouseLeftRadar() {
        mainApp.getActiveAlgorithmController().onMouseLeftRadar();
    }

    /**
     * Lock / unlock distance slider
     * @param disable disable flag
     */
    public void setDistanceSliderDisable(boolean disable) {
        distanceSlider.setDisable(disable);
    }

	/**
	 * @return distance that user has selected
	 */
	public int getSelectedDistance() {
		return (int) distanceSlider.getValue();
	}

    /**
     * Set current distance
     * @param newDistance new current distance
     */
    public void setSelectedDistance(int newDistance) {
        distanceSlider.setValue(newDistance);
    }

    public Canvas getRadarCanvas() {
        return radarCanvas;
    }

}
