package com.locatordevice.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

import com.locatordevice.comio.Communicator;

/**
 * Controller for dialog window of COM port selecting.
 * Result saving in portName field.
 * portName contain:
 *  - name of COM port if port selected
 *  - "None" if offline mode
 *  - null if user close the dialog or click exit button
 * @version 1.0
 * @author Morhun N.G.
 */
public class PortSelectDialogController {
	private String portName = null;
	private Stage stage;
	
	@FXML
	protected Button runButton;
	@FXML
	protected Button exitButton;
	@FXML
	protected ComboBox<String> portSelectComboBox;
	
	@FXML
    private void initialize() {
		this.portSelectComboBox.getItems().add("None");
		this.portSelectComboBox.getItems().add("Emulator");
		this.portSelectComboBox.getItems().addAll(Communicator.getCOMPorts());
		this.portSelectComboBox.setValue("None");
	}
	
	@FXML
	private void runButtonClick() {
		this.portName = this.portSelectComboBox.getValue();
		stage.close();
	}
	
	@FXML
	private void exitButtonClick() {
		this.portName = null;
		stage.close();
	}
	
	/**
	 * @return user's choose
	 */
	public String getPort() {
		return this.portName;
	}

	public void setStage(Stage dialogStage) {
		this.stage = dialogStage;
	}
}
