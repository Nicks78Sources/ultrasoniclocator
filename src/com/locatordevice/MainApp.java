package com.locatordevice;

import com.locatordevice.comio.Communicator;
import com.locatordevice.comio.exceptions.DeviceIOException;
import com.locatordevice.comio.interfaces.LocatorDeviceDriver;

import com.locatordevice.controller.PortSelectDialogController;
import com.locatordevice.controller.RootLayoutController;
import com.locatordevice.controller.MainWindowController;
import com.locatordevice.controller.tabs.*;

import com.locatordevice.beans.VirtualObject;
import com.locatordevice.emulator.DeviceEmulator;

import com.locatordevice.view.Dialogs;
import com.locatordevice.view.Drawer2D;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.List;

import java.io.IOException;

/**
 * Application start class
 * @version 1.1
 * @author Morhun N.G.
 */
public class MainApp extends Application {
    /** main container */
    private Stage primaryStage;
    /** application work mode */
    private boolean isOfflineMode = false;

    /** driver of locator device */
    private LocatorDeviceDriver locatorDeviceDriver = null;
    /** drawer for radar's canvas */
    private Drawer2D drawer2D;
    /** pointer to main window controller*/
    private MainWindowController mainWindowController;

    /** current algorithm tab controller */
    private MainWindowAlgorithmTabController activeAlgorithmController;
    /** pointer to rangefinder tab controller */
    private MainWindowRangefinderTabController rangefinderTabController;

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public LocatorDeviceDriver getLocatorDeviceDriver() {
        return locatorDeviceDriver;
    }

    public Drawer2D getDrawer2D() {
        return drawer2D;
    }

    public MainWindowController getMainWindowController() {
        return mainWindowController;
    }

    public MainWindowAlgorithmTab getActiveAlgorithmController() {
        return activeAlgorithmController;
    }

    public MainWindowRangefinderTabController getRangefinderTabController() {
        return this.rangefinderTabController;
    }

    public void setActiveAlgorithmController(MainWindowAlgorithmTabController activeAlgorithmController) {
        this.activeAlgorithmController = activeAlgorithmController;
    }

    /**
     * Entry point of project.
     * @param args arguments from console
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Application initialization
     * @param primaryStage main container
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;

        String port = devicePortSelectDialog();
        if (null == port) {
            System.exit(0);
        } else {
            switch (port) {
                case "None":
                    isOfflineMode = true;
                    break;
                case "Emulator":
                    locatorDeviceDriver = new DeviceEmulator(getSimulatedObjects());
                    break;
                default:
                    try {
                        locatorDeviceDriver = new Communicator(port);
                        locatorDeviceDriver.reset();
                    } catch (Exception e) {
                        Dialogs.errorDialog("Fatal error",
                                "An error occurred while initializing the device driver. " +
                                        "The application will be closed.",
                                e.getMessage());
                        System.exit(1);
                    }
                    break;
            }
        }

        buildMainWindow();

        this.primaryStage.show();
    }

    /**
     * Invoking when application closing
     */
    private void onAppClose() {
        if (!isOfflineMode) {
            try {
                locatorDeviceDriver.setTimeout(100);
                // locatorDeviceDriver.reset();
                locatorDeviceDriver.ledOff();
            } catch (DeviceIOException ignored) {}
            try {
                locatorDeviceDriver.resourcesFree();
            } catch (DeviceIOException e) {
                Dialogs.errorDialog("An error occurred while uploading the device driver.", e.getMessage());
            }
        }
    }

    /**
     * Compile main window
     */
    private void buildMainWindow() {
        primaryStage.setTitle("Locator device");
        primaryStage.getIcons().add(new Image("/images/Radar.png"));
        primaryStage.setResizable(false);

        BorderPane rootLayout = buildRootLayout();
        AnchorPane mainWindowContent = buildMainWindowContent();

        rootLayout.setCenter(mainWindowContent);
        Scene mainScene = new Scene(rootLayout);
        primaryStage.setScene(mainScene);

        primaryStage.setOnCloseRequest(windowEvent -> onAppClose());
    }

    /**
     * Compile root layout of main window
     * @return root layout
     */
    private BorderPane buildRootLayout() {
        FXMLLoader loader = new FXMLLoader();
        RootLayoutController rootLayoutController = new RootLayoutController(this);
        loader.setController(rootLayoutController);
        loader.setLocation(MainApp.class.getResource("/fxml/MainWindowRootLayout.fxml"));

        BorderPane rootLayout = null;
        try {
            rootLayout = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rootLayout;
    }

    /**
     * Compile all content of main window
     * @return content of main window including algorithms tabs
     */
    private AnchorPane buildMainWindowContent() {
        AnchorPane mainWindowContent = null;

        FXMLLoader loader = new FXMLLoader();
        mainWindowController = new MainWindowController(this);

        loader.setController(mainWindowController);
        loader.setLocation(MainApp.class.getResource("/fxml/MainWindow.fxml"));
        try {
            mainWindowContent = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        drawer2D = new Drawer2D(mainWindowController.getRadarCanvas());
        drawer2D.clearRadar();

        buildControlTabs(mainWindowContent);

        return mainWindowContent;
    }

    /**
     * Compile all control tabs
     * Each algorithm has their control tab
     * @param mainWindowContent content of window
     */
    private void buildControlTabs(final AnchorPane mainWindowContent) {
        if (isOfflineMode) {
            // offline mode tab
            FXMLLoader offlineModeTabLoader = new FXMLLoader();
            MainWindowAlgorithmTabController mainWindowOfflineModeTabController = new MainWindowOfflineModeTabController(this);
            offlineModeTabLoader.setController(mainWindowOfflineModeTabController);
            offlineModeTabLoader.setLocation(MainApp.class.getResource("/fxml/mainWindowTabs/MainWindowOfflineModeTab.fxml"));
            Tab offlineModeTab = new Tab("Offline mode");
            mainWindowOfflineModeTabController.setTabPointer(offlineModeTab);
            try {
                offlineModeTab.setContent(offlineModeTabLoader.load());
            } catch (IOException e) {
                e.printStackTrace();
            }
            ((TabPane) mainWindowContent.lookup("#controlTabPane")).getTabs().add(offlineModeTab);
        } else {
            // manual control tab
            FXMLLoader manualControlTabLoader = new FXMLLoader();
            MainWindowAlgorithmTabController mainWindowManualControlTabController = new MainWindowManualControlTabController(this);
            manualControlTabLoader.setController(mainWindowManualControlTabController);
            manualControlTabLoader.setLocation(MainApp.class.getResource("/fxml/mainWindowTabs/MainWindowManualControlTab.fxml"));
            Tab manualControlTab = new Tab("Manual control");
            mainWindowManualControlTabController.setTabPointer(manualControlTab);
            try {
                manualControlTab.setContent(manualControlTabLoader.load());
            } catch (IOException e) {
                e.printStackTrace();
            }
            ((TabPane) mainWindowContent.lookup("#controlTabPane")).getTabs().add(manualControlTab);

            // rangefinder tab
            FXMLLoader rangefinderTabLoader = new FXMLLoader();
            MainWindowAlgorithmTabController mainWindowRangefinderTabController = new MainWindowRangefinderTabController(this);
            rangefinderTabLoader.setController(mainWindowRangefinderTabController);
            rangefinderTabLoader.setLocation(MainApp.class.getResource("/fxml/mainWindowTabs/MainWindowRangefinderTab.fxml"));
            Tab rangefinderTab = new Tab("Rangefinder");
            mainWindowRangefinderTabController.setTabPointer(rangefinderTab);
            try {
                rangefinderTab.setContent(rangefinderTabLoader.load());
            } catch (IOException e) {
                e.printStackTrace();
            }
            ((TabPane) mainWindowContent.lookup("#controlTabPane")).getTabs().add(rangefinderTab);
            this.rangefinderTabController = (MainWindowRangefinderTabController) mainWindowRangefinderTabController;

            // singe scan 2D tab
            FXMLLoader singleScan2DTabLoader = new FXMLLoader();
            MainWindowAlgorithmTabController mainWindowSingleScan2DTabController = new MainWindowSingleScan2DTabController(this);
            singleScan2DTabLoader.setController(mainWindowSingleScan2DTabController);
            singleScan2DTabLoader.setLocation(MainApp.class.getResource("/fxml/mainWindowTabs/MainWindowSingleScan2DTab.fxml"));
            Tab singleScan2DTab = new Tab("Single scan 2D");
            mainWindowSingleScan2DTabController.setTabPointer(singleScan2DTab);
            try {
                singleScan2DTab.setContent(singleScan2DTabLoader.load());
            } catch (IOException e) {
                e.printStackTrace();
            }
            ((TabPane) mainWindowContent.lookup("#controlTabPane")).getTabs().add(singleScan2DTab);

            // multi scan 2D tab
            FXMLLoader multiScan2DTabLoader = new FXMLLoader();
            MainWindowAlgorithmTabController mainWindowMultiScan2DTabController = new MainWindowMultiScan2DTabController(this);
            multiScan2DTabLoader.setController(mainWindowMultiScan2DTabController);
            multiScan2DTabLoader.setLocation(MainApp.class.getResource("/fxml/mainWindowTabs/MainWindowMultiScan2DTab.fxml"));
            Tab multiScan2DTab = new Tab("Multi scan 2D");
            mainWindowMultiScan2DTabController.setTabPointer(multiScan2DTab);
            try {
                multiScan2DTab.setContent(multiScan2DTabLoader.load());
            } catch (IOException e) {
                e.printStackTrace();
            }
            ((TabPane) mainWindowContent.lookup("#controlTabPane")).getTabs().add(multiScan2DTab);

            // singe scan 3D tab
            FXMLLoader singleScan3DTabLoader = new FXMLLoader();
            MainWindowAlgorithmTabController mainWindowSingleScan3DTabController = new MainWindowSingleScan3DTabController(this);
            singleScan3DTabLoader.setController(mainWindowSingleScan3DTabController);
            singleScan3DTabLoader.setLocation(MainApp.class.getResource("/fxml/mainWindowTabs/MainWindowSingleScan3DTab.fxml"));
            Tab singleScan3DTab = new Tab("Single scan 3D");
            mainWindowSingleScan3DTabController.setTabPointer(singleScan3DTab);
            try {
                singleScan3DTab.setContent(singleScan3DTabLoader.load());
            } catch (IOException e) {
                e.printStackTrace();
            }
            ((TabPane) mainWindowContent.lookup("#controlTabPane")).getTabs().add(singleScan3DTab);

            // multi scan 3D tab
            FXMLLoader multiScan3DTabLoader = new FXMLLoader();
            MainWindowAlgorithmTabController mainWindowMultiScan3DTabController = new MainWindowMultiScan3DTabController(this);
            multiScan3DTabLoader.setController(mainWindowMultiScan3DTabController);
            multiScan3DTabLoader.setLocation(MainApp.class.getResource("/fxml/mainWindowTabs/MainWindowMultiScan3DTab.fxml"));
            Tab multiScan3DTab = new Tab("Multi scan 3D");
            mainWindowMultiScan3DTabController.setTabPointer(multiScan3DTab);
            try {
                multiScan3DTab.setContent(multiScan3DTabLoader.load());
            } catch (IOException e) {
                e.printStackTrace();
            }
            ((TabPane) mainWindowContent.lookup("#controlTabPane")).getTabs().add(multiScan3DTab);
        }
    }

    /**
     * Dialog for selecting COM port of Device.
     * Show all available COM ports including virtual.
     * @return selected COM port name.
     * port name contain:
     *  - name of COM port if port selected
     *  - "None" if offline mode
     *  - "Emulator" if will be used device emulator
     *  - null if user close the dialog or click exit button
     */
    private String devicePortSelectDialog() {
        FXMLLoader loader = new FXMLLoader();
        PortSelectDialogController portSelectDialogController = new PortSelectDialogController();
        loader.setController(portSelectDialogController);
        loader.setLocation(MainApp.class.getResource("/fxml/StartDialog.fxml"));

        AnchorPane dialogAnchorPane;
        try {
             dialogAnchorPane = loader.load();
        } catch (IOException e) {
            Dialogs.errorDialog("Fatal error", "Can not load port choose dialog", e.getMessage());
            return null;
        }

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Setting device port");
        dialogStage.getIcons().add(new Image("/images/ChoosePort.png"));
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        dialogStage.setResizable(false);
        dialogStage.centerOnScreen();
        Scene scene = new Scene(dialogAnchorPane);
        dialogStage.setScene(scene);

        portSelectDialogController.setStage(dialogStage);
        dialogStage.showAndWait(); // Show the dialog and wait until user closes it

        return portSelectDialogController.getPort();
    }

    /**
     * Creates list of virtual objects for device emulator
     * @return list of virtual objects for simulating
     */
    private List<VirtualObject> getSimulatedObjects() {
        List<VirtualObject> simulatedObjects = new ArrayList<>();

        simulatedObjects.add(new VirtualObject(1.2, -15, -10, 0.7));
        simulatedObjects.add(new VirtualObject(2.3, 0, 8, 0.9));
        simulatedObjects.add(new VirtualObject(3.6, 20, -10, 0.4));
        simulatedObjects.add(new VirtualObject(6.4, 35, 20, 0.3));
        simulatedObjects.add(new VirtualObject(14.2, -50, 35, 0.15));

        return simulatedObjects;
    }

}
