package com.locatordevice.comio.interfaces;

import com.locatordevice.comio.DeviceAnswerData;

import com.locatordevice.comio.enums.DeviceSoundType;
import com.locatordevice.comio.enums.TransferType;

import com.locatordevice.comio.exceptions.DeviceIOException;

/**
 * Describes basic functionality for driver of Locator Device
 * @version 1.0
 * @author Morhun N.G.
 */
public interface LocatorDeviceDriver {

    DeviceAnswerData scanSpace(final TransferType transferType, final int periods) throws DeviceIOException;
    void setAzimuth(final int newAzimuth) throws DeviceIOException;
    void ledOn() throws DeviceIOException;
    void ledOff() throws DeviceIOException;
    void setLed(final boolean ledState) throws DeviceIOException;
    void makeSound(final DeviceSoundType soundType) throws DeviceIOException;
    void reset() throws DeviceIOException;

    boolean getLedState();
    int getCurrentAzimuth();

    int getTimeout();
    void setTimeout(int newTimeout);

    void resourcesFree() throws DeviceIOException;

}
