package com.locatordevice.comio;

import com.locatordevice.comio.enums.DeviceAnswerType;
import com.locatordevice.comio.enums.DeviceErrorType;
import com.locatordevice.comio.enums.DeviceSoundType;
import com.locatordevice.comio.enums.TransferType;
import com.locatordevice.comio.exceptions.DeviceIOException;
import com.locatordevice.comio.interfaces.LocatorDeviceDriver;

import jssc.SerialPortList;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

import java.io.UnsupportedEncodingException;

/**
 * Driver for work with Locator Device
 * @version 1.0
 * @author Morhun N.G.
 */
public class Communicator implements LocatorDeviceDriver {

	/** COM port handle */
	protected SerialPort serialPort;	
	/** timeout for reading of COM port in ms */
	protected int timeout;

	/** current azimuth in steps */
	protected int azimuth; // 127 ~ 0 degree, one step: 1.5 degree
	/** current LED state: on/off */
	protected boolean led;
	
	/** number of periods of radiation of last scan */
	protected int scanPeriods;
	/** azimuth of last scan */
	protected int scanAzimuth;
	/** data part for transfer of last scan */
	protected TransferType scanTransferPart;
	
	/**
	 * Constructor.
	 * Setting COM port for work with locator device.
	 * @param port name of COM port
	 * @throws DeviceIOException
	 */
	public Communicator(final String port) throws DeviceIOException  {
		this.serialPort = new SerialPort(port);
		try {
			this.serialPort.openPort();
		} catch (SerialPortException e) {
			throw new DeviceIOException("Cannot open COM port.\n" + e.getMessage());
		}
		try {
			this.serialPort.setParams(SerialPort.BAUDRATE_115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		} catch (SerialPortException e) {	
			throw new DeviceIOException("Cannot configure COM port.\n"  + e.getMessage());
		}
       
		this.timeout = 3000; // ms
        
        this.azimuth = LocatorConstants.ZERO_AZIMUTH;
        this.led = false;
	}
	
	/**
	 * Destructor. Free COM port.
	 * @throws DeviceIOException 
	 */
	public void resourcesFree() throws DeviceIOException {
		try {
			this.serialPort.closePort();
		} catch (SerialPortException e) {
			throw new DeviceIOException("Cannot close COM port.\n" + e.getMessage());
		}
	}
	
	public int getTimeout() {
		return this.timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	
	/**
	 * Prepare command for scan space for locator device.
	 * Packing parameters into one byte.
	 * @param transferType part of data which will send to computer
	 * @param periods number of radiation periods
	 * @return result of scan
	 * @throws DeviceIOException
	 * @throws IllegalArgumentException
	 */
	public DeviceAnswerData scanSpace(final TransferType transferType, final int periods)
			throws DeviceIOException {
		/*
		 * Third byte of scan command has following structure:
		 *   b7 b6 b5 b4 b3 b2 b1 b0
		 * where:
		 *   b7-b6 is transfer type:
		 *     10 - all, 01 - half, 00 - quarter.
		 *   b5-b0 is periods of radiation
		 */
		if (periods < LocatorConstants.MINIMUM_PERIODS_OF_RADIATION
				|| periods > LocatorConstants.MAXIMUM_PERIODS_OF_RADIATION) {
			throw new IllegalArgumentException("Wrong number of radiation periods");
		}
		
		final byte transferTypeByte;
		switch (transferType) {
		case ALL:
			transferTypeByte = (byte) 128;	// 10000000
			break;
		case HALF:
			transferTypeByte = 64; 			// 01000000
			break;
		case QUARTER:
			transferTypeByte = 0; 			// 00000000
			break;
		default:
			throw new IllegalArgumentException("Wrong transfer type parameter");
		}
		
		final byte periodsByte = (byte) (63 & periods); // 00111111 & periods
		
		this.scanAzimuth = this.azimuth;
		this.scanPeriods = periods;
		this.scanTransferPart = transferType;
		
		byte[] command = new byte[3];
		command[0] = '*';
		command[1] = 'P';
		command[2] = (byte) (transferTypeByte | periodsByte);
		
		return this.sendCommandAndReceiveAnswer(command);
	}
	
	/**
	 * Setting device's antenna azimuth
	 * @param azimuth new azimuth
	 * @throws DeviceIOException
	 * @throws IllegalArgumentException
	 */
	public void setAzimuth(final int azimuth) throws DeviceIOException {
		if (azimuth < LocatorConstants.MINIMAL_AZIMUTH || azimuth > LocatorConstants.MAXIMAL_AZIMUTH) {
			throw new IllegalArgumentException("Wrong azimuth parameter");
		}
		
		byte[] command = new byte[3];
		command[0] = '*';
		command[1] = 'A';
		command[2] = (byte) azimuth;
		
		this.sendCommandAndReceiveAnswer(command);

		this.azimuth = azimuth;
	}
	
	public int getCurrentAzimuth() {
		return this.azimuth;
	}

	/**
	 * Turn on LED on locator's antenna
	 * @throws DeviceIOException
	 */
	public void ledOn() throws DeviceIOException {
		byte[] command = new byte[3];
		command[0] = '*';
		command[1] = 'L';
		command[2] = '1';
		
		this.sendCommandAndReceiveAnswer(command);
		this.led = true;
	}
	
	/**
	 * Turn off LED on locator's antenna
	 * @throws DeviceIOException
	 */
	public void ledOff() throws DeviceIOException {
		byte[] command = new byte[3];
		command[0] = '*';
		command[1] = 'L';
		command[2] = '0';
		
		this.sendCommandAndReceiveAnswer(command);
		this.led = false;
	}
	
	/**
	 * On/Off LED
	 * @param ledState turn on LED if true and turn off LED if false
	 * @throws DeviceIOException
	 */
	public void setLed(final boolean ledState) throws DeviceIOException {
		if (ledState) {
			this.ledOn();
		} else {
			this.ledOff();
		}
	}
	
	/**
	 * @return true if LED on, and false if LED off
	 */
	public boolean getLedState() {
		return this.led;
	}

	/**
	 * Makes sound on ultrasonic element
	 * @param soundType type of sound for play
	 * @throws DeviceIOException
	 */
	public void makeSound(final DeviceSoundType soundType) throws DeviceIOException {
		byte[] command = new byte[3];
		command[0] = '*';
		command[1] = 'S';
		command[2] = soundType.getSoundCode();

		this.sendCommandAndReceiveAnswer(command);
	}

	/**
	 * Reset state of Locator Device as it just starting
	 * @throws DeviceIOException
	 */
	public void reset() throws DeviceIOException {
		this.azimuth = LocatorConstants.ZERO_AZIMUTH;
		this.led = false;
		
		byte[] command = new byte[2];
		command[0] = '*';
		command[1] = 'R';
		
		this.sendCommandAndReceiveAnswer(command);
	}
	
	/**
	 * Send direct command/commands to locator device
	 * @param command list of commands to locator device. Maximum 32 bytes.
	 * @throws DeviceIOException
	 */
	protected void sendCommand(final String command) throws DeviceIOException {
		try {
			this.serialPort.writeBytes(command.getBytes("ASCII"));
		} catch (SerialPortException | UnsupportedEncodingException e) {
			throw new DeviceIOException(e.getMessage());
		}
	}
	
	/**
	 * Send direct command to locator device
	 * @param command one command to locator device
	 * @throws DeviceIOException
	 */
	protected void sendCommand(final byte[] command) throws DeviceIOException {
		try {
			this.serialPort.writeBytes(command);
		} catch (SerialPortException e) {
			throw new DeviceIOException(e.getMessage());
		}
	}
	
	/**
	 * Receive answer from Locator Device 
	 * @return object that content all data of device answer
	 * @throws DeviceIOException
	 */
	private DeviceAnswerData receiveAnswer() throws DeviceIOException {
		//this.serialPort.purgePort(PURGE_RXCLEAR | PURGE_TXCLEAR);

		if (this.readByteFromSerialPort() != '*') {
			throw new DeviceIOException("Unrecognized device answer");
		}
		DeviceAnswerData devAns = new DeviceAnswerData();
		
		switch (this.readByteFromSerialPort()) {
		case '!':
			devAns.setAnswerType(DeviceAnswerType.SUCCESS);
			devAns.setErrorType(DeviceErrorType.SUCCESS);
			devAns.setData(null,null);
			break;
		case '?':
			devAns.setAnswerType(DeviceAnswerType.ERROR);			
			switch (this.readByteFromSerialPort()) {
			case 'o':
				devAns.setErrorType(DeviceErrorType.OVERFLOW);
				break;
			case 'c':
				devAns.setErrorType(DeviceErrorType.INVALID_COMMAND);
				break;
			case 'p':
				devAns.setErrorType(DeviceErrorType.WRONG_PARAMETER);
				break;
			case 't':
				devAns.setErrorType(DeviceErrorType.TIMEOUT);
				break;
			default:
				throw new DeviceIOException("Unrecognized device answer");
			}
			devAns.setData(null,null);
			break;
		case '=':
			devAns.setAnswerType(DeviceAnswerType.DATA);
			devAns.setAzimuth(this.scanAzimuth);
			devAns.setPeriods(this.scanPeriods);	

			/*
			devAns.setData(this.serialPort.readBytes(
				this.scanTransferPart.getRawDataLength(), this.timeout),
				this.scanTransferPart);
			*/
			// In jSSC library not exist function for increase receive buffer.
			// So, when data size is 4 Kb or 8 Kb receive buffer will be overflow.
			// For fix it, data reading from port when receiving.

			int bytesForReading = this.scanTransferPart.getRawDataLength();
			byte[] receivedData = new byte[bytesForReading];
			for (int i = 0; i < bytesForReading; i++) {
				receivedData[i] = this.readByteFromSerialPort();
				//receivedData[i] = (byte) this.serialPort.readIntArray(1)[0];
			}
			devAns.setData(receivedData, this.scanTransferPart);
			
			if (this.readByteFromSerialPort() != '!') {
				throw new DeviceIOException("Unrecognized device answer");
			}
			break;
		default:
			throw new DeviceIOException("Unrecognized device answer");
		}
		
		return devAns;
	}

	/**
	 * Reading one byte from COM port or COM port buffer
	 * @return byte read from COM port
	 * @throws DeviceIOException
	 */
	private byte readByteFromSerialPort() throws DeviceIOException {
		try {
			return this.serialPort.readBytes(1, this.timeout)[0];
		} catch (SerialPortException | SerialPortTimeoutException e) {
			throw new DeviceIOException(e.getMessage());
		}
	}

	/**
	 * Send command to locator device and receive answer
	 * @param command one command for locator device
	 * @return scanned data
	 * @throws DeviceIOException
	 */
	private DeviceAnswerData sendCommandAndReceiveAnswer(final byte[] command)
			throws DeviceIOException {

		this.sendCommand(command);
		return this.receiveAnswer();
	}
	
	/**
	 * @return array of all COM ports names including virtual.
	 * Note that you must have permissions for this
	 * otherwise this method returns null.
	 */
	public static String[] getCOMPorts() {
		return SerialPortList.getPortNames();
	}
	
	/**
	 * Print list of all COM ports in system including virtual.
	 * Note that you must have permissions for it.
	 */
	public static void printCOMPorts() {
		String[] portNames = SerialPortList.getPortNames();
		for (String portName : portNames) {
			System.out.println(portName);
		}
	}
	
}
