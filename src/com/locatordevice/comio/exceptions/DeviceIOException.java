package com.locatordevice.comio.exceptions;

/**
 * Describes exceptions which arise when working with Locator Device  
 * @version 1.0
 * @author Murhun N.G.
 */
public class DeviceIOException extends Exception {
    public DeviceIOException() { super(); }
    public DeviceIOException(String msg) {super(msg);}
}
