package com.locatordevice.comio.exceptions;

/**
 * Describes exceptions which is caused when file of scan has wrong format
 * @version 1.0
 * @author Murhun N.G.
 */
public class ScanFileFormatException extends Exception {
    public ScanFileFormatException() { super(); }
    public ScanFileFormatException(String msg) { super(msg); }
}
