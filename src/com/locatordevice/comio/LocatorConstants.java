package com.locatordevice.comio;

/**
 * Contains main constants which is used in locator and algorithms of location
 */
public abstract class LocatorConstants {

    public static final int ZERO_AZIMUTH = 127;
    public static final int MINIMAL_AZIMUTH = 47;
    public static final int MAXIMAL_AZIMUTH = 207;
    public static final int MINIMUM_PERIODS_OF_RADIATION = 0;
    public static final int MAXIMUM_PERIODS_OF_RADIATION = 63;
    public static final int MAXIMUM_DISTANCE_OF_SCAN = 16; // m

    public static final int NUMBER_OF_RECEIVERS = 4;
    public static final int SAMPLES_PER_PERIOD = 5;
    public static final double AZIMUTH_STEP = 1.5;
    public static final int HORIZONTAL_DISTANCE_BETWEEN_RECEIVERS = 80; // mm
    public static final int VERTICAL_DISTANCE_BETWEEN_RECEIVERS = 40; // mm

    // TODO normalize to CI units
    public static final double SPEED_OF_SOUND = 340; // m/s
    public static final double FREQUENCY_OF_RADIATION = 32.53; // kHz
    public static final double TRANSMIT_PERIOD = 30.74085; // mks
    public static final double SAMPLE_PERIOD = TRANSMIT_PERIOD / SAMPLES_PER_PERIOD; // 6.14817 mks
    public static final double PAUSE_TIME = 645.6; // mks
    public static final double PERIODS_OF_PAUSE_TIME = 21; // PAUSE_TIME / TRANSMIT_PERIOD
    public static final double LENGTH_OF_WAVE = SPEED_OF_SOUND / (FREQUENCY_OF_RADIATION * 1e+3); // 0.01045189 m
    public static final double PAUSE_DISTANCE = LENGTH_OF_WAVE * PERIODS_OF_PAUSE_TIME / 2; // 0.11 m

}
