package com.locatordevice.comio;

import java.io.IOException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;

import com.locatordevice.comio.enums.DeviceAnswerType;
import com.locatordevice.comio.enums.DeviceErrorType;
import com.locatordevice.comio.enums.TransferType;
import com.locatordevice.comio.exceptions.ScanFileFormatException;
import com.locatordevice.utils.DateUtil;
import com.locatordevice.utils.FilesUtil;

/**
 * Describe locator's answer.
 * Objects can be created from device driver only
 * and setters must called from driver only too.
 * or reading from disk.
 * From other packages read only.
 * @version 1.0
 * @author Morhun N.G.
 */
public class DeviceAnswerData {

	/** type of answer from device */
    protected DeviceAnswerType ansType;
	/** an error witch occurred when scanning */
	protected DeviceErrorType errType;
	/** data obtained from scan */
	protected byte[] data;
	/** part of data witch was transfer */
	protected TransferType trnType;
	/** azimuth of scan */
	protected int azimuth;
	/** scan periods of radiation */
	protected int periods;
	/** date and time of scan */
	protected long scanTime;

    private static final String PATH_SEPARATOR = System.getProperty("file.separator");
	private static final String PATH_TO_SCANS = "." + PATH_SEPARATOR + "scans" + PATH_SEPARATOR;

	private static final int DATA_FILE_HEADER_SIZE = 16;

	/**
	 * Creating scan object from file
	 * Name of file must have following format: YYYY.MM.DD_HH-MM-SS-MSS.txt
	 * @param canonicalFileName path to file and its name
	 * @throws IOException
	 * @throws ScanFileFormatException
	 */
	public DeviceAnswerData(String canonicalFileName)
			throws IOException, ScanFileFormatException {
		this.loadFromFile(canonicalFileName);
	}

	/**
	 * Data and properties recording upon receipt.
	 * Calling from driver only!
	 */
	protected DeviceAnswerData() {
		scanTime = System.currentTimeMillis();
		data = null;
	}

	/**
	 * Constructor for create simulated device answer.
	 * Tt is used from device emulator.
	 * @param azimuth azimuth of scan
	 * @param periods number of periods of radiation of scan
	 */
	public DeviceAnswerData(int azimuth, int periods) {
		this();
		this.azimuth = azimuth;
		this.periods = periods;
	}

	/**
	 * Sets unpacked data of scan.
	 * Tt is used from device emulator.
	 * @param data obtained data, one byte - one sample
	 * @param transferType type of data transfer
	 */
	public void setUnpackedData(final byte[] data, final TransferType transferType) {
		if (data == null || transferType == null) {
			return;
		}

		setAnswerType(DeviceAnswerType.DATA);
		setErrorType(DeviceErrorType.SUCCESS);
		setDataTransferPart(transferType);

		this.data = data;
	}

	 /**
	 * Read scanned data from file. Filename: YYYY.MM.DD_HH-MM-SS-MSS.txt
	 * @param canonicalFileName path to file and its name
	 * @throws IOException
	 */
	private void loadFromFile(final String canonicalFileName)
			throws ScanFileFormatException, IOException {
		/*
		 * Incoming file format:
		 * First 16 bytes are header, which consist from four parts:
		 * First part: 'A','=',<azimuth in steps(1.5 degree)>,';'
 		 * Second part: 'P','=',<number of periods>,';'
		 * Third part: 'T','=',<transfer type:4 - all, 2 - half, 1 - quarter >, ';'
		 * Fourth part: 0000 - reserved
		 * Rest of file is scanned data: high1, low1, high2, low2, ...
		 * Data byte structure: < 0 0 0 0 LDn RDn LUp RUp >
		 * Data length depends on <transfer type>: 2*8192, 2*4096, 2*2048 (2*2048*<transfer type>)
		 */

		this.ansType = DeviceAnswerType.DATA;
		this.errType = DeviceErrorType.SUCCESS;

		final File file;
        file = new File(canonicalFileName); // absolute path
        this.scanTime = DateUtil.parseScanDate(canonicalFileName.substring(
				canonicalFileName.lastIndexOf(PATH_SEPARATOR) + 1, canonicalFileName.length() - 4));
		
		try(FileInputStream fileInputStream = new FileInputStream(file)) {
			int dataSize = fileInputStream.available() - DATA_FILE_HEADER_SIZE;
			if ( ! (dataSize == 2*(8*1024) || dataSize == 2*(4*1024) || dataSize == 2*(2*1024))) {
				throw new ScanFileFormatException("Wrong file size");
			}
 			
			byte[] header = new byte[DATA_FILE_HEADER_SIZE];
			for (int i = 0; i < DATA_FILE_HEADER_SIZE; i++) {
				header[i] = (byte) fileInputStream.read();
			}
			byte fileHeaderMask[] = {'A','=',0,';',
									 'P','=',0,';',
									 'T','=',0,';',
										0,0,0,0};
			this.azimuth = header[2];
			this.periods = header[6];
			this.trnType = TransferType.getTransferTypeByDistance(header[10]);
			
			header[2] = 0;
			header[6] = 0;
			header[10] = 0;
			for (int i = 0; i < DATA_FILE_HEADER_SIZE; i++) {
				if (header[i] != fileHeaderMask[i]) {
					throw new ScanFileFormatException("Wrong file header");
				}
			}

			this.data = new byte[dataSize];
			for (int i = 0; i < dataSize; i++) {
				this.data[i] = (byte) fileInputStream.read();
			}
		}
	}

	/**
	 * Save scanned data into file with name: YYYY.MM.DD_HH-MM-SS-MSS.txt
	 * @throws IOException
	 */
	public void saveToFile() throws IOException {
		/*
		 * File format:
		 * First 16 bytes are header, which consist from four part:
		 * First part: 'A','=',<azimuth in steps(1.5 degree)>,';'
		 * Second part: 'P','=',<number of periods>,';'
		 * Third part: 'T','=',<transfer type: 4 - all, 2 - half, 1 - quarter>,';'
		 * Fourth part: 0000 - reserved
		 * Rest of file is scanned data: high1, low1, high2, low2, ...
		 * Data byte structure: < 0 0 0 0 LDn RDn LUp RUp >
		 * Data length depends on <transfer type>: 2*8192, 2*4096, 2*2048 (2*2048*<transfer type>)
		 */

		FilesUtil.verifyAndCreateDirectory(PATH_TO_SCANS);
		final String filename = PATH_TO_SCANS + DateUtil.formatScanDate(this.scanTime) + ".txt";
		
		final FileOutputStream fileOutputStream = new FileOutputStream(new File(filename));

	    byte[] headerRecord = new byte[4];
	    headerRecord[0] = 'A';
	    headerRecord[1] = '=';
	    headerRecord[2] = (byte) this.azimuth;
	    headerRecord[3] = ';';
	    fileOutputStream.write(headerRecord);
	    
	    headerRecord[0] = 'P';
	    headerRecord[1] = '=';
	    headerRecord[2] = (byte) this.periods;
	    headerRecord[3] = ';';
	    fileOutputStream.write(headerRecord);
	    
	    headerRecord[0] = 'T';
	    headerRecord[1] = '=';
		headerRecord[2] = (byte) this.trnType.getScanFactor();
	    headerRecord[3] = ';';
	    fileOutputStream.write(headerRecord);
	    
	    headerRecord[0] = 0;
	    headerRecord[1] = 0;
	    headerRecord[2] = 0;
	    headerRecord[3] = 0;
	    fileOutputStream.write(headerRecord);
	    
	    fileOutputStream.write(this.data);
	    
	    fileOutputStream.close();
	}
	
	public long getScanTime() {
		return this.scanTime;
	}
	
	public DeviceAnswerType getAnswerType() {
		return this.ansType;
	}
	
	public DeviceErrorType getErrorType() {
		return this.errType;
	}
	
	public byte[] getReceivedRawData() {
		return this.data;
	}
	
	public TransferType getDataTransferPart() {
		return this.trnType;
	}
	
	public int getAzimuth() {
		return this.azimuth;
	}
	
	public int getPeriods() {
		return this.periods;
	}
	
	protected void setAnswerType(DeviceAnswerType deviceAnswerType) {
		this.ansType = deviceAnswerType;
	}
	
	protected void setErrorType(DeviceErrorType det) {
		this.errType = det;
	}
	
	/**
	 * Unpacking compressed raw data: one selection = one byte  
	 * @param rawData raw data from device
	 * @param transferType type of data transfer
	 */
	protected void setData(final byte[] rawData, final TransferType transferType) {
		// if device's answer isn't data transfer then rawData and transferType will be null
		if (rawData == null || transferType == null) {
			return;
		}

		this.trnType = transferType;
		int rawDataLength = transferType.getRawDataLength();

		this.data = new byte[2*rawDataLength];
		for (int i = 0; i < rawDataLength; i++) {
			byte packedByte = rawData[i];
			this.data[2*i] = (byte) (packedByte >>> 4);
			this.data[2*i+1] = (byte) (packedByte & 0x0F);
		}
	}

	protected void setDataTransferPart(TransferType transferType) {
		this.trnType = transferType;
	}
	
	protected void setAzimuth(int newAzimuth) {
		this.azimuth = newAzimuth;
	}
	
	protected void setPeriods(int newPeriods) {
		this.periods = newPeriods;
	}

}
