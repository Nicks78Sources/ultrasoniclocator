package com.locatordevice.comio.enums;

public enum DeviceErrorType {
	SUCCESS,
	OVERFLOW,
	INVALID_COMMAND,
    WRONG_PARAMETER,
	TIMEOUT
}
