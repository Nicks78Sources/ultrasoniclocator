package com.locatordevice.comio.enums;

public enum TransferType {
	QUARTER(4),
	HALF(8),
	ALL(16);

    /** scan distance in meters */
    private int distance;

    TransferType(int distance) {
        this.distance = distance;
    }

    /**
     * @return distance of scan in meters
     */
    public int getScanDistance() {
        return distance;
    }

    /**
     * @return factor of scanned distance
     */
    public int getScanFactor() {
        return distance / 4;
    }

    /**
     * @return length of raw scanned data in bytes
     */
    public int getRawDataLength() {
        return distance * 512;
    }

    /**
     * @return length of channel in bytes
     */
    public int getChannelLength() {
        return distance * 1024;
    }

    /**
     * Converting user defined distance to transfer type of locator device.
     * @param distance maximal requiring distance
     * @return type of data transfer for specified distance
     *  or null when distance is invalid.
     */
    public static TransferType getTransferTypeByDistance(final int distance) {
        if (distance > 0) {
            if (distance <= 4) {
                return QUARTER;
            } else if (distance <= 8) {
                return  HALF;
            } else if (distance <= 16) {
                return ALL;
            }
        }

        return null;
    }

}
