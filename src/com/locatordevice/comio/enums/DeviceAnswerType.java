package com.locatordevice.comio.enums;

public enum DeviceAnswerType {
	SUCCESS,
	ERROR,
	DATA
}
