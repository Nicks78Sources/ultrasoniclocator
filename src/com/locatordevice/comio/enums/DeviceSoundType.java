package com.locatordevice.comio.enums;

public enum DeviceSoundType {
	SILENT(48),
	F_4800HZ(49),
	F_6400HZ(50),
	SHOOT(51);

	private byte soundCode;

	DeviceSoundType(int soundCode) {
		this.soundCode = (byte) soundCode;
	}

	public byte getSoundCode() {
		return soundCode;
	}
}
