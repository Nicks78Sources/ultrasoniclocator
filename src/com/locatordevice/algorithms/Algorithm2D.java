package com.locatordevice.algorithms;

import com.locatordevice.MainApp;
import com.locatordevice.beans.DetectedObject2D;
import com.locatordevice.beans.ScanData;
import com.locatordevice.comio.enums.TransferType;
import com.locatordevice.comio.exceptions.DeviceIOException;
import com.locatordevice.comio.interfaces.LocatorDeviceDriver;
import com.locatordevice.utils.MathUtil;
import com.locatordevice.view.Drawer2D;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.locatordevice.comio.LocatorConstants.*;

/**
 * Abstract class with shared methods of 2D algorithms
 * @version 1.0
 * @author Morhun N.G.
 */
public abstract class Algorithm2D {

    public static final int MINIMUM_VIEWING_ANGLE_DEGREES = 0;
    public static final int MAXIMUM_VIEWING_ANGLE_DEGREES = 180;

    /** radar visualization constants */
    protected static final int RADAR_HORIZONTAL_RANGE = 600;
    protected static final int RADAR_VERTICAL_RANGE = 300;
    protected static final int RADAR_INDENT = 50;
    protected static final int RADAR_X_ORIGIN = 350; // (0,0) <=> (350,350)
    protected static final int RADAR_Y_ORIGIN = 350;
    protected static final int RADAR_DISTANCE_LABELS_Y_POSITION = 370;
    protected static final int SYMBOL_CORRECTION_SHIFT = 5;
    protected static final int MOUSE_COORDINATES_X = 20;
    protected static final int MOUSE_COORDINATES_Y = 20;

    protected static final Color UPPER_CHANNELS_OBJECT_COLOR = Color.RED;
    protected static final Color LOWER_CHANNELS_OBJECT_COLOR = Color.GREEN;
    /** size of detected object on radar */
    protected static final int OBJECT_SIZE = 17;
    /** rules for sorting detected objects */
    protected static final Comparator<DetectedObject2D> detectedObjectsComparator
            = (o1, o2) -> (int) Math.signum(o2.getDistance() - o1.getDistance()); // from far to near

    /** pointer to radar drawer */
    protected Drawer2D drawer2D;
    /** pointer to device driver */
    protected LocatorDeviceDriver locatorDeviceDriver;
    /** filters for obtained data */
    protected Filter filter;
    /** last required maximal distance */
    protected int maxDistance;
    /** current azimuth of radar's antenna */
    protected double currentAzimuthDegree;
    /**
     * Half of maximum angle scanning (degrees).
     * Ranges from 0+ to 90 degrees.
     * Zero is current azimuth of antenna.
     */
    protected double viewingAngleHalf;
    /**
     * Maximum number of samples for {@code viewingAngleHalf}.
     * If shift between signals in two channels is less then nMax
     * than we consider that it is same object.
     * It used for calculating azimuth of object in space.
     */
    protected int nMax;


    public Algorithm2D(MainApp mainAppPointer) {
        this.locatorDeviceDriver = mainAppPointer.getLocatorDeviceDriver();
        this.drawer2D = mainAppPointer.getDrawer2D();
        this.filter = new Filter();
        setupFilter();

        this.currentAzimuthDegree = (locatorDeviceDriver.getCurrentAzimuth() - ZERO_AZIMUTH) * AZIMUTH_STEP;
        this.viewingAngleHalf = 25; // degrees
    }

    /**
     * Sets the angle within which space will be analyzed in one scan
     * Zero angle is current azimuth of antenna
     * @param viewingAngle viewing angle, degrees
     */
    public void setViewingAngle(double viewingAngle) {
        if (viewingAngle >= MINIMUM_VIEWING_ANGLE_DEGREES && viewingAngle <= MAXIMUM_VIEWING_ANGLE_DEGREES) {
            this.viewingAngleHalf = viewingAngle / 2;
            calculateNMax();
        }
    }

    /**
     * Calculates maximum number of samples corresponding to viewing angle field,
     * which considered as one signal with shift in channel
     */
    protected void calculateNMax() {
        this.nMax = (int) Math.floor(HORIZONTAL_DISTANCE_BETWEEN_RECEIVERS * 1e-3 / (LENGTH_OF_WAVE / SAMPLES_PER_PERIOD)
                * Math.sin(MathUtil.degToRad(viewingAngleHalf)));
    }

    /**
     * Detects objects using data from two horizontal receivers
     * @param leftChannel data from left receiver
     * @param rightChannel data from right receiver
     * @param color color of objects
     * @param periods periods of radiation of scan
     * @return list of detected objects
     */
    protected List<DetectedObject2D> processChannels(byte[] leftChannel, byte[] rightChannel, Color color, int periods) {
        List<DetectedObject2D> foundObjects = new ArrayList<>();
        int nLeft, nRight;

        int i = 0;
        while (i < leftChannel.length) {
            // find five consecutive zeros in both channels
            int zerosCounter = 0;
            while (i < leftChannel.length && zerosCounter < SAMPLES_PER_PERIOD) {
                if (leftChannel[i] == 0 && rightChannel[i] == 0) {
                    zerosCounter++;
                } else {
                    zerosCounter = 0;
                }
                i++;
            }
            // find first one in any channel
            while (i < leftChannel.length && leftChannel[i] == 0 && rightChannel[i] == 0) {
                i++;
            }
            if (i < leftChannel.length) {
                nLeft = nRight = -1;
                if (leftChannel[i] == 1) { // in the left channel signal appears first
                    nLeft = i;
                    // find corresponding signal in the right channel
                    for (int j = i; j < leftChannel.length && (j - i) <= nMax; j++) {
                        if (rightChannel[j] == 1) {
                            nRight = j; // corresponding signal found
                            break;
                        }
                    }
                    if (nRight > 0) {
                        foundObjects.add(detectObject(nLeft, nRight, color, periods));
                        i = nRight + 1;
                    } else {
                        i = nLeft + 1; // corresponding signal was not found
                    }
                } else {  // in the right channel signal appears first
                    nRight = i;
                    // find corresponding signal in the left channel
                    for (int j = i; j < leftChannel.length && (j - i) <= nMax; j++) {
                        if (leftChannel[j] == 1) {
                            nLeft = j; // corresponding signal found
                            break;
                        }
                    }
                    if (nLeft > 0) {
                        foundObjects.add(detectObject(nLeft, nRight, color, periods));
                        i = nLeft + 1;
                    } else {
                        i = nRight + 1; // corresponding signal was not found
                    }
                }
            }
        } // try to find next object

        return foundObjects;
    }

    /**
     * Calculates distance and horizontal angle of object
     * @param nLeft serial number of sample of begin of object's echo signal in left channel
     * @param nRight serial number of sample of begin of object's echo signal in right channel
     * @param color color of object
     * @param periods periods of radiation of scan
     * @return detected object
     */
    protected DetectedObject2D detectObject(int nLeft, int nRight, Color color, int periods) {
        double distance = 0.5 * (
                (periods + PERIODS_OF_PAUSE_TIME) * SAMPLES_PER_PERIOD
                        + (nLeft + nRight) / 2) * LENGTH_OF_WAVE / SAMPLES_PER_PERIOD;
        double horizontalAngle = MathUtil.radToDeg(Math.asin(
                ((nLeft - nRight) * LENGTH_OF_WAVE / SAMPLES_PER_PERIOD) / (HORIZONTAL_DISTANCE_BETWEEN_RECEIVERS * 1e-3)));
        horizontalAngle += currentAzimuthDegree; // take into account current azimuth of the antenna

        return new DetectedObject2D(distance, horizontalAngle, color);
    }

    /**
     * Averaging data obtained from scan
     * Objects without couple is not added to returning list
     * Objects must be only of two colors
     * @param foundObjects list of detected objects
     * @param color color of averaged objects
     * @return list of averaging objects
     */
    protected List<DetectedObject2D> averagingObjectsData(List<DetectedObject2D> foundObjects, Color color) {
        /* allowable variation in range between measurements coordinate of object, meters */
        final double allowableVariationInRange = 10 * LENGTH_OF_WAVE;
        /* allowed variation in angles between measurements coordinate of object, degrees */
        final double allowedVariationInAngle = 16 * AZIMUTH_STEP;

        DetectedObject2D object1, object2;
        List<DetectedObject2D> averagedObjectsList = new ArrayList<>();
        for (int i = 0; i < foundObjects.size() - 1; i++) {
            object1 = foundObjects.get(i);
            object2 = foundObjects.get(i + 1);
            if (object1.getColor() != object2.getColor()) {
                if ((Math.abs(object1.getDistance() - object2.getDistance()) < allowableVariationInRange)
                        && Math.abs(object1.getAzimuth() - object2.getAzimuth()) < allowedVariationInAngle) {
                    averagedObjectsList.add(new DetectedObject2D(
                            (object1.getDistance() + object2.getDistance()) / 2,
                            (object1.getAzimuth() + object2.getAzimuth()) / 2,
                            color
                    ));
                }
            }
        }
        return averagedObjectsList;
    }

    /**
     * Finds all objects in horizontal plain
     * Used data from the lower and upper receiver pairs
     * Detected objects from upper pair of receivers has {@code UPPER_CHANNELS_OBJECT_COLOR} color
     * Detected objects from lower pair of receivers has {@code LOWER_CHANNELS_OBJECT_COLOR} color
     * @param scanData data of scan
     * @return detected objects
     */
    protected List<DetectedObject2D> detectAllObjects(ScanData scanData) {
        List<DetectedObject2D> foundObjects = new ArrayList<>();

        foundObjects.addAll(processChannels(scanData.getLDn(), scanData.getRDn(),
                LOWER_CHANNELS_OBJECT_COLOR, scanData.getNumberOfPeriodsOfRadiation()));
        foundObjects.addAll(processChannels(scanData.getLUp(), scanData.getRUp(),
                UPPER_CHANNELS_OBJECT_COLOR, scanData.getNumberOfPeriodsOfRadiation()));

        return foundObjects;
    }

    /**
     * Configurations of filters which used in algorithm
     */
    protected void setupFilter() {
        filter.setFpRestoreLostSignalPeriodsForRestore(2);
        filter.setFpClearDazzledZonePeriodsOfZerosToStop(1);
        filter.setFpDeleteNoiseMaximumNumberOfOnesForRemoval(3);
    }

    /**
     * Filtering data of scan from noise
     * @param dataForFiltering data for filtering
     */
    protected ScanData filtering(ScanData dataForFiltering) {
        ScanData data = ScanDataProcessor.clone(dataForFiltering);

        byte[][] channels = data.getChannels();

        for (int i = 0; i < NUMBER_OF_RECEIVERS; i++) {
            filteringChannel(channels[i]);
        }
        return data;
    }

    /**
     * Apply filters for channel
     * @param channel channel for filtering
     */
    protected void filteringChannel(byte[] channel) {
        filter.restoreLostSignal(channel);
        int i = filter.clearDazzledZone(channel);
        filter.deleteNoise(channel, i, channel.length);
    }

    /**
     * Draws grid on radar
     */
    protected void drawRadarGrid() {
        drawer2D.setColor(Drawer2D.RADAR_GRID_COLOR);
        drawRadarGridStatic();
        drawRadarGridDynamic();
    }

    /**
     * Draws part of radar's grid, which same for all distances.
     */
    protected void drawRadarGridStatic() {
        drawer2D.drawLine(RADAR_INDENT, RADAR_Y_ORIGIN, RADAR_INDENT + RADAR_HORIZONTAL_RANGE, RADAR_Y_ORIGIN);
        drawRadialLine(0);
        drawRadialLine(30);
        drawRadialLine(60);
        drawRadialLine(-30);
        drawRadialLine(-60);

        //drawer2D.drawUpperSemicircle(RADAR_X_ORIGIN, RADAR_Y_ORIGIN, RADAR_VERTICAL_RANGE);

        drawer2D.drawText("0", RADAR_X_ORIGIN - SYMBOL_CORRECTION_SHIFT, RADAR_DISTANCE_LABELS_Y_POSITION);
        drawer2D.drawText("-90°", 15, RADAR_Y_ORIGIN);
        drawer2D.drawText("-60°", 55, 200);
        drawer2D.drawText("-30°", 170, 75);
        drawer2D.drawText("0°"  , 345, 40);
        drawer2D.drawText("+30°", 505, 75);
        drawer2D.drawText("+60°", 620, 200);
        drawer2D.drawText("+90°", RADAR_INDENT + RADAR_HORIZONTAL_RANGE + SYMBOL_CORRECTION_SHIFT, RADAR_Y_ORIGIN);
    }

    /**
     * Draws dynamic part of radar's grid witch depends from {@code maxDistance}
     */
    protected void drawRadarGridDynamic() {
        float step = (float) RADAR_VERTICAL_RANGE / maxDistance;

        for (int i = 1; i <= maxDistance; i++) {
            int shift = Math.round(i * step);
            drawer2D.drawUpperSemicircle(RADAR_X_ORIGIN, RADAR_Y_ORIGIN, shift);
            String distanceLabel = Integer.toString(i);
            drawer2D.drawText(distanceLabel, RADAR_X_ORIGIN - shift - SYMBOL_CORRECTION_SHIFT, RADAR_DISTANCE_LABELS_Y_POSITION);
            drawer2D.drawText(distanceLabel, RADAR_X_ORIGIN + shift - SYMBOL_CORRECTION_SHIFT, RADAR_DISTANCE_LABELS_Y_POSITION);
        }

        if (maxDistance == 2) {
            for (int i = 1 ; i <= 3; i += 2) {
                int shift = Math.round(i * step / 2);
                drawer2D.drawUpperSemicircle(RADAR_X_ORIGIN, RADAR_Y_ORIGIN, shift);
                String distanceLabel = String.format("%.1f", (float) i / 2);
                drawer2D.drawText(distanceLabel, RADAR_X_ORIGIN - shift - 2*SYMBOL_CORRECTION_SHIFT, RADAR_DISTANCE_LABELS_Y_POSITION);
                drawer2D.drawText(distanceLabel, RADAR_X_ORIGIN + shift - 2*SYMBOL_CORRECTION_SHIFT, RADAR_DISTANCE_LABELS_Y_POSITION);
            }
        } else if (maxDistance == 1) {
            for (int i = 1 ; i <= 3; i++) {
                int shift = Math.round(i * step / 4);
                drawer2D.drawUpperSemicircle(RADAR_X_ORIGIN, RADAR_Y_ORIGIN, shift);
                String distanceLabel = String.format("%.2f", (float) i / 4);
                drawer2D.drawText(distanceLabel,RADAR_X_ORIGIN - shift - 3*SYMBOL_CORRECTION_SHIFT, RADAR_DISTANCE_LABELS_Y_POSITION);
                drawer2D.drawText(distanceLabel,RADAR_X_ORIGIN + shift - 3*SYMBOL_CORRECTION_SHIFT, RADAR_DISTANCE_LABELS_Y_POSITION);
            }
        }

    }

    /**
     * Redraw radar's data corresponding to new parameters
     * this method do not get any data from device
     * @param maxDistance maximal distance on radar
     */
    public void redrawRadar(final int maxDistance) {
        this.maxDistance = maxDistance;

        drawer2D.clearRadar();
        drawRadarGrid();
        drawCurrentDirection();

        drawRadarData();
    }

    /**
     * Draws objects on radar
     */
    protected abstract void drawRadarData();

    /**
     * Draws current azimuth(antenna's direction)
     */
    protected void drawCurrentDirection() {
        drawer2D.setColor(Drawer2D.RADAR_CURRENT_AZIMUTH_COLOR);
        drawRadialLine(currentAzimuthDegree);
    }

    /**
     * Draws objects on radar
     * @param objects2DList list of objects for visualization
     */
    protected void drawObjectsOnRadar(List<DetectedObject2D> objects2DList) {
        if (objects2DList != null) {
            for (DetectedObject2D detectedObject : objects2DList) {
                if (detectedObject.getDistance() <= maxDistance) {
                    drawObject(detectedObject);
                }
            }
        }
    }

    /**
     * Draws object on radar in specified position
     * @param distance distance to object from radar's antenna (meters)
     * @param azimuth azimuth to object from radar's antenna (degrees)
     */
    protected void drawObject(double distance, double azimuth) {
        int radius = (int) Math.round(distance * RADAR_VERTICAL_RANGE / maxDistance); // pixels
        double beta = MathUtil.degToRad(90 - Math.abs(azimuth)); // azimuth and beta are complementary angles
        int x = RADAR_X_ORIGIN + (int) (Math.round((radius * Math.cos(beta)) * Math.signum(azimuth)));
        int y = RADAR_Y_ORIGIN + (- ((int) Math.round(radius * Math.sin(beta))));

        //drawer2D.drawFilledCircle(x, y, OBJECT_SIZE);
        drawer2D.drawTransparentFilledCircle(x, y, OBJECT_SIZE, 0.5);
    }

    /**
     * Draws @param detectedObject2D on radar
     */
    protected void drawObject(DetectedObject2D detectedObject2D) {
        drawer2D.setColor(detectedObject2D.getColor());
        drawObject(detectedObject2D.getDistance(), detectedObject2D.getAzimuth());
    }

    /**
     * Draws line from radar's origin to radar's border in the direction of @param lineAzimuth
     * @param lineAzimuth azimuth of line. @param lineAzimuth changes from -90 to 90 degrees
     */
    protected void drawRadialLine(double lineAzimuth) {
        double beta = MathUtil.degToRad(90 - Math.abs(lineAzimuth)); // lineAzimuth and beta are complementary angles
        int xShiftFromOrigin = (int) (Math.round((RADAR_VERTICAL_RANGE * Math.cos(beta)) * Math.signum(lineAzimuth)));
        int yShiftFromOrigin = - ((int) Math.round(RADAR_VERTICAL_RANGE * Math.sin(beta)));

        drawer2D.drawLine(RADAR_X_ORIGIN, RADAR_Y_ORIGIN,
                RADAR_X_ORIGIN + xShiftFromOrigin, RADAR_Y_ORIGIN + yShiftFromOrigin);
    }

    /**
     * Draws coordinates under mouse cursor in polar coordinates
     */
    public void drawMouseCoordinates(int x, int y) {
        drawer2D.setColor(Drawer2D.RADAR_COORDINATES_COLOR);

        String coordinates;
        if ( (y < RADAR_INDENT || y > (RADAR_INDENT + RADAR_VERTICAL_RANGE))
                || Math.sqrt(MathUtil.sqr(x - RADAR_X_ORIGIN) + MathUtil.sqr(y - RADAR_Y_ORIGIN)) > RADAR_VERTICAL_RANGE) {
            coordinates = "- - - - -";
        } else {
            int xDist = (x - RADAR_X_ORIGIN);
            int yDist = Math.abs(y - RADAR_Y_ORIGIN);
            double distance = (double) maxDistance * Math.sqrt(MathUtil.sqr(xDist) + MathUtil.sqr(yDist)) / RADAR_VERTICAL_RANGE;
            double angle = MathUtil.radToDeg(Math.atan((double) xDist / yDist));
            coordinates = String.format("%.3f m, %.3f°", distance, angle);
        }

        eraseMouseCoordinates();
        drawer2D.drawText(coordinates, MOUSE_COORDINATES_X, MOUSE_COORDINATES_Y);
    }

    /**
     * Erase coordinates of mouse cursor on radar's canvas
     */
    public void eraseMouseCoordinates() {
        drawer2D.eraseRectangle(MOUSE_COORDINATES_X, MOUSE_COORDINATES_Y - 10, MOUSE_COORDINATES_X + 150, MOUSE_COORDINATES_Y + 10);
    }

    /**
     * Makes scan with specified parameters
     * @param requiredDistance maximum distance
     * @param periods periods of radiation
     * @return data of scan
     */
    protected ScanData scan(final int requiredDistance, final int periods) throws DeviceIOException {
        return ScanDataProcessor.getScanData(locatorDeviceDriver.scanSpace(
                TransferType.getTransferTypeByDistance(requiredDistance), periods));
    }

}
