package com.locatordevice.algorithms;

import static com.locatordevice.comio.LocatorConstants.SAMPLES_PER_PERIOD;

/**
 * This class filtering data of scans with specified parameters
 * @version 1.0
 * @author Morhun N.G.
 */
public class Filter {

    /**
     * Parameter of {@code restoreLostSignal} filter
     * Sets maximal quantity of periods which considered
     * as lost signal between two parts of this signal
     */
    private int fpRestoreLostSignal_PeriodsForRestore;

    /**
     * Parameter of {@code clearDazzledZone} filter
     * Sets number of periods of consecutive zeros in obtained signal
     * to determine end of parasitic oscillations
     */
    private int fpClearDazzledZone_PeriodsOfZerosToStop;

    /**
     * Parameter of {@code deleteNoise} filter
     * Sets maximum number of consecutive ones which considered as noise
     */
    private int fpDeleteNoise_MaximumNumberOfOnesForRemoval;

    public Filter() {
        fpRestoreLostSignal_PeriodsForRestore = 0;
        fpClearDazzledZone_PeriodsOfZerosToStop = 1;
        fpDeleteNoise_MaximumNumberOfOnesForRemoval = 2;
    }

    public int getFpRestoreLostSignalPeriodsForRestore() {
        return fpRestoreLostSignal_PeriodsForRestore;
    }

    public void setFpRestoreLostSignalPeriodsForRestore(int periodsForRestore) {
        if (periodsForRestore >= 0) {
            this.fpRestoreLostSignal_PeriodsForRestore = periodsForRestore;
        }
    }

    public int getFpClearDazzledZonePeriodsOfZerosToStop() {
        return fpClearDazzledZone_PeriodsOfZerosToStop;
    }

    public void setFpClearDazzledZonePeriodsOfZerosToStop(int periodsOfZerosToStop) {
        if (periodsOfZerosToStop > 0) {
            this.fpClearDazzledZone_PeriodsOfZerosToStop = periodsOfZerosToStop;
        }
    }

    public int getFpDeleteNoiseMaximumNumberOfOnesForRemoval() {
        return fpDeleteNoise_MaximumNumberOfOnesForRemoval;
    }

    public void setFpDeleteNoiseMaximumNumberOfOnesForRemoval(int maximumNumberOfOnesForRemoval) {
        if (maximumNumberOfOnesForRemoval > 0) {
            this.fpDeleteNoise_MaximumNumberOfOnesForRemoval = maximumNumberOfOnesForRemoval;
        }
    }

    /**
     * Restore lost part of signal
     * Consider, that if has no signal between two signals less than
     * {@code filterParameterPeriodsForRestore} that it is same signal
     * @param channel channel for processing
     */
    public void restoreLostSignal(byte[] channel) {
        final int MAXIMAL_ZEROS_FOR_RESTORE = (fpRestoreLostSignal_PeriodsForRestore + 1) * SAMPLES_PER_PERIOD - 1;

        int i = 0;
        int zerosCounter;

        // find zero after ones
        while (i < channel.length && channel[i] == 0) {
            i++;
        }
        while (i < channel.length && channel[i] == 1) {
            i++;
        }

        while (i < channel.length) {
            // counting zeros
            zerosCounter = 0;
            while (i < channel.length && channel[i] == 0) {
                zerosCounter++;
                i++;
            }
            if (i == channel.length) {
                return;
            }

            if (zerosCounter >= SAMPLES_PER_PERIOD && zerosCounter <= MAXIMAL_ZEROS_FOR_RESTORE) {
                // restoring weak signal
                int j = i - zerosCounter - 1; // last 1 before break of signal
                while (i - j - 1 > 7) {
                    j += SAMPLES_PER_PERIOD;
                    channel[j] = 1;
                }
                if (i - j - 1 >= SAMPLES_PER_PERIOD) { // 5-7 zeros to continue of signal
                    j += (i - j) / 2;
                    channel[j] = 1;
                }
            }

            // skip ones
            while (i < channel.length && channel[i] == 1) {
                i++;
            }
        }

    }

    /**
     * Removes the portion of parasitic oscillations in the receiving antennas
     * @param channel for processing
     * @return index of end of parasitic oscillations
     */
    public int clearDazzledZone(byte[] channel) {
        final int ZEROS_TO_STOP = SAMPLES_PER_PERIOD * fpClearDazzledZone_PeriodsOfZerosToStop;

        int i = 0;
        // find first signal (first 1)
        while (i < channel.length && channel[i] == 0) {
            i++;
        }
        if (i == channel.length) {
            return 0;
        }

        // remove all units until come specified periods of consecutive zeros
        int zerosCounter = 0;
        while (i < channel.length && zerosCounter < ZEROS_TO_STOP) {
            if (channel[i] == 0) {
                zerosCounter++;
            } else {
                channel[i] = 0;
                zerosCounter = 0;
            }
            i++;
        }

        return i;
    }

    /**
     * Deleting noises from channel
     * @param channel channel data
     * @param start index of start filtering
     * @param finish index of finish filtering
     */
    public void deleteNoise(byte[] channel, int start, int finish) {
        int i = start;
        int onesCounter;
        int onesStart;
        int zerosCounter;

        while (i < finish) {
            // find one
            while (i < finish && channel[i] == 0) {
                i++;
            }
            if (i == finish) {
                return;
            }

            // count ones
            onesCounter = 0;
            onesStart = i;
            while (i < finish && channel[i] == 1) {
                onesCounter++;
                i++;
            }
            if (i == finish) {
                return;
            }

            if (onesCounter <= fpDeleteNoise_MaximumNumberOfOnesForRemoval) {
                // verify whether is five zeros after ones
                if ((i + 5) < finish
                        && (channel[i + 1] == 0 && channel[i + 2] == 0
                        && channel[i + 3] == 0 && channel[i + 4] == 0)) {
                    // delete all ones between five(or more) zeros
                    while (onesCounter > 0) {
                        channel[onesStart + onesCounter - 1] = 0;
                        onesCounter--;
                    }
                    i--; // add zero for future sequence of zeros
                }
            }

            // find five consecutive zeros
            zerosCounter = 0;
            while (i < finish && zerosCounter < SAMPLES_PER_PERIOD) {
                if (channel[i] == 0) {
                    zerosCounter++;
                } else {
                    zerosCounter = 0;
                }
                i++;
            }
        } // next iteration
    }

    /**
     * Deleting noises from channel
     * @param channel data channel
     */
    public void deleteNoise(byte[] channel) {
        deleteNoise(channel, 0, channel.length);
    }


}
