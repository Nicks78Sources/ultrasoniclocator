package com.locatordevice.algorithms;

import com.locatordevice.MainApp;
import com.locatordevice.beans.ScanData;
import com.locatordevice.comio.exceptions.DeviceIOException;
import com.locatordevice.view.Drawer2D;

import java.util.ArrayList;
import java.util.Collections;

import static com.locatordevice.comio.LocatorConstants.AZIMUTH_STEP;
import static com.locatordevice.comio.LocatorConstants.ZERO_AZIMUTH;

/**
 * Algorithm of locate objects in space with one scan
 * @version 1.0
 * @author Morhun N.G.
 */
public class SingleScanAlgorithm3D extends Algorithm3D {

    /** last scan */
    protected ScanData lastScanRawData = null;
    /** filtered data of last scan */
    protected ScanData lastScanFilteredData;

    public SingleScanAlgorithm3D(final MainApp mainAppPointer) {
        super(mainAppPointer);

        this.detectedObjects = new ArrayList<>();
        viewingAngleHalf = 25; // degrees
    }

    /**
     * @return obtained data of last scan
     */
    public ScanData getLastScanRawData() {
        return lastScanRawData;
    }

    /**
     * Scanning space and drawing obtained data on radar
     * @param requiredDistance maximal distance that is required by user
     * @param periods periods of radiation
     */
    public void runAlgorithm(final int requiredDistance, final int periods) throws DeviceIOException {
        lastScanRawData = scan(requiredDistance, periods);
        lastScanFilteredData = filtering(lastScanRawData);
        currentAzimuthDegree = getCurrentAzimuthDegrees();
        detectObjects();
        redrawRadar(requiredDistance);
        updateCanvasSnapshot();
    }

    /**
     * Detects objects according to the obtained signal
     */
    protected void detectObjects() {
        detectedObjects.clear();

        detectedObjects = detectAllObjects(lastScanFilteredData);

        Collections.sort(detectedObjects, detectedObjectsComparator);
    }

    /**
     * Draws current azimuth(antenna's direction) and viewing scope on radar
     */
    protected void drawCurrentDirection() {
        drawCurrentViewingScope(currentAzimuthDegree);
        super.drawCurrentDirection();
    }

    /**
     * Draws current borders of viewing area
     * If the border is more than 90 degrees, it is taken as 90 degrees
     * @param currentAzimuth current azimuth in degrees
     */
    protected void drawCurrentViewingScope(double currentAzimuth) {
        drawer2D.setColor(Drawer2D.RADAR_VIEWING_SCOPE_BORDER_COLOR);
        int shift = (int) (viewingAngleHalf * (RADAR_HORIZONTAL_RANGE / 2) / MAXIMUM_VIEWING_HORIZONTAL_ANGLE_ON_RADAR);
        int currentAzimuthXCoordinate = (int) (RADAR_X_ORIGIN + currentAzimuth * (RADAR_HORIZONTAL_RANGE / 2) / MAXIMUM_VIEWING_HORIZONTAL_ANGLE_ON_RADAR);
        int xLeft = currentAzimuthXCoordinate - shift;
        int xRight = currentAzimuthXCoordinate + shift;

        if (xLeft < (RADAR_X_ORIGIN - RADAR_HORIZONTAL_RANGE / 2)) {
            xLeft = RADAR_LEFT_INDENT;
        }
        if (xRight > (RADAR_X_ORIGIN + RADAR_HORIZONTAL_RANGE / 2)) {
            xRight = RADAR_LEFT_INDENT + RADAR_HORIZONTAL_RANGE;
        }

        drawer2D.drawLine(xLeft, RADAR_TOP_INDENT, xLeft, RADAR_TOP_INDENT + RADAR_VERTICAL_RANGE);
        drawer2D.drawLine(xRight, RADAR_TOP_INDENT, xRight, RADAR_TOP_INDENT + RADAR_VERTICAL_RANGE);
    }

    /**
     * Sets antenna's azimuth
     * @param azimuthInDegree new azimuth in degrees
     * @throws DeviceIOException
     */
    public void setAzimuth(double azimuthInDegree) throws DeviceIOException {
        int azimuth = (int) Math.round(azimuthInDegree / AZIMUTH_STEP + ZERO_AZIMUTH);
        locatorDeviceDriver.setAzimuth(azimuth);
        currentAzimuthDegree = getCurrentAzimuthDegrees();

        detectedObjects.clear();

        redrawRadar(maxDistance);
    }

}
