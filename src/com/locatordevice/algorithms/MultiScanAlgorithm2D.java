package com.locatordevice.algorithms;

import com.locatordevice.MainApp;
import com.locatordevice.beans.DetectedObject2D;
import com.locatordevice.comio.exceptions.DeviceIOException;

import com.locatordevice.controller.tabs.MainWindowMultiScan2DTabController;
import com.locatordevice.view.Drawer2D;
import javafx.application.Platform;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.locatordevice.comio.LocatorConstants.*;

/**
 * Algorithm of locate objects in horizontal plane with multi scanning
 * @version 1.0
 * @author Morhun N.G.
 */
public class MultiScanAlgorithm2D extends Algorithm2D {

    public static final int MINIMUM_STUDIED_AREA_ANGLE = 3;
    public static final int MAXIMUM_STUDIED_AREA_ANGLE = 180;

    protected static final Color OBJECTS_COLOR = Color.LIGHTSALMON;

    /** pointer to own controller */
    protected MainWindowMultiScan2DTabController controller;
    /** list of averaged objects */
    protected List<DetectedObject2D> averagedObjects;
    /** number of periods of radiation for scans */
    protected int periodsOfRadiations;
    /** half of angle in which space is analyzed */
    protected double studiedAreaHalf;
    /** flag shows, when algorithm must be interrupted */
    protected boolean stopped;

    public MultiScanAlgorithm2D(final MainApp mainAppPointer, MainWindowMultiScan2DTabController controller) {
        super(mainAppPointer);

        this.controller = controller;
        viewingAngleHalf = 15; // degrees
        studiedAreaHalf = 35;  // degrees

        stopped = false;
    }

    public synchronized boolean isInterrupted() {
        return stopped;
    }

    public synchronized void sendInterruptSignal() {
        this.stopped = true;
    }

    public synchronized void prepareToStart() {
        this.stopped = false;
    }

    /**
     * Runs multi scan 2D algorithm
     * @param requiredDistance maximum required distance of analyzing
     * @param periods periods of radiation for each scan
     * @param studiedArea angle which will be studied
     * @param viewingAngle angle in which analyzed space per one scan
     * @throws DeviceIOException
     */
    public void runAlgorithm(final int requiredDistance, final int periods,
                             final double studiedArea, final double viewingAngle)
            throws DeviceIOException {
        if (requiredDistance > 0 && requiredDistance <= MAXIMUM_DISTANCE_OF_SCAN) {
            this.maxDistance = requiredDistance;
        } else {
            throw new IllegalArgumentException("Wrong distance");
        }
        if (periods >= MINIMUM_PERIODS_OF_RADIATION && periods <= MAXIMUM_PERIODS_OF_RADIATION) {
            this.periodsOfRadiations = periods;
        } else {
            throw new IllegalArgumentException("Wrong number of periods of radiation");
        }
        if (studiedArea >= MINIMUM_STUDIED_AREA_ANGLE && studiedArea <= MAXIMUM_STUDIED_AREA_ANGLE) {
            studiedAreaHalf = studiedArea / 2;
        } else {
            throw new IllegalArgumentException("Wrong studied area");
        }
        if (viewingAngle >= MINIMUM_VIEWING_ANGLE_DEGREES && viewingAngle <= MAXIMUM_VIEWING_ANGLE_DEGREES) {
            setViewingAngle(viewingAngle);
        } else {
            throw new IllegalArgumentException("Wrong viewing angle");
        }

        // round studied area to steps (one step - 1.5 degrees)
        studiedAreaHalf = Math.round(studiedAreaHalf / AZIMUTH_STEP) * AZIMUTH_STEP;

        drawer2D.clearRadar();
        drawRadarGrid();

        scanningCycle();
    }

    /**
     * Scans and analyzes studied area
     * Starts from near limit angle and finishes in other with step 1.5 degrees
     * @throws DeviceIOException
     */
    protected void scanningCycle() throws DeviceIOException {
        currentAzimuthDegree = (locatorDeviceDriver.getCurrentAzimuth() - ZERO_AZIMUTH) * AZIMUTH_STEP;

        double startAzimuthDegrees;
        double finishAzimuthDegrees;
        if (currentAzimuthDegree > 0) {
            startAzimuthDegrees = studiedAreaHalf;
            finishAzimuthDegrees = - studiedAreaHalf;
        } else {
            startAzimuthDegrees = - studiedAreaHalf;
            finishAzimuthDegrees = studiedAreaHalf;
        }

        double azimuthStep = (startAzimuthDegrees > 0) ? (- AZIMUTH_STEP) : AZIMUTH_STEP;

        // set antennas azimuth to the nearest border of studied area
        if (currentAzimuthDegree != startAzimuthDegrees) {
            setAzimuth(startAzimuthDegrees);
        }

        averagedObjects = new ArrayList<>();

        currentAzimuthDegree = startAzimuthDegrees - azimuthStep;
        do {
            currentAzimuthDegree += azimuthStep;
            setAzimuth(currentAzimuthDegree);
            processScan();
            Platform.runLater(() -> controller.redrawRadar(maxDistance));
            if (isInterrupted()) {
                break;
            }
        } while ((finishAzimuthDegrees >= 0 && currentAzimuthDegree < finishAzimuthDegrees)     // - -> +
                || (finishAzimuthDegrees < 0 && currentAzimuthDegree > finishAzimuthDegrees));  // + -> -

    }

    /**
     * Makes one scan and processes the data of the scan
     * Results are added into @field {@code averagedObjects}
     */
    protected void processScan() throws DeviceIOException {
        List<DetectedObject2D> detectedObjects = detectAllObjects(
                filtering(scan(maxDistance, periodsOfRadiations)));
        Collections.sort(detectedObjects, detectedObjectsComparator);

        this.averagedObjects.addAll(averagingObjectsData(detectedObjects, OBJECTS_COLOR));
    }

    /**
     * Displays mined data
     */
    protected void drawRadarData() {
        if (averagedObjects != null) {
            averagedObjects.stream().filter(detectedObject -> detectedObject.getDistance() <= maxDistance
                    && Math.abs(detectedObject.getAzimuth()) <= studiedAreaHalf).forEach(this::drawObject);
        }
    }

    @Override
    protected void drawCurrentDirection() {
        drawer2D.setColor(Drawer2D.RADAR_VIEWING_SCOPE_BORDER_COLOR);
        drawRadialLine(+studiedAreaHalf);
        drawRadialLine(-studiedAreaHalf);
        super.drawCurrentDirection();
    }

    /**
     * Turns locator's antenna into specified azimuth
     * @param azimuthInDegree new azimuth of locator's antenna
     * @throws DeviceIOException
     */
    protected void setAzimuth(double azimuthInDegree) throws DeviceIOException {
        int azimuth = (int) Math.round(azimuthInDegree / AZIMUTH_STEP + ZERO_AZIMUTH);
        locatorDeviceDriver.setAzimuth(azimuth);
        //currentAzimuthDegree = (locatorDeviceDriver.getCurrentAzimuth() - ZERO_AZIMUTH) * AZIMUTH_STEP;
    }

}
