package com.locatordevice.algorithms;

import com.locatordevice.MainApp;
import com.locatordevice.beans.DetectedObject3D;
import com.locatordevice.beans.ScanData;
import com.locatordevice.comio.enums.TransferType;
import com.locatordevice.comio.exceptions.DeviceIOException;
import com.locatordevice.comio.interfaces.LocatorDeviceDriver;
import com.locatordevice.utils.MathUtil;
import com.locatordevice.view.Drawer2D;

import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.locatordevice.comio.LocatorConstants.*;
import static com.locatordevice.comio.LocatorConstants.LENGTH_OF_WAVE;

/**
 * Abstract class with shared methods of 3D algorithms
 * @version 1.0
 * @author Morhun N.G.
 */
public abstract class Algorithm3D {

    public static final int MINIMUM_VIEWING_ANGLE_DEGREES = 0;
    public static final int MAXIMUM_VIEWING_ANGLE_DEGREES = 180;

    /** radar visualization constants */
    protected static final int RADAR_LEFT_INDENT = 50;
    protected static final int RADAR_TOP_INDENT = 50;
    protected static final int RADAR_HORIZONTAL_RANGE = 450;
    protected static final int RADAR_VERTICAL_RANGE = 300;
    protected static final int RADAR_X_ORIGIN = RADAR_LEFT_INDENT + RADAR_HORIZONTAL_RANGE / 2;
    protected static final int RADAR_Y_ORIGIN = RADAR_TOP_INDENT + RADAR_VERTICAL_RANGE / 2;
    protected static final int GRID_STEP = 75;
    protected static final int MAXIMUM_VIEWING_HORIZONTAL_ANGLE_ON_RADAR = 90;
    protected static final int MAXIMUM_VIEWING_VERTICAL_ANGLE_ON_RADAR = 60;
    protected static final int NUMBER_OF_HORIZONTAL_GRID_LINES = 5;
    protected static final int NUMBER_OF_VERTICAL_GRID_LINES = 7;
    protected static final int GRID_LABELS_STEP = 30;
    protected static final int RADAR_ANGLES_LABELS_Y_POSITION = RADAR_TOP_INDENT + RADAR_VERTICAL_RANGE + 20;
    protected static final int RADAR_ANGLES_LABELS_X_POSITION = RADAR_LEFT_INDENT + RADAR_HORIZONTAL_RANGE + 20;
    protected static final int SCALE_LEFT_INDENT = RADAR_LEFT_INDENT + RADAR_HORIZONTAL_RANGE + 80;
    protected static final int SCALE_WIDTH = 40;
    protected static final int RADAR_SCALE_LABELS_X_POSITION = SCALE_LEFT_INDENT + SCALE_WIDTH + 15;
    protected static final int SYMBOL_CORRECTION_SHIFT = 5;
    protected static final int MOUSE_COORDINATES_X_LENGTH = 135;            // ro
    protected static final int MOUSE_COORDINATES_X_HORIZONTAL_ANGLE = 250;  // phi
    protected static final int MOUSE_COORDINATES_X_VERTICAL_ANGLE = 350;    // theta
    protected static final int MOUSE_COORDINATES_Y = 25;

    /** size of detected object on radar */
    protected static final int OBJECT_SIZE = 17;
    /** rules for sorting detected objects */
    protected static final Comparator<DetectedObject3D> detectedObjectsComparator
            = (o1, o2) -> (int) Math.signum(o2.getDistance() - o1.getDistance()); // from far to near

    /** pointer to radar drawer */
    protected Drawer2D drawer2D;
    /** pointer to device driver */
    protected LocatorDeviceDriver locatorDeviceDriver;
    /** list of detected objects */
    protected List<DetectedObject3D> detectedObjects;
    /** filters for obtained data */
    protected Filter filter;
    /** last required maximal distance */
    protected int maxDistance = 0;
    /** current azimuth of radar's antenna */
    protected double currentAzimuthDegree;
    /**
     * Half of maximum angle scanning (degrees).
     * Ranges from 0+ to 90 degrees.
     * Supposed, that horizontal and vertical viewing angles are equals.
     * Origin is current azimuth of antenna.
     */
    protected double viewingAngleHalf;
    /**
     * Maximum number of samples for {@code viewingAngleHalf}.
     * If shift between signals in two channels is less then nMax
     * than we consider that it is same object.
     * It used for calculating azimuth of object in space.
     */
    protected int nMaxHorizontal;
    protected int nMaxVertical;
    /**
     * Snapshot of radar. Used for drawing distance under mouse pointer.
     * It is used, because canvas (and its graphics context) hasn't PixelReader.
     */
    WritableImage radarSnapshot;


    public Algorithm3D(MainApp mainAppPointer) {
        this.locatorDeviceDriver = mainAppPointer.getLocatorDeviceDriver();
        this.drawer2D = mainAppPointer.getDrawer2D();
        this.filter = new Filter();
        setupFilter();

        this.currentAzimuthDegree = getCurrentAzimuthDegrees();
        radarSnapshot = new WritableImage(drawer2D.getCanvasWidth(), drawer2D.getCanvasHeight());
    }

    /**
     * Sets the angle within which space will be analyzed in one scan
     * Zero angle is current azimuth of antenna
     * @param viewingAngle viewing angle, degrees
     */
    public void setViewingAngle(double viewingAngle) {
        if (viewingAngle >= MINIMUM_VIEWING_ANGLE_DEGREES && viewingAngle <= MAXIMUM_VIEWING_ANGLE_DEGREES) {
            this.viewingAngleHalf = viewingAngle / 2;
            calculateNMax();
        }
    }

    /**
     * Calculates maximum number of samples (if signals shift in two channels less then
     * this number, considered that it is one signal (object in space) with some (non-zero) azimuth)
     * in horizontal and vertical planes corresponding to viewing angle field.
     * It is used for calculating azimuth of object in plane.
     */
    protected void calculateNMax() {
        this.nMaxHorizontal = (int) Math.floor(HORIZONTAL_DISTANCE_BETWEEN_RECEIVERS * 1e-3 / (LENGTH_OF_WAVE / SAMPLES_PER_PERIOD)
                * Math.sin(MathUtil.degToRad(viewingAngleHalf)));
        this.nMaxVertical = (int) Math.floor(VERTICAL_DISTANCE_BETWEEN_RECEIVERS * 1e-3 / (LENGTH_OF_WAVE / SAMPLES_PER_PERIOD)
                * Math.sin(MathUtil.degToRad(viewingAngleHalf)));
    }

    /**
     * Configurations of filters which used in algorithm
     */
    protected void setupFilter() {
        filter.setFpRestoreLostSignalPeriodsForRestore(2);
        filter.setFpClearDazzledZonePeriodsOfZerosToStop(1);
        filter.setFpDeleteNoiseMaximumNumberOfOnesForRemoval(3);
    }

    /**
     * Filtering data of scan from noise
     * @param dataForFiltering data for filtering
     */
    protected ScanData filtering(ScanData dataForFiltering) {
        ScanData data = ScanDataProcessor.clone(dataForFiltering);

        byte[][] channels = data.getChannels();

        for (int i = 0; i < NUMBER_OF_RECEIVERS; i++) {
            filteringChannel(channels[i]);
        }
        return data;
    }

    /**
     * Apply filters for channel
     * @param channel channel for filtering
     */
    protected void filteringChannel(byte[] channel) {
        filter.restoreLostSignal(channel);
        int i = filter.clearDazzledZone(channel);
        filter.deleteNoise(channel, i, channel.length);
    }

    /**
     * Detects objects in space.
     * It used three receivers only.
     * RDn channel was excluded, because it has maximal noise
     * @param data data of scan
     * @return list of detected objects
     */
    protected List<DetectedObject3D> detectAllObjects(ScanData data) {
        byte[] LDn = data.getLDn();
        byte[] LUp = data.getLUp();
        byte[] RUp = data.getRUp();
        int channelSize = data.getChannelSize();
        int periods = data.getNumberOfPeriodsOfRadiation();

        List<DetectedObject3D> foundObjects = new ArrayList<>();

        int nLeftUp, nRightUp, nLeftDn;
        int i, j;
        int zerosCounter;

        i = 0; // let LUp will be main channel
        while (i < channelSize) {
            // find five consecutive zeros in LUp channel
            zerosCounter = 0;
            while (i < channelSize && zerosCounter < SAMPLES_PER_PERIOD) {
                if (LUp[i] == 0) {
                    zerosCounter++;
                } else {
                    zerosCounter = 0;
                }
                i++;
            }
            // find first one in LUp channel
            while (i < channelSize && LUp[i] == 0) {
                i++;
            }
            if (i < channelSize) {
                // we found one in LUp channel
                nLeftUp = i;
                nLeftDn = nRightUp = -1;

                /* find one after five consecutive zeros in LDn channel */
                j = i - nMaxVertical - (SAMPLES_PER_PERIOD + 1);
                // find five consecutive zeros
                zerosCounter = 0;
                while (j < channelSize && j < (nLeftUp + nMaxVertical) && zerosCounter < SAMPLES_PER_PERIOD) {
                    if (LDn[j] == 0) {
                        zerosCounter++;
                    } else {
                        zerosCounter = 0;
                    }
                    j++;
                }
                if (zerosCounter != SAMPLES_PER_PERIOD) { // five consecutive zeros did not find
                    i++;
                    continue; // next try to find object
                }
                // find one
                while (j < channelSize && j < (nLeftUp + nMaxVertical)) {
                    if (LDn[j] == 1) {
                        nLeftDn = j; // corresponding signal found in LDn channel
                        break;
                    }
                    j++;
                }
                if (nLeftDn == -1) { // one did not find
                    i++;
                    continue; // next try to find object
                }

                /* find one after five consecutive zeros in RUp channel */
                j = i - nMaxHorizontal - (SAMPLES_PER_PERIOD + 1);
                // find five consecutive zeros
                zerosCounter = 0;
                while (j < channelSize && j < (nLeftUp + nMaxHorizontal) && zerosCounter < SAMPLES_PER_PERIOD) {
                    if (RUp[j] == 0) {
                        zerosCounter++;
                    } else {
                        zerosCounter = 0;
                    }
                    j++;
                }
                if (zerosCounter != SAMPLES_PER_PERIOD) { // five consecutive zeros did not find
                    i++;
                    continue; // next try to find object
                }
                // find one
                while (j < channelSize && j < (nLeftUp + nMaxHorizontal)) {
                    if (RUp[j] == 1) {
                        nRightUp = j; // corresponding signal found in RUp channel
                        break;
                    }
                    j++;
                }
                if (nRightUp == -1) { // one did not find
                    i++;
                    continue; // next try to find object
                }

                foundObjects.add(detectObject(nLeftUp, nRightUp, nLeftDn, periods));
                i++;
            }
        } // try to find next object

        return foundObjects;
    }

    /**
     * Calculates distance and full (horizontal and vertical) azimuth of object
     * @param nLeftUp serial number of sample of begin of object's echo signal in left up channel
     * @param nRightUp serial number of sample of begin of object's echo signal in right up channel
     * @param nLeftDn serial number of sample of begin of object's echo signal in left down channel
     * @param periods periods of radiation of scan
     * @return detected object in space
     */
    protected DetectedObject3D detectObject(int nLeftUp, int nRightUp, int nLeftDn, int periods) {
        double distance = 0.5 * (
                (periods + PERIODS_OF_PAUSE_TIME) * SAMPLES_PER_PERIOD
                        + (nLeftUp + nRightUp + nLeftDn) / 3) * LENGTH_OF_WAVE / SAMPLES_PER_PERIOD;
        double horizontalAngle = MathUtil.radToDeg(Math.asin(
                ((nLeftUp - nRightUp) * LENGTH_OF_WAVE / SAMPLES_PER_PERIOD) / (HORIZONTAL_DISTANCE_BETWEEN_RECEIVERS * 1e-3)));
        horizontalAngle += getCurrentAzimuthDegrees(); // take into account current azimuth of the antenna
        double verticalAngle = MathUtil.radToDeg(Math.asin(
                ((nLeftDn - nLeftUp) * LENGTH_OF_WAVE / SAMPLES_PER_PERIOD) / (VERTICAL_DISTANCE_BETWEEN_RECEIVERS * 1e-3)));

        return new DetectedObject3D(distance, horizontalAngle, verticalAngle);
    }

    /**
     * Draws grid on radar
     */
    protected void drawRadarGrid() {
        drawer2D.setColor(Drawer2D.RADAR_GRID_COLOR);
        drawRadarGridStatic();
        drawRadarGridDynamic();
    }

    /**
     * Draws part of radar's grid, which same for all distances.
     */
    protected void drawRadarGridStatic() {
        //drawer2D.drawRectangle(RADAR_LEFT_INDENT, RADAR_TOP_INDENT, RADAR_HORIZONTAL_RANGE, RADAR_VERTICAL_RANGE);

        int y;
        for (int i = 0, labelValue = MAXIMUM_VIEWING_VERTICAL_ANGLE_ON_RADAR; i < NUMBER_OF_HORIZONTAL_GRID_LINES; i++, labelValue -= GRID_LABELS_STEP) {
            y = RADAR_TOP_INDENT + i * GRID_STEP;
            drawer2D.drawLine(RADAR_LEFT_INDENT, y,
                    RADAR_LEFT_INDENT + RADAR_HORIZONTAL_RANGE, y);
            drawer2D.drawText(Integer.toString(labelValue) + "°", RADAR_ANGLES_LABELS_X_POSITION, y);
        }

        int x;
        for (int i = 0, labelValue = -MAXIMUM_VIEWING_HORIZONTAL_ANGLE_ON_RADAR; i < NUMBER_OF_VERTICAL_GRID_LINES; i++, labelValue += GRID_LABELS_STEP) {
            x = RADAR_LEFT_INDENT + i * GRID_STEP;
            drawer2D.drawLine(x, RADAR_TOP_INDENT,
                    x, RADAR_TOP_INDENT + RADAR_VERTICAL_RANGE);
            drawer2D.drawText(String.format("%3d°", labelValue),
                    x - 2 * SYMBOL_CORRECTION_SHIFT, RADAR_ANGLES_LABELS_Y_POSITION);
        }

        drawScale();
    }

    /**
     * Draws scale colors
     */
    protected void drawScale() {
        int r, g, b;    // 0 .. 255
        int n;          // 0 .. (255 * 2)
        int flag;       // 0 or 1

        int y;
        for (int i = 0; i < RADAR_VERTICAL_RANGE; i++) {
            n = Math.round((float) 510 * i / RADAR_VERTICAL_RANGE);
            flag = n / 256;
            r = (255 - n) * (1 - flag);
            g = n * (1 - flag) + (510 - n) * flag;
            b = (n - 255) * flag;
            drawer2D.setColor(Color.rgb(r, g, b));

            y = RADAR_VERTICAL_RANGE + RADAR_TOP_INDENT - i;
            drawer2D.drawLine(SCALE_LEFT_INDENT, y, SCALE_LEFT_INDENT + SCALE_WIDTH, y);
        }
    }

    /**
     * Draws dynamic part of radar's grid witch depends from {@code maxDistance}
     */
    protected void drawRadarGridDynamic() {
        drawer2D.setColor(Drawer2D.RADAR_GRID_COLOR);

        float step = (float) RADAR_VERTICAL_RANGE / maxDistance;

        for (int i = 0; i <= maxDistance; i++) {
            drawer2D.drawText(Integer.toString(maxDistance - i) + " m",
                    RADAR_SCALE_LABELS_X_POSITION, Math.round(RADAR_TOP_INDENT + step * i));
        }

        if (maxDistance == 2) {
            for (int i = 1 ; i <= 3; i += 2) {
                drawer2D.drawText(String.format("%.1f m",maxDistance * (1.0 - (double)i/4)),
                        RADAR_SCALE_LABELS_X_POSITION, RADAR_TOP_INDENT + RADAR_VERTICAL_RANGE / 4 * i);
            }
        } else if (maxDistance == 1) {
            for (int i = 1 ; i <= 3; i++) {
                drawer2D.drawText(String.format("%.2f m", maxDistance * (1.0 - (double)i/4)),
                        RADAR_SCALE_LABELS_X_POSITION, RADAR_TOP_INDENT + RADAR_VERTICAL_RANGE / 4 * i);
            }
        }
    }

    /**
     * Redraw radar's data corresponding to new parameters
     * this method do not get any data from device
     * @param maxDistance maximal distance on radar
     */
    public void redrawRadar(final int maxDistance) {
        this.maxDistance = maxDistance;

        drawer2D.clearRadar();
        drawRadarGrid();
        drawCurrentDirection();

        drawRadarData();
    }

    /**
     * Draws current azimuth of radar's antenna
     */
    protected void drawCurrentDirection() {
        drawer2D.setColor(Drawer2D.RADAR_CURRENT_AZIMUTH_COLOR);
        int x = (int) (RADAR_X_ORIGIN + getCurrentAzimuthDegrees() * (RADAR_HORIZONTAL_RANGE / 2) / MAXIMUM_VIEWING_HORIZONTAL_ANGLE_ON_RADAR);
        drawer2D.drawLine(x, RADAR_TOP_INDENT, x, RADAR_TOP_INDENT + RADAR_VERTICAL_RANGE);
    }

    /**
     * Draws objects on radar
     */
    protected void drawRadarData() {
        if (detectedObjects != null) {
            for (DetectedObject3D object3D : detectedObjects) {
                if (object3D.getDistance() <= maxDistance) {
                    drawObject(object3D.getDistance(), object3D.getHorizontalAzimuth(), object3D.getVerticalAzimuth());
                }
            }
        }
    }

    /**
     * Draws object on radar
     * @param distance distance to object
     * @param horizontalAngle horizontal azimuth to object in degrees
     * @param verticalAngle vertical azimuth to object in degrees
     */
    public void drawObject(double distance, double horizontalAngle, double verticalAngle) {
        int x = (int) (RADAR_X_ORIGIN + horizontalAngle * (RADAR_HORIZONTAL_RANGE / 2) / MAXIMUM_VIEWING_HORIZONTAL_ANGLE_ON_RADAR);
        int y = (int) (RADAR_Y_ORIGIN - verticalAngle * (RADAR_VERTICAL_RANGE / 2) / MAXIMUM_VIEWING_VERTICAL_ANGLE_ON_RADAR);

        drawer2D.setColor(getObjectColorByDistance(distance));
        drawer2D.drawFilledCircle(x, y, OBJECT_SIZE);
    }

    /**
     * Calculates color of object in current scale.
     * Scale is from 0 to {@code maxDistance}
     * 0 corresponds red, maxDistance corresponds blue
     * @param distance distance to thr object
     * @return color of object in current scale
     */
    protected Color getObjectColorByDistance(double distance) {
        int n = Math.round((float) distance * 510 / maxDistance); // 255*2 == 510
        int flag = n / 256;
        int r = (255 - n) * (1 - flag);
        int g = n * (1 - flag) + (510 - n) * flag;
        int b = (n - 255) * flag;
        return Color.rgb(r, g, b);
    }

    /**
     * Draws coordinates under mouse cursor in polar coordinates
     */
    public void drawMouseCoordinates(int x, int y) {
        drawer2D.setColor(Drawer2D.RADAR_COORDINATES_COLOR);
        eraseMouseCoordinates();

        if (x < RADAR_LEFT_INDENT || x > (RADAR_LEFT_INDENT + RADAR_HORIZONTAL_RANGE)
                || y < RADAR_TOP_INDENT || y > (RADAR_TOP_INDENT + RADAR_VERTICAL_RANGE)) {
            drawer2D.drawText("- - - - -", MOUSE_COORDINATES_X_HORIZONTAL_ANGLE, MOUSE_COORDINATES_Y);
        } else {
            String distanceText;
            Color tmp = radarSnapshot.getPixelReader().getColor(x, y);
            Color colorUnderMousePointer = Color.color(tmp.getRed(), tmp.getGreen(), tmp.getBlue(), 1.0); // fix 0.0 opacity
            if (colorUnderMousePointer.equals(Drawer2D.RADAR_BACKGROUND_COLOR)
                    || colorUnderMousePointer.equals(Drawer2D.RADAR_GRID_COLOR)
                    || colorUnderMousePointer.equals(Drawer2D.RADAR_CURRENT_AZIMUTH_COLOR)
                    || colorUnderMousePointer.equals(Drawer2D.RADAR_VIEWING_SCOPE_BORDER_COLOR)) {
                distanceText = "dist:    - - - -";
            } else {
                int r = (int) Math.round(colorUnderMousePointer.getRed() * 255);
                int g = (int) Math.round(colorUnderMousePointer.getGreen() * 255);
                int b = (int) Math.round(colorUnderMousePointer.getBlue() * 255);
                double distance = (double) maxDistance / (2*255)
                        * ((255 - r + g) / 2 * (1 - Math.signum(b)) + (255 + (255 - g + b) / 2) * Math.signum(b));
                distanceText = String.format("dist: %.3f m", distance);
            }

            double horizontalAngle = (x - RADAR_LEFT_INDENT - RADAR_HORIZONTAL_RANGE / 2) * MAXIMUM_VIEWING_HORIZONTAL_ANGLE_ON_RADAR / (RADAR_HORIZONTAL_RANGE / 2);
            double verticalAngle = (RADAR_TOP_INDENT + RADAR_VERTICAL_RANGE / 2 - y) * MAXIMUM_VIEWING_VERTICAL_ANGLE_ON_RADAR / (RADAR_VERTICAL_RANGE / 2);

            /*drawer2D.drawText("ρ : " + distanceText, MOUSE_COORDINATES_X_LENGTH, MOUSE_COORDINATES_Y);
            drawer2D.drawText(String.format("φ : %.1f", horizontalAngle), MOUSE_COORDINATES_X_HORIZONTAL_ANGLE, MOUSE_COORDINATES_Y);
            drawer2D.drawText(String.format("θ : %.1f", verticalAngle), MOUSE_COORDINATES_X_VERTICAL_ANGLE, MOUSE_COORDINATES_Y);*/

            drawer2D.drawText(distanceText, MOUSE_COORDINATES_X_LENGTH, MOUSE_COORDINATES_Y);
            drawer2D.drawText(String.format("hor: %.1f°", horizontalAngle), MOUSE_COORDINATES_X_HORIZONTAL_ANGLE, MOUSE_COORDINATES_Y);
            drawer2D.drawText(String.format("ver: %.1f°", verticalAngle), MOUSE_COORDINATES_X_VERTICAL_ANGLE, MOUSE_COORDINATES_Y);
        }
    }

    /**
     * Erase coordinates of mouse cursor on radar's canvas
     */
    public void eraseMouseCoordinates() {
        drawer2D.eraseRectangle(MOUSE_COORDINATES_X_LENGTH, MOUSE_COORDINATES_Y - 10,
                MOUSE_COORDINATES_X_VERTICAL_ANGLE - MOUSE_COORDINATES_X_LENGTH + 100, 25);
    }

    /**
     * Save snapshot of radar in {@code radarSnapshot} field.
     * This snapshot used for getting distance to object under mouse cursor.
     */
    public void updateCanvasSnapshot() {
        radarSnapshot = drawer2D.takeCanvasSnapshot();
    }

    /**
     * Makes scan with specified parameters
     * @param requiredDistance maximum distance
     * @param periods periods of radiation
     * @return data of scan
     */
    protected ScanData scan(final int requiredDistance, final int periods) throws DeviceIOException {
        return ScanDataProcessor.getScanData(locatorDeviceDriver.scanSpace(
                TransferType.getTransferTypeByDistance(requiredDistance), periods));
    }

    /**
     * @return current azimuth of locator's antenna in degrees
     */
    protected double getCurrentAzimuthDegrees() {
        return (locatorDeviceDriver.getCurrentAzimuth() - ZERO_AZIMUTH) * AZIMUTH_STEP;
    }

    /**
     * @return current maximum distance on radar in algorithm
     */
    public int getMaxDistance() {
        return maxDistance;
    }


}
