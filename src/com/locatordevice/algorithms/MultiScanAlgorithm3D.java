package com.locatordevice.algorithms;

import com.locatordevice.MainApp;
import com.locatordevice.beans.DetectedObject3D;
import com.locatordevice.comio.exceptions.DeviceIOException;
import com.locatordevice.controller.tabs.MainWindowMultiScan3DTabController;
import com.locatordevice.view.Drawer2D;
import javafx.application.Platform;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.locatordevice.comio.LocatorConstants.*;

/**
 * Algorithm of locate object in space
 * @version 1.0
 * @author Morhun N.G.
 */
public class MultiScanAlgorithm3D extends Algorithm3D {

    public static final int MINIMUM_STUDIED_AREA_ANGLE = 3;
    public static final int MAXIMUM_STUDIED_AREA_ANGLE = 180;

    /** pointer to own controller */
    protected MainWindowMultiScan3DTabController controller;
    /** number of periods of radiation for scans */
    protected int periodsOfRadiations;
    /** half of angle in which space is analyzed */
    protected double studiedAreaHalf;
    /** flag shows, when algorithm must be interrupted */
    protected boolean stopped;

    public MultiScanAlgorithm3D(MainApp mainAppPointer, MainWindowMultiScan3DTabController controller) {
        super(mainAppPointer);
        this.controller = controller;

        this.studiedAreaHalf = 35;
    }

    public synchronized boolean isInterrupted() {
        return stopped;
    }

    public synchronized void sendInterruptSignal() {
        this.stopped = true;
    }

    public synchronized void prepareToStart() {
        this.stopped = false;
    }

    public void runAlgorithm(final int requiredDistance, final int periods,
                             final double studiedArea, final double viewingAngle)
            throws DeviceIOException {
        if (requiredDistance > 0 && requiredDistance <= MAXIMUM_DISTANCE_OF_SCAN) {
            this.maxDistance = requiredDistance;
        } else {
            throw new IllegalArgumentException("Wrong distance");
        }
        if (periods >= MINIMUM_PERIODS_OF_RADIATION && periods <= MAXIMUM_PERIODS_OF_RADIATION) {
            this.periodsOfRadiations = periods;
        } else {
            throw new IllegalArgumentException("Wrong number of periods of radiation");
        }
        if (studiedArea >= MINIMUM_STUDIED_AREA_ANGLE && studiedArea <= MAXIMUM_STUDIED_AREA_ANGLE) {
            studiedAreaHalf = studiedArea / 2;
        } else {
            throw new IllegalArgumentException("Wrong studied area");
        }
        if (viewingAngle >= MINIMUM_VIEWING_ANGLE_DEGREES && viewingAngle <= MAXIMUM_VIEWING_ANGLE_DEGREES) {
            setViewingAngle(viewingAngle);
        } else {
            throw new IllegalArgumentException("Wrong viewing angle");
        }

        // round studied area to steps (one step - 1.5 degrees)
        studiedAreaHalf = Math.round(studiedAreaHalf / AZIMUTH_STEP) * AZIMUTH_STEP;

        drawer2D.clearRadar();
        drawRadarGrid();

        scanningCycle();
    }

    protected void scanningCycle() throws DeviceIOException {
        currentAzimuthDegree = getCurrentAzimuthDegrees();

        double startAzimuthDegrees;
        double finishAzimuthDegrees;
        if (currentAzimuthDegree > 0) {
            startAzimuthDegrees = studiedAreaHalf;
            finishAzimuthDegrees = - studiedAreaHalf;
        } else {
            startAzimuthDegrees = - studiedAreaHalf;
            finishAzimuthDegrees = studiedAreaHalf;
        }

        double azimuthStep = (startAzimuthDegrees > 0) ? (- AZIMUTH_STEP) : AZIMUTH_STEP;

        // set antennas azimuth to the nearest border of studied area
        if (currentAzimuthDegree != startAzimuthDegrees) {
            setAzimuth(startAzimuthDegrees);
        }

        detectedObjects = new ArrayList<>();

        currentAzimuthDegree = startAzimuthDegrees - azimuthStep;
        do {
            currentAzimuthDegree += azimuthStep;
            setAzimuth(currentAzimuthDegree);
            processScan();
            Platform.runLater(() -> {
                controller.redrawRadar(maxDistance);
                updateCanvasSnapshot();
            });
            if (isInterrupted()) {
                break;
            }
        } while ((finishAzimuthDegrees >= 0 && currentAzimuthDegree < finishAzimuthDegrees)     // - -> +
                || (finishAzimuthDegrees < 0 && currentAzimuthDegree > finishAzimuthDegrees));  // + -> -

    }

    /**
     * Makes one scan and processes the data of the scan
     * Results are added into @field {@code averagedObjects}
     */
    protected void processScan() throws DeviceIOException {
        List<DetectedObject3D> objects3D = detectAllObjects(
                filtering(scan(maxDistance, periodsOfRadiations)));
        Collections.sort(objects3D, detectedObjectsComparator);

        this.detectedObjects.addAll(objects3D);
    }

    /**
     * Draws current azimuth of antenna on radar
     * and borders of scanning
     */
    protected void drawCurrentDirection() {
        drawer2D.setColor(Drawer2D.RADAR_VIEWING_SCOPE_BORDER_COLOR);
        int shift = (int) (studiedAreaHalf * (RADAR_HORIZONTAL_RANGE / 2) / MAXIMUM_VIEWING_HORIZONTAL_ANGLE_ON_RADAR);
        int xLeft = RADAR_X_ORIGIN - shift;
        int xRight = RADAR_X_ORIGIN + shift;

        drawer2D.drawLine(xLeft, RADAR_TOP_INDENT, xLeft, RADAR_TOP_INDENT + RADAR_VERTICAL_RANGE);
        drawer2D.drawLine(xRight, RADAR_TOP_INDENT, xRight, RADAR_TOP_INDENT + RADAR_VERTICAL_RANGE);

        super.drawCurrentDirection();
    }

    /**
     * Turns locator's antenna into specified azimuth
     * @param azimuthInDegree new azimuth of locator's antenna
     * @throws DeviceIOException
     */
    protected void setAzimuth(double azimuthInDegree) throws DeviceIOException {
        int azimuth = (int) Math.round(azimuthInDegree / AZIMUTH_STEP + ZERO_AZIMUTH);
        locatorDeviceDriver.setAzimuth(azimuth);
    }

}
