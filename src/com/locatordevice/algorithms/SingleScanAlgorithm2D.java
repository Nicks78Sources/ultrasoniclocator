package com.locatordevice.algorithms;

import com.locatordevice.MainApp;
import com.locatordevice.beans.DetectedObject2D;
import com.locatordevice.beans.ScanData;
import com.locatordevice.comio.exceptions.DeviceIOException;

import com.locatordevice.view.Drawer2D;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.locatordevice.comio.LocatorConstants.*;

/**
 * Algorithm of locate objects in horizontal plane with one scan
 * @version 1.0
 * @author Morhun N.G.
 */
public class SingleScanAlgorithm2D extends Algorithm2D {

    private static final Color AVERAGED_OBJECT_COLOR = Color.YELLOW;

    /** last scan */
    protected ScanData lastScanRawData = null;
    /** filtered data of last scan, creating if it is required */
    protected ScanData lastScanFilteredData;
    /** list of detected objects per one scan */
    protected List<DetectedObject2D> detectedObjects;
    /** list of averaged objects */
    protected List<DetectedObject2D> averagedObjects;
    /** display averaging or raw data */
    private boolean isAveraging;

    public SingleScanAlgorithm2D(final MainApp mainAppPointer) {
        super(mainAppPointer);

        this.detectedObjects = new ArrayList<>();
        isAveraging = true;
    }

    public void setAveraging(boolean isAveraging) {
        this.isAveraging = isAveraging;
    }

    /**
     * @return obtained data of last scan
     */
    public ScanData getLastScanRawData() {
        return lastScanRawData;
    }

    /**
     * Scanning space and drawing obtained data on radar
     * @param requiredDistance maximal distance that is required by user
     * @param periods periods of radiation
     */
    public void runAlgorithm(final int requiredDistance, final int periods) throws DeviceIOException {
        lastScanRawData = scan(requiredDistance, periods);
        lastScanFilteredData = filtering(lastScanRawData);
        currentAzimuthDegree = (locatorDeviceDriver.getCurrentAzimuth() - ZERO_AZIMUTH) * AZIMUTH_STEP;
        averagedObjects = null;
        detectObjects();
        redrawRadar(requiredDistance);
    }

    /**
     * Detects objects according to the obtained signal
     */
    protected void detectObjects() {
        detectedObjects.clear();

        detectedObjects = detectAllObjects(lastScanFilteredData);

        Collections.sort(detectedObjects, detectedObjectsComparator);
    }

    /**
     * Draws detected objects on radar's canvas
     * corresponding to current settings
     */
    protected void drawRadarData() {
        if (lastScanRawData != null) {
            if (isAveraging) {
                if (averagedObjects == null) {
                    averagedObjects = averagingObjectsData(detectedObjects, AVERAGED_OBJECT_COLOR);
                }
                drawObjectsOnRadar(averagedObjects);
            } else {
                drawObjectsOnRadar(detectedObjects);
            }
        }
    }

    /**
     * Draws current azimuth(antenna's direction) and viewing scope on radar
     */
    protected void drawCurrentDirection() {
        super.drawCurrentDirection();
        drawCurrentViewingScope(currentAzimuthDegree);
    }

    /**
     * Draws current borders of viewing area
     * If the border is more than 90 degrees, it is taken as 90 degrees
     * @param currentAzimuth current azimuth in degrees
     */
    private void drawCurrentViewingScope(double currentAzimuth) {
        drawer2D.setColor(Drawer2D.RADAR_VIEWING_SCOPE_BORDER_COLOR);
        if (currentAzimuth + viewingAngleHalf < 90) {
            drawRadialLine(currentAzimuth + viewingAngleHalf);
        } else {
            drawRadialLine(90);
        }

        if (currentAzimuth - viewingAngleHalf > -90) {
            drawRadialLine(currentAzimuth - viewingAngleHalf);
        } else {
            drawRadialLine(-90);
        }
    }

    /**
     * Sets antenna's azimuth
     * @param azimuthInDegree new azimuth in degrees
     * @throws DeviceIOException
     */
    public void setAzimuth(double azimuthInDegree) throws DeviceIOException {
        int azimuth = (int) Math.round(azimuthInDegree / AZIMUTH_STEP + ZERO_AZIMUTH);
        locatorDeviceDriver.setAzimuth(azimuth);
        currentAzimuthDegree = (int) Math.round(
                (locatorDeviceDriver.getCurrentAzimuth() - ZERO_AZIMUTH) * AZIMUTH_STEP);

        detectedObjects.clear();
        if (averagedObjects != null) {
            averagedObjects.clear();
        }

        redrawRadar(maxDistance);
    }

}
