package com.locatordevice.algorithms;

import com.locatordevice.MainApp;
import com.locatordevice.beans.ScanData;
import com.locatordevice.comio.enums.TransferType;
import com.locatordevice.comio.exceptions.DeviceIOException;
import com.locatordevice.comio.interfaces.LocatorDeviceDriver;

import com.locatordevice.view.Drawer2D;

import static com.locatordevice.comio.LocatorConstants.*;

/**
 * Algorithm for rangefinder
 * @version 1.0
 * @author Morhun N.G.
 */
public class RangefinderAlgorithm {

    private static final int DIAGRAM_VERTICAL_RANGE = 300;
    private static final int DIAGRAM_HORIZONTAL_RANGE = 550;
    private static final int DIAGRAM_LEFT_INDENT = 50;
    private static final int DIAGRAM_TOP_INDENT = 50;
    private static final int CHANNEL_DATA_WIDTH = 100;
    private static final int CHANNEL_INDENT = 30;
    private static final int CHANNEL_WIDTH = CHANNEL_DATA_WIDTH + CHANNEL_INDENT;
    private static final int MOUSE_COORDINATES_X = 20;
    private static final int MOUSE_COORDINATES_Y = 20;
    private static final int DIAGRAM_GRID_LABELS_X_POSITION = 620;
    private static final int DIAGRAM_CHANNEL_LABELS_Y_POSITION = 375;
    private static final String[] CHANNELS_LABELS = {"LDn", "RDn", "LUp", "RUp"};
    private static final int DIAGRAM_CHANNEL_LABELS_X_STEP = 130;
    private static final int DIAGRAM_CHANNEL_LABELS_X_LEFT_INDENT = 115;

    /** pointer to device driver */
    private LocatorDeviceDriver locatorDeviceDriver;
    /** pointer to radar drawer */
    private Drawer2D drawer2D;
    /** last scan */
    private ScanData lastScanRawData;
    /** filtered data of last scan, creating if require */
    private ScanData lastScanFilteredData = null;
    /** last required maximal distance */
    private int maxDistance;
    /** filters for obtained data */
    private Filter rangefinderFilter;
    /** shows whether draw filtered data or raw data */
    private boolean filter = false;

    public RangefinderAlgorithm(final MainApp mainAppPointer) {
        this.locatorDeviceDriver = mainAppPointer.getLocatorDeviceDriver();
        this.drawer2D = mainAppPointer.getDrawer2D();

        this.rangefinderFilter = new Filter();
        // setup filter
        rangefinderFilter.setFpRestoreLostSignalPeriodsForRestore(2);
        rangefinderFilter.setFpClearDazzledZonePeriodsOfZerosToStop(1);
        rangefinderFilter.setFpDeleteNoiseMaximumNumberOfOnesForRemoval(3);
    }

    /**
     * @return data of last scan
     */
    public ScanData getLastScanRawData() {
        return lastScanRawData;
    }

    /**
     * Sets data for algorithm as if it were received from the locator
     * and drawing it on radar's canvas
     * @param dataForAnalyze scan data for displaying
     */
    public void setLastScanRawData(ScanData dataForAnalyze) {
        lastScanRawData = dataForAnalyze;
        lastScanFilteredData = null;
        redrawDistancesDiagram(dataForAnalyze.getChannelSize() / 1024); // arg is distance in m
    }

    /**
     * Sets filter flag
     * If this flag is true than will display filtered data
     * @param filter filter flag
     */
    public void setFilter(boolean filter) {
        this.filter = filter;
    }

    /**
     * Scanning space and drawing diagram of distances corresponding to each channel
     * @param requiredDistance maximal distance that is required by user
     * @param periods periods of radiation
     */
    public void drawDistancesDiagram(final int requiredDistance, final int periods) throws DeviceIOException {
        scan(requiredDistance, periods);
        redrawDistancesDiagram(requiredDistance);
    }

    /**
     * Redraw diagram of distances corresponding to new parameters
     * this method do not get any data from device
     * @param maxDistance maximal distance on drawing diagram
     */
    public void redrawDistancesDiagram(final int maxDistance) {
        this.maxDistance = maxDistance;

        drawer2D.clearRadar();
        drawDiagramGrid();

        // exit method when has no data for displaying
        if (lastScanRawData == null) {
            return;
        }

        if (!filter) {
            drawDiagramData(lastScanRawData);
        } else {
            if (lastScanFilteredData == null) {
                lastScanFilteredData = filtering(lastScanRawData);
            }
            drawDiagramData(lastScanFilteredData);
        }
    }

    /**
     * Drawing grid on radar
     * Field maxDistance is maximal value on scale
     */
    private void drawDiagramGrid() {
        drawer2D.setColor(Drawer2D.RADAR_GRID_COLOR);

        float step = (float) DIAGRAM_VERTICAL_RANGE / maxDistance;

        int y;
        for (int i = 0; i <= maxDistance; i++) {
            y = Math.round(DIAGRAM_TOP_INDENT + step * i);
            drawer2D.drawLine(DIAGRAM_LEFT_INDENT, y, DIAGRAM_LEFT_INDENT + DIAGRAM_HORIZONTAL_RANGE, y);
            drawer2D.drawText(Integer.toString(maxDistance - i) + " m", DIAGRAM_GRID_LABELS_X_POSITION, y);
        }

        if (maxDistance == 2) {
            for (int i = 1 ; i <= 3; i += 2) {
                y = DIAGRAM_TOP_INDENT + DIAGRAM_VERTICAL_RANGE / 4 * i;
                drawer2D.drawLine(DIAGRAM_LEFT_INDENT, y, DIAGRAM_LEFT_INDENT + DIAGRAM_HORIZONTAL_RANGE, y);
                drawer2D.drawText(String.format("%.1f m", maxDistance * (1.0 - (double)i/4)), DIAGRAM_GRID_LABELS_X_POSITION, y);
            }
        } else if (maxDistance == 1) {
            for (int i = 1 ; i <= 3; i++) {
                y = DIAGRAM_TOP_INDENT + DIAGRAM_VERTICAL_RANGE / 4 * i;
                drawer2D.drawLine(DIAGRAM_LEFT_INDENT, y, DIAGRAM_LEFT_INDENT + DIAGRAM_HORIZONTAL_RANGE, y);
                drawer2D.drawText(String.format("%.2f m", maxDistance * (1.0 - (double)i/4)), DIAGRAM_GRID_LABELS_X_POSITION, y);
            }
        }

        for (int i = 0; i < 4; i++) {
            drawer2D.drawText(CHANNELS_LABELS[i],
                    DIAGRAM_CHANNEL_LABELS_X_LEFT_INDENT + i * DIAGRAM_CHANNEL_LABELS_X_STEP,
                    DIAGRAM_CHANNEL_LABELS_Y_POSITION);
        }
    }

    /**
     * Drawing data of scan on radar's canvas
     * @param data data for displaying
     */
    private void drawDiagramData(ScanData data) {
        byte[][] channels = data.getChannels();

        for (int i = 0; i < NUMBER_OF_RECEIVERS; i++) {
            drawChannel(channels[i], DIAGRAM_LEFT_INDENT + CHANNEL_INDENT + i * CHANNEL_WIDTH);
        }
    }

    /**
     * Draws column diagram of channel
     * @param channelData array of channel data
     * @param xShift x-coordinate of left side of channel diagram
     */
    private void drawChannel(byte[] channelData, int xShift) {
        drawer2D.setColor(Drawer2D.RADAR_DATA_COLOR);

        int periods = lastScanRawData.getNumberOfPeriodsOfRadiation();
        int iDistance = (int) ((maxDistance - SPEED_OF_SOUND
                * (TRANSMIT_PERIOD * periods + PAUSE_TIME) * 1e-6 / 2)
                / (LENGTH_OF_WAVE / SAMPLES_PER_PERIOD / 2));
        int iScanned = channelData.length;
        int iMaxOut = (iDistance > iScanned) ? iScanned : iDistance;
        int i = 0;

        int y1;
        int y2 = 0; // for java

        boolean signalFlag = false; // true if signal present
        while (i < iMaxOut) {
            if (channelData[i] == 1) {
                if (!signalFlag) {
                    y2 = yCoordinateOnDiagram(i, periods);
                    signalFlag = true;
                }
                i++;
            } else {
                if (signalFlag) {
                    if ((i+1) < iMaxOut && channelData[i+1] == 1) {
                        i += 1;
                    } else if ((i+2) < iMaxOut && channelData[i+2] == 1) {
                        i += 2;
                    } else if ((i+3) < iMaxOut && channelData[i+3] == 1) {
                        i += 3;
                    } else if ((i+4) < iMaxOut && channelData[i+4] == 1) {
                        i += 4;
                    } else {
                        y1 = yCoordinateOnDiagram(i, periods);
                        signalFlag = false;
                        drawer2D.drawFilledRectangle(xShift, y1, CHANNEL_DATA_WIDTH, y2 - y1);
                    }
                } else {
                    i++;
                }
            }
        }
        if (signalFlag) {
            y1 = yCoordinateOnDiagram(i, periods);
            // signalFlag = false;
            drawer2D.drawFilledRectangle(xShift, y1, CHANNEL_DATA_WIDTH, y2 - y1);
        }

        if (iDistance > iScanned) {
            drawer2D.setColor(Drawer2D.RADAR_NOT_SCANNED_COLOR);

            y1 = yCoordinateOnDiagram(iDistance, periods);
            y2 = yCoordinateOnDiagram(iScanned, periods);
            drawer2D.drawFilledRectangle(xShift, y1, CHANNEL_DATA_WIDTH, y2 - y1);
        }
    }

    /**
     * Calculate y-coordinate in radar's canvas in specified scale
     * @param numberOfSample sequence number of the sample
     * @param periods number of periods of radiation
     * @return y-coordinate
     */
    private int yCoordinateOnDiagram(int numberOfSample, int periods) {
        return (DIAGRAM_TOP_INDENT + DIAGRAM_VERTICAL_RANGE)
                - (int) (((SPEED_OF_SOUND * (TRANSMIT_PERIOD * periods + PAUSE_TIME) * 1e-6)
                + numberOfSample * (LENGTH_OF_WAVE / SAMPLES_PER_PERIOD)) / 2
                / maxDistance * DIAGRAM_VERTICAL_RANGE);
    }

    /**
     * Drawing coordinates under mouse cursor in diagram scale
     */
    public void drawMouseCoordinates(int x, int y) {
        drawer2D.setColor(Drawer2D.RADAR_COORDINATES_COLOR);

        String coordinates;
        if (y < DIAGRAM_TOP_INDENT || y > (DIAGRAM_TOP_INDENT + DIAGRAM_VERTICAL_RANGE)) {
            coordinates = "- - - - -";
        } else {
            double distance = (double) maxDistance / DIAGRAM_VERTICAL_RANGE * (DIAGRAM_VERTICAL_RANGE + DIAGRAM_TOP_INDENT - y);
            coordinates = String.format("%.3f m", distance);
        }

        eraseMouseCoordinates();
        drawer2D.drawText(coordinates, MOUSE_COORDINATES_X, MOUSE_COORDINATES_Y);
    }

    /**
     * Erase coordinates of mouse cursor on radar's canvas
     */
    public void eraseMouseCoordinates() {
        drawer2D.eraseRectangle(MOUSE_COORDINATES_X, MOUSE_COORDINATES_Y -10,MOUSE_COORDINATES_X+75, MOUSE_COORDINATES_Y+10);
    }

    /**
     * Filtering data of scan from noise
     * @param dataForFiltering data for filtering
     */
    private ScanData filtering(ScanData dataForFiltering) {
        ScanData data = ScanDataProcessor.clone(dataForFiltering);

        byte[][] channels = data.getChannels();

        for (int i = 0; i < NUMBER_OF_RECEIVERS; i++) {
            filteringChannel(channels[i]);
        }
        return data;
    }

    /**
     * Apply filters for channel
     * @param channel channel for filtering
     */
    private void filteringChannel(byte[] channel) {
        rangefinderFilter.restoreLostSignal(channel);
        int i = rangefinderFilter.clearDazzledZone(channel);
        rangefinderFilter.deleteNoise(channel, i, channel.length);
    }

    /**
     * Getting data from device and save in lastScanRawData field
     * @param requiredDistance maximum distance
     * @param periods periods of radiation
     */
    private void scan(final int requiredDistance, final int periods) throws DeviceIOException {
        lastScanRawData = ScanDataProcessor.getScanData(locatorDeviceDriver.scanSpace(
                TransferType.getTransferTypeByDistance(requiredDistance),periods));
        lastScanFilteredData = null;
    }

}
