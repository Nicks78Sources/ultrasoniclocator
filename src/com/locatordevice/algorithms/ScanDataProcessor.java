package com.locatordevice.algorithms;

import com.locatordevice.beans.ScanData;
import com.locatordevice.comio.DeviceAnswerData;
import com.locatordevice.comio.enums.DeviceAnswerType;
import com.locatordevice.comio.exceptions.ScanFileFormatException;
import com.locatordevice.utils.DateUtil;
import com.locatordevice.utils.FilesUtil;

import static com.locatordevice.comio.LocatorConstants.*;

import java.io.PrintWriter;

import java.io.IOException;

/**
 * Class for basic processing (convert) of scan data
 * @version 1.0
 * @author Morhun N.G.
 */
public class ScanDataProcessor {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private static final String PATH_SEPARATOR = System.getProperty("file.separator");
    private static final String PATH_TO_SCANS = "." + PATH_SEPARATOR + "scans" + PATH_SEPARATOR;
    private static final String UNPACKED_SCAN_DATA_FILE_PREFIX = "c_";
    private static final String UNPACKED_SCAN_DATA_FILE_EXTENSION = ".txt";

    private static final double DISTANCE_PER_MKS = SPEED_OF_SOUND / 2 * 1e-6;
    private static final double SPEED_OF_SOUND_IN_SM_PER_MKS = SPEED_OF_SOUND * 1e+2 * 1e-6;

    /**
     * Clone data of scan
     * @param data for copy
     * @return copy of scan data
     */
    public static ScanData clone(ScanData data) {
        ScanData copy = new ScanData();
        int channelSize = data.getChannelSize();

        copy.setAzimuth(data.getScanAzimuth());
        copy.setNumberOfPeriodsOfRadiation(data.getNumberOfPeriodsOfRadiation());
        copy.setScanTime(data.getScanTime());
        copy.setChannelSize(channelSize);

        copy.setLDn(new byte[channelSize]);
        copy.setRDn(new byte[channelSize]);
        copy.setLUp(new byte[channelSize]);
        copy.setRUp(new byte[channelSize]);

        System.arraycopy(data.getLDn(), 0, copy.getLDn(), 0, channelSize);
        System.arraycopy(data.getRDn(), 0, copy.getRDn(), 0, channelSize);
        System.arraycopy(data.getLUp(), 0, copy.getLUp(), 0, channelSize);
        System.arraycopy(data.getRUp(), 0, copy.getRUp(), 0, channelSize);

        byte[][] channels = copy.getChannels();
        channels[0] = copy.getLDn();
        channels[1] = copy.getRDn();
        channels[2] = copy.getLUp();
        channels[3] = copy.getRUp();
        copy.setChannels(channels);

        return copy;
    }

    /**
     * Getting device answer data
     * Selecting data of scan only
     * Unpacking it in comfortable format
     * @param deviceAnswerData raw data from locator device
     * @return unpacked data of scan only
     */
    public static ScanData getScanData(DeviceAnswerData deviceAnswerData) {
        if (deviceAnswerData == null) {
            return null;
        }
        if (deviceAnswerData.getAnswerType() != DeviceAnswerType.DATA
                || deviceAnswerData.getDataTransferPart() == null) {
            return null;
        }

        ScanData scanData = new ScanData();

        scanData.setAzimuth((deviceAnswerData.getAzimuth() - ZERO_AZIMUTH) * AZIMUTH_STEP);
        scanData.setNumberOfPeriodsOfRadiation(deviceAnswerData.getPeriods());
        scanData.setScanTime(deviceAnswerData.getScanTime());
        scanData.setChannelSize(deviceAnswerData.getDataTransferPart().getChannelLength());

        unpackData(scanData, deviceAnswerData);

        byte[][] channels = scanData.getChannels();
        channels[0] = scanData.getLDn();
        channels[1] = scanData.getRDn();
        channels[2] = scanData.getLUp();
        channels[3] = scanData.getRUp();
        scanData.setChannels(channels);

        return  scanData;
    }

    /**
     * Save unpacked scan data in file: ./scans/c_YYYY.MM.DD_HH-MM-SS-MSS.txt
     */
    public static void saveToFile(ScanData scanData) throws IOException {
        FilesUtil.verifyAndCreateDirectory(PATH_TO_SCANS);

        PrintWriter writer;
        writer = new PrintWriter(PATH_TO_SCANS + UNPACKED_SCAN_DATA_FILE_PREFIX
                + DateUtil.formatScanDate(scanData.getScanTime()) + UNPACKED_SCAN_DATA_FILE_EXTENSION);

        String scanTextData = getDecodedDataTable(scanData);
        writer.print(scanTextData);

        writer.close();
    }

    /**
     * Loading scan data from raw file
     * Filename: YYYY.MM.DD_HH-MM-SS-MSS.txt
     * @param canonicalFileName path and file for reading
     * @return data of scan
     * @throws IOException
     */
    public static ScanData loadFromFile(final String canonicalFileName) throws IOException {
        DeviceAnswerData deviceAnswerData;
        try {
            deviceAnswerData = new DeviceAnswerData(canonicalFileName);
        } catch (ScanFileFormatException e) {
            return null;
        }
        return getScanData(deviceAnswerData);
    }

    /**
     * Creating multiline string from scan data
     * @return decoded scan dat in table
     */
    public static String getDecodedDataTable(ScanData scanData) {
        StringBuilder scanTextData = new StringBuilder();
        double timeOfRadiation = scanData.getNumberOfPeriodsOfRadiation() * TRANSMIT_PERIOD;

        scanTextData.append(String.format("Azimuth: %+3.1f" + LINE_SEPARATOR
                        + "Periods: %d (%.1f mks, %.1f sm)" + LINE_SEPARATOR + LINE_SEPARATOR,
                scanData.getScanAzimuth(), scanData.getNumberOfPeriodsOfRadiation(),
                timeOfRadiation, timeOfRadiation * SPEED_OF_SOUND_IN_SM_PER_MKS));
        scanTextData.append("  No  Time mks Dist. m\t LDn RDn LUp RUp").append(LINE_SEPARATOR);
        scanTextData.append("-----------------------------------------").append(LINE_SEPARATOR);

        byte[] LDn = scanData.getLDn();
        byte[] RDn = scanData.getRDn();
        byte[] LUp = scanData.getLUp();
        byte[] RUp = scanData.getRUp();
        final int channelSize = scanData.getChannelSize();
        double time;
        double distance;
        for (int i = 0; i < channelSize; i++) {
            time = timeOfRadiation + PAUSE_TIME + (i-1) * SAMPLE_PERIOD;
            distance = DISTANCE_PER_MKS * time;

            scanTextData.append(String.format("%5d\t%.0f\t%.3f\t  %d   %d   %d   %d " + LINE_SEPARATOR,
                    i, time, distance, LDn[i], RDn[i], LUp[i], RUp[i]));
        }
        return scanTextData.toString();
    }

    /**
     * Unpacking data of scan, which received from locator device, in four channels
     * @param scanData object which will contain unpacked data
     * @param deviceAnswerData raw data from device
     */
    private static void unpackData(ScanData scanData, DeviceAnswerData deviceAnswerData) {
        final byte[] rawArray = deviceAnswerData.getReceivedRawData();

        final int channelSize = deviceAnswerData.getDataTransferPart().getChannelLength();
        byte[] LDn = new byte[channelSize];
        byte[] RDn = new byte[channelSize];
        byte[] LUp = new byte[channelSize];
        byte[] RUp = new byte[channelSize];

        final byte fourthBitMask = 0x8; // 1000
        final byte thirdBitMask  = 0x4; // 0100
        final byte secondBitMask = 0x2; // 0010
        final byte firstBitMask  = 0x1; // 0001

        int packedByte;
        for (int i = 0; i < channelSize; i++) {
            packedByte = rawArray[i];

            LDn[i] = (byte) ((packedByte & fourthBitMask) >> 3);
            RDn[i] = (byte) ((packedByte & thirdBitMask)  >> 2);
            LUp[i] = (byte) ((packedByte & secondBitMask) >> 1);
            RUp[i] = (byte) (packedByte & firstBitMask);
        }

        scanData.setLDn(LDn);
        scanData.setRDn(RDn);
        scanData.setLUp(LUp);
        scanData.setRUp(RUp);
    }

}
