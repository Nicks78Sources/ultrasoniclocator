package com.locatordevice.beans;

import com.locatordevice.view.Drawer2D;

import javafx.scene.paint.Color;

/**
 * Class contains detected object in plane
 * @version 1.0
 * @author Morhun N.G.
 */
public class DetectedObject2D {

    /** distance to object in plane (meters) */
    private double distance;
    /** angle to object in plane (degrees) */
    private double azimuth;
    /** color of object on radar */
    private Color color;

    public DetectedObject2D(double distance, double azimuth, Color color) {
        this.distance = distance;
        this.azimuth = azimuth;
        this.color = color;
    }

    public DetectedObject2D(double distance, double azimuth) {
        this.distance = distance;
        this.azimuth = azimuth;
        this.color = Drawer2D.RADAR_DATA_COLOR;
    }

    public DetectedObject2D() {
        this.distance = 0;
        this.azimuth = 0;
        this.color = Drawer2D.RADAR_DATA_COLOR;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(double azimuth) {
        this.azimuth = azimuth;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

}
