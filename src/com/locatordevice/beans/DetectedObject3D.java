package com.locatordevice.beans;

import com.locatordevice.view.Drawer2D;
import javafx.scene.paint.Color;

/**
 * Class contains detected object in space
 * @version 1.0
 * @author Morhun N.G.
 */
public class DetectedObject3D {

    /** distance to object in plane (meters) */
    private double distance;
    /** horizontal angle to object in plane (degrees) */
    private double horizontalAzimuth;
    /** vertical angle to object in plane (degrees) */
    private double verticalAzimuth;
    /** color of object on radar */
    private Color color;

    public DetectedObject3D(double distance, double horizontalAzimuth, double verticalAzimuth, Color color) {
        this.distance = distance;
        this.horizontalAzimuth = horizontalAzimuth;
        this.verticalAzimuth = verticalAzimuth;
        this.color = color;
    }

    public DetectedObject3D(double distance, double horizontalAzimuth, double verticalAzimuth) {
        this.distance = distance;
        this.horizontalAzimuth = horizontalAzimuth;
        this.verticalAzimuth = verticalAzimuth;
        this.color = Drawer2D.RADAR_DATA_COLOR;
    }

    public DetectedObject3D() {
        this.distance = 0;
        this.horizontalAzimuth = 0;
        this.verticalAzimuth = 0;
        this.color = Drawer2D.RADAR_DATA_COLOR;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getHorizontalAzimuth() {
        return horizontalAzimuth;
    }

    public void setHorizontalAzimuth(double azimuth) {
        this.horizontalAzimuth = azimuth;
    }

    public double getVerticalAzimuth() {
        return verticalAzimuth;
    }

    public void setVerticalAzimuth(double azimuth) {
        this.verticalAzimuth = azimuth;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

}
