package com.locatordevice.beans;

/**
 * Class contains imagine object in space.
 * It used in emulator of device.
 * @version 1.0
 * @author Morhun N.G.
 */
public class VirtualObject extends DetectedObject3D {
    /** coefficient of object's ability reflect (0 .. 1) */
    private double reflectCoefficient;

    public VirtualObject(double distance, double horizontalAzimuth, double verticalAzimuth, double reflectCoefficient) {
        super(distance, horizontalAzimuth, verticalAzimuth);
        this.reflectCoefficient = reflectCoefficient;
    }

    public double getReflectCoefficient() {
        return reflectCoefficient;
    }

    public void setReflectCoefficient(double reflectCoefficient) {
        this.reflectCoefficient = reflectCoefficient;
    }
}
