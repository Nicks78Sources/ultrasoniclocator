package com.locatordevice.beans;

import static com.locatordevice.comio.LocatorConstants.NUMBER_OF_RECEIVERS;

/**
 * Class contains unpacked scanned data from the locator device.
 * @version 1.0
 * @author Morhun N.G.
 */
public class ScanData {
    /** azimuth of scan in degree (step 1.5 degree) */
    private double azimuth;
    /** number of periods of radiation (0-63) for scan */
    private int numberOfPeriodsOfRadiation;
    /** time, when scan was created */
    private long scanTime;
    /** data arrays from each ultrasonic receiver */
    private byte[] LDn;
    private byte[] RDn;
    private byte[] LUp;
    private byte[] RUp;
    /** array of channels: LDn, RDn, LUp, RUp */
    private byte[][] channels = new byte[NUMBER_OF_RECEIVERS][];
    /** length of data array (8192*2 or 4096*2 or 2048*2) */
    private int channelSize;

    public byte[][] getChannels() {
        return channels;
    }

    public void setChannels(byte[][] channels) {
        this.channels = channels;
    }

    public byte[] getLDn() {
        return LDn;
    }

    public byte[] getRDn() {
        return RDn;
    }

    public byte[] getLUp() {
        return LUp;
    }

    public byte[] getRUp() {
        return RUp;
    }

    public int getChannelSize() {
        return channelSize;
    }

    public long getScanTime() {
        return this.scanTime;
    }

    public double getScanAzimuth() {
        return this.azimuth;
    }

    public int getNumberOfPeriodsOfRadiation() {
        return this.numberOfPeriodsOfRadiation;
    }

    public void setRUp(byte[] RUp) {
        this.RUp = RUp;
    }

    public void setLUp(byte[] LUp) {
        this.LUp = LUp;
    }

    public void setRDn(byte[] RDn) {
        this.RDn = RDn;
    }

    public void setLDn(byte[] LDn) {
        this.LDn = LDn;
    }

    public void setChannelSize(int channelSize) {
        this.channelSize = channelSize;
    }

    public void setAzimuth(double azimuth) {
        this.azimuth = azimuth;
    }

    public void setScanTime(long scanTime) {
        this.scanTime = scanTime;
    }

    public void setNumberOfPeriodsOfRadiation(int numberOfPeriodsOfRadiation) {
        this.numberOfPeriodsOfRadiation = numberOfPeriodsOfRadiation;
    }

}
