package com.locatordevice.view;

import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.ArcType;
import javafx.scene.text.Font;

/**
 * This class provides basic functions of drawing on radar's canvas
 * @version 1.0
 * @author Morhun N.G.
 */
public class Drawer2D {
    /** canvas of radar */
    private Canvas radarCanvas;
    private int canvasWidth;
    private int canvasHeight;
    /** graphics context of radar's canvas */
    private final GraphicsContext graphicsContext;
    /** default font */
    private Font defaultFont;

    public static final Color RADAR_BACKGROUND_COLOR = Color.BLACK;
    public static final Color RADAR_GRID_COLOR = Color.WHITE;
    public static final Color RADAR_DATA_COLOR = Color.GREEN;
    public static final Color RADAR_NOT_SCANNED_COLOR = Color.GRAY;
    public static final Color RADAR_COORDINATES_COLOR = Color.WHITE;
    public static final Color RADAR_CURRENT_AZIMUTH_COLOR = Color.YELLOW;
    public static final Color RADAR_VIEWING_SCOPE_BORDER_COLOR = Color.AQUAMARINE;

    public Drawer2D(Canvas radarCanvas) {
        this.radarCanvas = radarCanvas;
        this.graphicsContext = radarCanvas.getGraphicsContext2D();

        canvasWidth = (int) radarCanvas.getWidth();
        canvasHeight = (int) radarCanvas.getHeight();

        defaultFont = Font.loadFont("file:resources/fonts/ArialRegular.TTF", 16);
        graphicsContext.setFont(defaultFont);
        graphicsContext.setTextBaseline(VPos.CENTER);
    }

    public int getCanvasWidth() {
        return canvasWidth;
    }

    public int getCanvasHeight() {
        return canvasHeight;
    }

    /**
     * Clears radar's canvas
     */
    public void clearRadar() {
        Paint p = graphicsContext.getFill();
        graphicsContext.setFill(RADAR_BACKGROUND_COLOR);
        graphicsContext.fillRect(0, 0, radarCanvas.getWidth(), radarCanvas.getHeight());
        graphicsContext.setFill(p);
    }

    /**
     * Clears radar's canvas with current color
     */
    public void fillRadar() {
        graphicsContext.fillRect(0, 0, radarCanvas.getWidth(), radarCanvas.getHeight());
    }

    /**
     * Clears specified rectangle on radar's canvas
     * @param x1 x-left-top coordinate of rectangle
     * @param y1 y-left-top coordinate of rectangle
     * @param w width of rectangle
     * @param h height of rectangle
     */
    public void eraseRectangle(int x1, int y1, int w, int h) {
        Paint p = graphicsContext.getFill();
        graphicsContext.setFill(RADAR_BACKGROUND_COLOR);
        graphicsContext.fillRect(x1+0.5, y1+0.5, w +0.5, h +0.5);
        graphicsContext.setFill(p);
    }

    /**
     * Sets current color
     * @param color brush color
     */
    public void setColor(Color color) {
        graphicsContext.setFill(color);
        graphicsContext.setStroke(color);
    }

    /**
     * Sets current font
     * @param font current font
     */
    public void setFont(Font font) {
        graphicsContext.setFont(font);
    }

    /**
     * Sets default font
     */
    public void resetFont() {
        graphicsContext.setFont(defaultFont);
    }

    /**
     * Draws line on radar's canvas
     * @param x1 x-coordinate of start point
     * @param y1 y-coordinate of start point
     * @param x2 x-coordinate of finish point
     * @param y2 y-coordinate of finish point
     */
    public void drawLine(int x1, int y1, int x2, int y2) {
        graphicsContext.strokeLine(x1 + 0.5, y1 + 0.5, x2 + 0.5, y2 + 0.5);
    }

    /**
     * Draws and fill specified rectangle on radar's canvas
     * @param x x-left-top coordinate of rectangle
     * @param y y-left-top coordinate of rectangle
     * @param w width of rectangle
     * @param h height of rectangle
     */
    public void drawFilledRectangle(int x, int y, int w, int h) {
        graphicsContext.fillRect(x + 0.5, y + 0.5, w + 0.5, h + 0.5);
    }

    /**
     * Draws specified rectangle's border on radar's canvas
     * @param x x-left-top coordinate of rectangle
     * @param y y-left-top coordinate of rectangle
     * @param w width of rectangle
     * @param h height of rectangle
     */
    public void drawRectangle(int x, int y, int w, int h) {
        graphicsContext.strokeRect(x + 0.5, y + 0.5, w, h);
    }

    /**
     * Draws and fill circle
     * @param x x-coordinate of circle's center
     * @param y y-coordinate of circle's center
     * @param d diameter of circle
     */
    public void drawFilledCircle(int x, int y, int d) {
        int r = d / 2;
        graphicsContext.fillOval(x - r + 0.5, y - r + 0.5, d, d);
    }

    /**
     * Draws transparent circle
     * @param x x-coordinate of circle's center
     * @param y y-coordinate of circle's center
     * @param d diameter of circle
     * @param alpha alpha blend value
     */
    public void drawTransparentFilledCircle(int x, int y, int d, double alpha) {
        int r = d / 2;
        graphicsContext.setGlobalAlpha(alpha);
        graphicsContext.fillOval(x - r + 0.5, y - r + 0.5, d, d);
        graphicsContext.setGlobalAlpha(1.0);
    }

    /**
     * Draws circle's borders
     * @param x x-coordinate of circle's center
     * @param y y-coordinate of circle's center
     * @param d diameter of circle
     */
    public void drawCircle(int x, int y, int d) {
        int r = d / 2;
        graphicsContext.strokeOval(x - r + 0.5, y - r + 0.5, d, d);
    }

    /**
     * Draws upper semicircle
     * @param x0 x-coordinate of circle's center
     * @param y0 y-coordinate of circle's center
     * @param r radius of circle
     */
    public void drawUpperSemicircle(int x0, int y0, int r) {
        int diameter = 2*r;
        graphicsContext.strokeArc(x0 - r, y0 - r, diameter, diameter, 0, 180, ArcType.OPEN);
    }

    /**
     * Draws text in specified coordinates on radar's canvas
     * @param text text for output
     * @param x x-coordinate of text
     * @param y y-coordinate of text
     */
    public void drawText(String text, int x, int y) {
        graphicsContext.fillText(text, x, y);
    }

    /**
     * Take snapshot of canvas
     * @return snapshot of whole canvas
     */
    public WritableImage takeCanvasSnapshot() {
        WritableImage image = new WritableImage(canvasWidth, canvasHeight);
        radarCanvas.snapshot(null, image);
        return image;
    }

}
