package com.locatordevice.view;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Simple wrapper on javafx dialogs
 * @version 1.0
 * @author Morhun N.G.
 */
public class Dialogs {

    public static void infoDialog(String title, String header, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.setResizable(true);

        alert.showAndWait();
    }

    public static void infoDialog(String message) {
        infoDialog("Information", null, message);
    }

    public static void warningDialog(String title, String header, String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.setResizable(true);

        alert.showAndWait();
    }

    public static void warningDialog(String message) {
        warningDialog("Warning", null, message);
    }

    public static void errorDialog(String title, String header, String errorContent) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(errorContent);
        alert.setResizable(true);

        alert.showAndWait();
    }

    public static void errorDialog(String header, String errorContent) {
        errorDialog("Error", header, errorContent);
    }

    public static void errorDialog(String errorContent) {
        errorDialog("Error", null, errorContent);
    }

    /**
     * Simple confirmation dialog
     * @return true if OK pressed and false otherwise
     */
    public static boolean confirmationDialog(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        Optional<ButtonType> result = alert.showAndWait();
        return (result.get() == ButtonType.OK);
    }

    public static boolean confirmationDialog(String header, String content) {
        return confirmationDialog("Question", header, content);
    }

    public static boolean confirmationDialog(String content) {
        return confirmationDialog("Question", null, content);
    }

}
